DOCKER_IMAGE=docker.atl-paas.net/mjensen/stroll-app
DOCKER_TAG=`cat version`
ENVIRONMENT=ddev
SERVICE=stroll-app

deps:
	npm install

build: deps
	docker build -t ${DOCKER_IMAGE}:${DOCKER_TAG} .

push: build
	docker push ${DOCKER_IMAGE}:${DOCKER_TAG}

deploy: push
	DOCKER_IMAGE=${DOCKER_IMAGE} DOCKER_TAG=${DOCKER_TAG} micros service:deploy ${SERVICE} -f service-descriptor.yml -e ${ENVIRONMENT}

