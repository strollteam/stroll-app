1. Go to https://developer.atlassian.com/apps/
1. Create an application called 'Stroll Development <Username>'
1. Enable the Stride API
1. Reveal the Client ID and Client Secret from the "Enabled APIs" tab
   Copy stride.sh.txt to stride.sh and make it executable
   Edit this script and put in your own credentials
   Start the app locally using fast.sh
   Do not commit stride.sh.
1. Start a local ngrok
   ngrok http 8080
   Note the ngrok http path
1. In the "Install" tab of app management
   Enter the descriptor url as http://ngrok.hostname/descriptor
1. Create your own private room
1. Install the integration in to this room using the "Installation URL" in the Install tab of app management
1. Install and run redis following [these instructions](https://www.npmjs.com/package/redis)

Dev loop - Common:

1. Install grunt - npm install -g grunt-cli

Dev loop - With Stride:

2. Start ngrok and fast.sh
3. If you made a change to the descriptor, run the curl command in your Install tab in app management
4. Restarting Stroll by terminating fast.sh and rerunning it, should be relativley quick

Dev loop - Without Stride:

2. Start the app with 'grunt test-dev'
3. Load http://localhost:8080/test.html to test the ui components of this app  
   Most of the features will work except those that require stride (decisions/invite).
   You can pass in cloudId/conversationId/userId to test.html has query parameters and start multiple browser windows to test how multiple users interact.