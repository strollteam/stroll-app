#!/bin/sh

echoerr() { echo "$@" 1>&2; }

BIN=`npm bin --global`

if [ ! -f $BIN/micros ]; then
    echoerr install micros cli
    exit -1
fi

if [ ! -f `which jq` ]; then
    echoerr install jq
    exit -1
fi

JSON=`$BIN/micros service:show stroll-app --json`
DEV=`echo $JSON | jq -r '.stacks.ddev[0].version'`
PROD=`echo $JSON | jq -r '.stacks."prod-east"[0].version'`
LOCAL=`cat package.json | jq -r '.version'`

if [ "$1" = "" ]; then
    echo Development: $DEV
    echo Production: $PROD
    echo Local: $LOCAL
elif [ "$1" = "--prod" ]; then
    echo $PROD
elif [ "$1" = "--dev" ]; then
    echo $DEV
elif [ "$1" = "--local" ]; then
    echo $LOCAL
fi

