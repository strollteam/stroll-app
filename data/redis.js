const environment = process.env.NODE_ENV ? process.env.NODE_ENV : "production";
var redis;
if (environment == "production") {
    redis = require("redis");
}
else {
    // this is an in-memory redis
    redis = require("redis-js");
}

const host = process.env.REDIS_DB_HOST || "localhost";
const port = process.env.REDIS_DB_PORT || "6379";

client = redis.createClient(port, host);

client.on("error", function (err) {
    log.error("Error " + err);
});

function addPoll(poll) {
    let value = JSON.stringify(poll);
    client.set(`polls/${poll.id}`, value);

    // store the poll in a set for its conversation, too
    client.sadd(`polls/${poll.conversationId}`, poll.id);
}

function getPoll(id, callback) {
    client.get(`polls/${id}`, function (err, reply) {
        callback(err, JSON.parse(reply));
    });
}

function deletePoll(poll, callback) {
    let id = poll.id;
    let conversationId = poll.conversationId;
    client.del(`polls/${id}`, (err, reply) => {
        client.srem(`polls/${conversationId}`, poll.id, (err, reply) => {
            client.keys(`answers/${id}/*/*`, (err, keys) => {
                if (err) {
                    callback(err, []);
                }
                else if (keys && keys.length > 0) {
                    client.del(keys, callback);
                }
                else {
                    callback(err, []);
                }
            });
        });
    });
}

function flushdb(callback) {
    client.flushdb(function(err,succeeded) {
        callback(err,succeeded);
    });
}

function getPolls(callback) {
    client.keys('polls/*', function (err, replies) {
        client.mget(replies, function (err, replies) {
            callback(err, replies &&
                replies
                    .filter(r => !!r)
                    .map(r => JSON.parse(r)));
        });
    });
}

function getPollsByConversation(conversationId, callback) {
    client.smembers(`polls/${conversationId}`, (err, keys) => {
        if (keys && keys.length > 0) {
            client.mget(keys.map(id => `polls/${id}`), (err, polls) => {
                callback(err, polls &&
                    polls
                        .filter(poll => !!poll)
                        .map(r => JSON.parse(r)));
            })
        }
        else {
            callback(err, null);
        }
    });
}

function updatePoll(poll) {
    this.addPoll(poll);
}

function addAnswer(answer) {
    let pollId = answer.pollId;
    let answerId = answer.id;
    let userId = answer.userId;

    // if this user has voted on this poll before, delete their vote then re-cast it
    client.keys(`answers/${pollId}/*/${userId}`, (err, keys) => {
        if (err) {
            log.warn(`Error retrieving votes for user ${userId}`);
            return;
        }
        else {
            if (!keys || keys.length == 0) {
                // no vote already, save the vote
                client.set(`answers/${pollId}/${answerId}/${userId}`, JSON.stringify(answer));
            }
            else {
                // first delete their vote
                client.del(keys, (err, reply) => {
                    if (err) {
                        log.warn(`Error deleting votes ${keys}`);
                    }
                    else {
                        // now cast the vote
                        client.set(`answers/${pollId}/${answerId}/${userId}`, JSON.stringify(answer));
                    }
                });
            }
        }
    });
}

function getAnswers(pollId, callback) {
    client.keys(`answers/${pollId}/*/*`, function (err, replies) {
        client.mget(replies, function (err, replies) {
            callback(err, replies && replies.filter(f => !!f).map(r => JSON.parse(r)));
        });
    });
}
function getAnswersByUser(userId, callback) {
    client.keys(`answers/*/*/${userId}`, function(err, replies) {
        if (err) {
            log.warn(`Error getting answers by user: ${err}`);
            callback(err, []);
        }
        else {
            replies = replies.filter(f => !!f);
            if (!replies || replies.length == 0) {
                callback(err, []);
            }
            else {
                client.mget(replies, function(err, replies) {
                    callback(err, replies && replies.map(r => JSON.parse(r)));
                });
            }
        }
    });
}

function getSchedules(callback) {
    client.keys(`schedules/*`, function (err, replies) {
        if (!replies || replies.length == 0) {
            callback(err, []);
        }
        else {
            client.mget(replies, function (err, replies) {
                callback(err, replies &&
                    replies
                        .filter(r => !!r)
                        .map(r => JSON.parse(r)));
            });
        }
    });
}

function addSchedule(poll, closeTime) {
    let schedule = {
        pollId: poll.id,
        closeTime: closeTime
    };
    client.set(`schedules/${poll.id}`, JSON.stringify(schedule));
}

function deleteSchedule(pollId) {
    client.del(`schedules/${pollId}`);
}

module.exports = {
    addPoll,
    getPoll,
    getPolls,
    getPollsByConversation,
    deletePoll,
    updatePoll,
    addAnswer,
    getAnswers,
    getAnswersByUser,
    getSchedules,
    addSchedule,
    deleteSchedule,
    flushdb
}
