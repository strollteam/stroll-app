module.exports = function (grunt) {

    const CLIENT_ID = process.env.CLIENT_ID;
    const CLIENT_SECRET = process.env.CLIENT_SECRET;
    const DOCKER_IMAGE = "docker.atl-paas.net/mjensen/stroll-app";

    const webpack = require("webpack");
    const webpackConfig = require("./webpack.config.js");
    const username = process.env.USER || process.env.USERNAME;
    const cmd = require('node-cmd');
    const async = require('async');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        webpack: {
            options: webpackConfig,
            build: {
                plugins: webpackConfig.plugins.concat(
                    new webpack.DefinePlugin({
                        "process.env": {
                            // This has effect on the react lib size
                            "NODE_ENV": JSON.stringify("production")
                        }
                    })
                )
            },
            "build-test": {
                entry: Object.assign(webpackConfig.entry, {
                    "deps": "./test/deps.js"
                })
            },
            "build-dev": {
                devtool: "sourcemap",
                plugins: webpackConfig.plugins.concat(
                    new webpack.LoaderOptionsPlugin({
                        "debug": true
                    })
                )
            }
        },

        "webpack-dev-server": {
            options: {
                webpack: webpackConfig,
                publicPath: "/" + webpackConfig.output.publicPath
            },
            start: {
                webpack: {
                    devtool: "eval",
                    plugins: webpackConfig.plugins.concat(
                        new webpack.LoaderOptionsPlugin({
                            "debug": true
                        })
                    )
                }
            }
        },
        watch: {
            app: {
                files: ["src/**/*"],
                tasks: ["webpack:build-dev"],
                options: {
                    spawn: false,
                }
            },
            test: {
                files: ["src/**/*", "test/deps.js"],
                tasks: ["webpack:build-test"],
                options: {
                    spawn: false,
                }
            },
        },
        express: {
            development: {
                options: {
                    script: "./app.js",
                    "args": [CLIENT_ID, CLIENT_SECRET],
                    node_env: "development"
                }
            },
            test: {
                options: {
                    script: "./app.js",
                    "args": ["CLIENT_ID", "CLIENT_SECRET"],
                    node_env: "development"
                }
            },
            production: {
                options: {
                    script: "./app.js",
                    "args": [CLIENT_ID, CLIENT_SECRET],
                    node_env: "production",
                    background: false
                }
            }
        },
        webdriver: {
            test: {
                configFile: './test/wdio.conf.js'
            }
        },
        version: {
            options: {},
            dev: {
                options: {
                    release: "prerelease",
                    prereleaseIdentifier: username
                },
                src: ['package.json', 'app.js']
            },
            release: {
                options: {
                    release: "patch",
                },
                src: ['package.json', 'app.js']
            },
        },
        git: {
            check: {
                options: {
                    nameOnly: true,
                    simple: {
                        cmd: "diff",
                        onComplete: function (err, stdout, callback) {
                            if (err) {
                                grunt.fail.fatal(err.message, err.code);
                            } else {
                                if (stdout) {
                                    console.log(stdout);
                                    grunt.fail.fatal("Changes Detected");
                                }
                                callback();
                            }
                        }
                    }
                }
            },
            "commit-version": {
                options: {
                    message: "bump version for " + grunt.file.readJSON('package.json').version,
                    simple: {
                        cmd: "commit",
                        args: ["-a"],
                        onComplete: function (err, stdout, callback) {
                            console.log(stdout);
                            if (err) {
                                grunt.fail.fatal(err.message, err.code);
                            } else {
                                callback();
                            }
                        }
                    }
                }
            },
            "push": {
                options: {
                    simple: {
                        cmd: "push",
                        args: ["origin", "master"]
                    }
                }
            },
            "push-tags": {
                options: {
                    simple: {
                        cmd: "push",
                        args: ["origin", "--tags"]
                    }
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-webpack');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-express-server');
    grunt.loadNpmTasks('grunt-webdriver');
    grunt.loadNpmTasks('grunt-version');
    grunt.loadNpmTasks('grunt-simple-git');

    grunt.registerTask('tag-version', "tag new version", function () {
        let newVersion = grunt.file.readJSON('package.json').version;
        const done = this.async();
        console.log("Tag Version: ", newVersion);
        cmd.get(
            'git tag -m "tag for release ' + newVersion + '" release/release-' + newVersion,
            function (err, data, stderr) {
                console.log(data);
                console.log(err);
                done();
            }
        );
    });

    grunt.registerTask('docker-build', "build docker image", function () {
        let newVersion = grunt.file.readJSON('package.json').version;
        const done = this.async();
        console.log("Build Version: ", newVersion);
        const processRef = cmd.get(
            'docker build -t ' + DOCKER_IMAGE + ':' + newVersion + ' .',
            function (err, data, stderr) {
                done();
            }
        );
        processRef.stdout.on(
            'data',
            function (data) {
                console.log(data);
            }
        );
    });

    grunt.registerTask('docker-push', "push docker image", function () {
        let newVersion = grunt.file.readJSON('package.json').version;
        const done = this.async();
        console.log("Push Version: ", newVersion);
        const processRef = cmd.get(
            'docker push ' + DOCKER_IMAGE + ':' + newVersion,
            function (err, data, stderr) {
                done();
            }
        );
        processRef.stdout.on(
            'data',
            function (data) {
                console.log(data);
            }
        );
    });


    grunt.registerTask('prod', ['express:production']);
    grunt.registerTask("dev", ["webpack:build-dev", "express:development", "watch:app"]);
    grunt.registerTask("test-dev", ["webpack:build-test", "express:test", "watch:test"]);
    grunt.registerTask("test", ["webpack:build-test", "express:test", "webdriver:test"]);

    // grunt.registerTask("release", ["git:check", "version:release", "git:commit-version", "git:push", "git:tag-version","git:push-tags"]);
    // grunt.registerTask("release", ["git:check", "version:release", "git:commit-version", "git:push", "git:tag-version"]);
    grunt.registerTask("release", [
        "version:release", "git:commit-version", "git:push",
        "tag-version", "git:push-tags",
        "docker-build", "docker-tag",
        "version:dev", "git:commit-version", "git:push"]);

    grunt.registerTask("default", ["production"]);
};