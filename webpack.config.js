let webpack = require('webpack');
let path = require('path');
let OnlyIfChangedPlugin = require('only-if-changed-webpack-plugin');

var opts = {
    rootDir: process.cwd(),
    devBuild: process.env.NODE_ENV !== 'production',
  };

//noinspection JSUnresolvedFunction,JSUnresolvedVariable
module.exports = {
    devtool: 'sourcemap',
    entry:{
        sidebar: './src/entry-points/SideBar.js',
        dialog: './src/entry-points/Dialog.js'
    },
    output: {
        path: __dirname+"/generated/js",
        filename: '[name].js'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new OnlyIfChangedPlugin({
            cacheDirectory: path.join(opts.rootDir, 'generated'),
            cacheIdentifier: opts
        })
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-0'],
                    plugins: ['react-html-attrs', 'transform-decorators-legacy', 'transform-class-properties']
                },
                exclude: path.resolve(__dirname, 'node_modules/')
            },
            {
                test: /\.less$/,
                loader: "style-loader!css-loader!less-loader"
            }
        ]
    }
};

