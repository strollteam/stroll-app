#!/bin/bash
. docker.sh
echo
echo ${C1}Build Image:${C2} $DOCKER_IMAGE:$DOCKER_TAG ${NC}
echo
docker build -t $DOCKER_IMAGE:$DOCKER_TAG .
