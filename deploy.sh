#/bin/sh
. docker.sh

echo
echo -e ${C1}Dev Deploy Image:${C2} $DOCKER_IMAGE:$DOCKER_TAG ${NC}
echo

DOCKER_IMAGE=$DOCKER_IMAGE \
DOCKER_TAG=$DOCKER_TAG \
micros service:deploy stroll-app -f service-descriptor.yml
