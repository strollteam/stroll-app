import React, {Component} from 'react';

import SideBarLoading from '../components/sidebar/SideBarLoading';
import PollForm from '../components/dialog/PollForm';
import {connect} from "react-redux"

require('../css/dialog.less');

@connect((store) => {
    return {
        state: store.state
    }
})
class DialogContainer extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    render() {
        const self = this;
        return (
            <div className="dialog">
                {((state) => {
                    switch (state) {
                        case 'loading':
                            return <SideBarLoading/>;
                        case 'add':
                            return <PollForm savePoll={self.props.savePoll}/>;
                        default:
                            return <h1>Unknown state: {state}</h1>
                    }
                })(this.props.state)}
            </div>
        );
    }
}

export default DialogContainer;