import React, {Component} from 'react';

import SideBarLoading from '../components/sidebar/SideBarLoading';
import SideBarEmpty from '../components/sidebar/SideBarEmpty';
import SideBarPopulated from '../components/sidebar/SideBarPopulated';
import Poll from '../components/sidebar/Poll';
import ViewPoll from '../components/sidebar/ViewPoll';
import {connect} from "react-redux"

require('../css/sidebar.less');

@connect((store) => {
    return {
        state: store.state,
        polls: store.polls,
        current: store.current,
        answers: store.answers
    }
})
class SideBarContainer extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        AP.register({
            "createpoll-finished": function (data) {
                console.log("createpoll-finished");
                // AP.dialog.close("poll-dialog");
            }
        });
    }

    render() {
        return (
            <div className="sidebar">
                {((state) => {
                    switch (state) {
                        case 'selected':
                            return <Poll returnHome={this.props.returnHome}
                                         showResults={this.props.showResults}
                                         submitPoll={this.props.submitPoll}
                                         inviteTeamToPoll={this.props.inviteTeamToPoll}
                                         closePoll={this.props.closePoll}
                                         deletePollButtonClick={this.props.deletePollButtonClick}
                                         poll={this.props.current}/>;
                        case 'view':
                            return <ViewPoll returnHome={this.props.returnHome}
                                             showVotingScreen={this.props.showVotingScreen}
                                             poll={this.props.current}
                                             answers={this.props.answers}
                                             deletePollButtonClick={this.props.deletePollButtonClick}
                                             inviteTeamToPoll={this.props.inviteTeamToPoll}
                                             closePoll={this.props.closePoll}
                                             />;
                        case 'loading':
                            return <SideBarLoading/>;
                        case 'list':
                            let body;
                            if (this.props.polls.length == 0) {
                                body = <SideBarEmpty addPollButtonClick={this.props.addPollButtonClick}/>
                            } else {
                                body = <SideBarPopulated editPollButtonClick={this.props.editPollButtonClick}
                                                         completePollButtonClick={this.props.completePollButtonClick}
                                                         deletePollButtonClick={this.props.deletePollButtonClick}
                                                         viewPollButtonClick={this.props.viewPollButtonClick}
                                                         addPollButtonClick={this.props.addPollButtonClick}
                                                         polls={this.props.polls}
                                />
                            }
                            return (
                                <div>
                                    {body}
                                </div>
                            );
                        default:
                            return <h1>Unknown state: {state}</h1>
                    }
                })(this.props.state)}
            </div>
        );
    }
}

export default SideBarContainer;