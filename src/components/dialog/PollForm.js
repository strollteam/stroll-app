import React, {Component} from 'react';
import {FieldTextStateless} from '@atlaskit/field-text';
import PollFormQuestion from './PollFormQuestion';

class PollForm extends Component {

    constructor(props) {
        super(props);
        this.handleQuestionChangeEvent = this.handleQuestionChangeEvent.bind(this);
        this.updateValidationState = this.updateValidationState.bind(this);
        this.validatePollData = this.validatePollData.bind(this);
        this.state = {
            "dirty": false,
            "validation": {},
            "name": "",
            "questions": [
                {
                    "prompt": "",
                    "type": "select",
                    "options": [
                        {id: 1}, {id: 2}, {id: 3}
                    ]
                }
            ]
        }
    }

    validatePollData() {
        let validation = {
            valid: true,
            name: false,
            option: false
        };

        let question = this.state.questions[0];

        if (!question.prompt || question.prompt.trim().length == 0) {
            validation.name = true;
            validation.valid = false;
        }

        const filteredOptions = question.options.filter(option => option.value && option.value.length > 0);
        if (filteredOptions.length < 2) {
            validation.option = true;
            validation.valid = false;
        }
        return validation;
    }

    updateValidationState(callback) {
        let validation = this.validatePollData();
        this.setState({validation: validation}, () => {
            if (callback && typeof callback === "function" && validation.valid) {
                callback();
            }
        });
        return validation.valid;
    }

    componentDidMount() {
        const createData = this.createData.bind(this);
        const self = this;
        AP.register({
            "dialog-button-clicked": function (event, closeDialog) {
                try {
                    if (event.action === "action-save") {
                        self.setState({dirty:true},()=>{
                            self.updateValidationState(() => {
                                let createdPoll = createData();
                                self.props.savePoll(createdPoll);
                            });
                        });
                        closeDialog(false);
                    } else {
                        closeDialog(true);
                    }
                } catch (e) {
                    console.log("error", e);
                }
            }
        })
    }

    createData() {
        const stateSnapshot = Object.assign({}, this.state);
        stateSnapshot.questions = stateSnapshot.questions.map(question => {
            const filteredOptions = question.options.filter(option => option.value && option.value.length !== 0);
            const filteredQuestion = Object.assign({}, question);
            filteredQuestion.options = filteredOptions.map((option, index) => {
                return {
                    id: "choice_" + index,
                    value: {
                        en: option.value
                    }
                }
            });
            return filteredQuestion;
        });
        stateSnapshot.name = stateSnapshot.questions[0].prompt;
        delete stateSnapshot.dirty;
        delete stateSnapshot.validation;
        return stateSnapshot;
    }

    handleQuestionChangeEvent(questionNumber, questionData) {
        this.state.questions[questionNumber] = questionData;
        this.setState({
            name: this.state.questions[0].prompt,
            questions: this.state.questions
        }, () => {
            if (this.state.dirty) this.updateValidationState(this.createData());
        });
    }

    render() {
        return (
            <div className="createpoll">
                {this.state.questions.map((question, index) => {
                    return (<PollFormQuestion key={index}
                                              index={index}
                                              data={question}
                                              onQuestionUpdated={this.handleQuestionChangeEvent}
                                              validation={this.state.validation}
                                              dirty={this.state.dirty}
                    />);
                })}
            </div>
        )
    }
}

export default PollForm;
