import React, {Component} from "react";
import Button from "@atlaskit/button";
import PollOption from "./PollFormOption";
import AddIcon from "@atlaskit/icon/glyph/add";
import {FieldTextStateless} from "@atlaskit/field-text";
import {DragDropContext, Droppable} from "react-beautiful-dnd";

class PollFormQuestion extends Component {
    constructor(props) {
        super(props);
        this.state = props.data;
        this.removeChoice = this.removeChoice.bind(this);
        this.addChoice = this.addChoice.bind(this);
        this.handleQuestionNameChange = this.handleQuestionNameChange.bind(this);
        this.handleChoiceInputChange = this.handleChoiceInputChange.bind(this);
        this.questionChangeCallback = props.onQuestionUpdated;
        this.idCounter = this.state.options.length;
    }

    addChoice() {
        this.state.options.push({id: ++this.idCounter});
        this.setState({options: this.state.options}, () => {
            this.questionChangeCallback(this.props.index, this.state);
        });
    }

    removeChoice(index) {
        this.state.options.splice(index, 1);
        this.setState({options: this.state.options}, () => {
            this.questionChangeCallback(this.props.index, this.state);
        });
    }

    handleQuestionNameChange(event) {
        this.setState({prompt: event.target.value}, () => {
            this.questionChangeCallback(this.props.index, this.state);
        });
    }

    handleChoiceInputChange(index, event) {
        this.state.options[index].value = event.target.value;
        this.setState(this.state, () => {
            this.questionChangeCallback(this.props.index, this.state);
        });
    }

    reorder = (list, startIndex, endIndex) => {
        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);
        return result;
    };

    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        const options = this.reorder(
            this.state.options,
            result.source.index,
            result.destination.index
        );


        this.setState({options: options}, () => {
            this.questionChangeCallback(this.props.index, this.state);
        });
    }

    render() {
        let invalid = (this.props.validation.name && this.props.dirty);
        return (
            <div className="question">
                <FieldTextStateless onChange={this.handleQuestionNameChange}
                                    placeholder="Question"
                                    shouldFitContainer={true}
                                    value={this.state.prompt}
                                    name="question"
                                    isLabelHidden={true}
                                    label=""
                                    isInvalid={invalid}
                />
                <DragDropContext
                    onDragEnd={(result) => this.onDragEnd(result)}>
                    <div>
                        <Droppable droppableId={'' + this.props.index}>
                            {(provided, snapshot) => (
                                <div ref={provided.innerRef}>
                                    {this.state.options.map((choice, index) => {
                                        return <PollOption
                                            key={index}
                                            option={choice}
                                            index={index}
                                            validation={this.props.validation}
                                            handleChoiceInputChange={this.handleChoiceInputChange}
                                            removeChoice={this.removeChoice}
                                            dirty={this.props.dirty}
                                        />
                                    })}
                                    {provided.placeholder}
                                </div>
                            )}
                        </Droppable>
                        <div>
                            <div>
                                <Button title="remove" key="btn_add_choice" onClick={this.addChoice}>
                                    <AddIcon label="Add" size="medium"/>
                                </Button>
                            </div>
                        </div>
                    </div>
                </DragDropContext>

            </div>
        );
    }
}


export default PollFormQuestion;