import React, {Component} from 'react';
import Button from '@atlaskit/button';
import EditorRemoveIcon from '@atlaskit/icon/glyph/editor/remove';
import {FieldTextStateless} from '@atlaskit/field-text';
import {Draggable} from 'react-beautiful-dnd';

class PollFormOption extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let invalid = (this.props.index<2 && this.props.validation.option && this.props.dirty);
        return (
            <Draggable draggableId={this.props.option.id} key={this.props.option.id}>
                {(provided, snapshot) => (
                    <div>
                        <div
                            ref={provided.innerRef}
                            style={provided.draggableStyle}
                            {...provided.dragHandleProps}
                            tabIndex="-1">
                            <div className="question-container" key={"choice_" + this.props.index}>
                                <div className="drag-handle">
                                    ::
                                </div>
                                <div className="choice-field"
                                     onKeyDown={(e) => {
                                         e.stopPropagation()
                                     }}
                                     onMouseDown={(event) => {
                                         event.stopPropagation()
                                     }}>
                                    <FieldTextStateless
                                        onChange={(event) => this.props.handleChoiceInputChange(this.props.index, event)}
                                        placeholder={"Choice " + (this.props.index + 1)}
                                        id={"choice-" + this.props.option.id}
                                        value={this.props.option.value || ""}
                                        shouldFitContainer={true}
                                        isLabelHidden={true}
                                        label=""
                                        isInvalid={invalid}
                                    />
                                </div>
                                <div className="choice-actions">
                                    {
                                        this.props.index >= 2 &&
                                        <Button className="delete_button"
                                                id={"choice_" + this.props.option.id + "_delete_button"}
                                                key={"remove_btn_" + this.props.option.id}
                                                tabIndex={-1}
                                                onClick={() => this.props.removeChoice(this.props.index)}
                                                iconAfter={<EditorRemoveIcon label="remove Icon"/>}/>
                                    }
                                </div>
                            </div>
                        </div>
                        {provided.placeholder}
                    </div>
                )}
            </Draggable>
        );
    }
}

export default PollFormOption;