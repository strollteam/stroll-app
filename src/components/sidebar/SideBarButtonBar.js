import React, {Component} from 'react';

class SideBarButtonBar extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="buttons">
                <button onClick={this.props.addPollButtonClick}>Add</button>
            </div>
        )
    }

}

export default SideBarButtonBar;