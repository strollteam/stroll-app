import React, {Component} from 'react';
import PollCard from './PollCard';
import Button from '@atlaskit/button';

class SideBarPopulated extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let polls = this.props.polls;
        let editPollButtonClick = this.props.editPollButtonClick;
        let completePollButtonClick = this.props.completePollButtonClick;
        let viewPollButtonClick = this.props.viewPollButtonClick;
        let deletePollButtonClick = this.props.deletePollButtonClick;

        let openPolls = polls.filter(poll => poll.open && !poll.submitted);
        let votedPolls = polls.filter(poll => poll.open && poll.submitted);
        let closedPolls = polls.filter(poll => !poll.open);

        let pollCard = (poll) => {
            return <PollCard
                        key={poll.id}
                        deletePollButtonClick={deletePollButtonClick}
                        editPollButtonClick={editPollButtonClick}
                        completePollButtonClick={completePollButtonClick}
                        viewPollButtonClick={viewPollButtonClick}
                        poll={poll}
                    />
        };
        let pollList = (polls, title, className) =>{
            return <div className={"poll-list " + className}>
                <h4 className="poll-list-header">
                    {title}
                </h4>
                {polls.map((poll)=> pollCard(poll))}
            </div>
        }
        
        return (
            <div className="panel populated">
                    {openPolls.length > 0 && pollList(openPolls, "Open polls", "open")}
                    {votedPolls.length > 0 && pollList(votedPolls, "Voted polls", "voted")}
                    {closedPolls.length > 0 && pollList(closedPolls, "Closed polls", "closed")}
                
                <div className="buttons">
                    <Button onClick={()=>{
                        this.props.addPollButtonClick();
                    }}>Add a Stroll</Button>
                </div>
            </div>
        )
    }

}

export default SideBarPopulated;