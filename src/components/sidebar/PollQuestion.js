import React, {Component} from 'react';
import RadioGroup from '@atlaskit/field-radio-group';

class PollQuestion extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let question = this.props.question;
        let setAnswerValue = this.props.setAnswerValue;
        return (
            <div className="question">
                <span className="title">{question.prompt}</span>
                {((type) => {
                    switch (type) {
                        case 'select':
                            let items = question.options.map((o) => {
                                return {
                                    name: question.prompt,
                                    value: o.id,
                                    label: <div className="answer-label">{o.value.en}</div>
                                }
                            });
                            return <RadioGroup onRadioChange={(e) => {
                                setAnswerValue(question.id,e.target.value);
                            }}
                                               items={items}/>;
                        default:
                            return <h1>Unknown type: {type}</h1>
                    }
                })(question.type)}
            </div>
        )
    }

}

export default PollQuestion;