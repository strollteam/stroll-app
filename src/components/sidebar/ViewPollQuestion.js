import React, {Component} from 'react';
import ViewPollQuestionSelect from './ViewPollQuestionSelect';

class ViewPollQuestion extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let question = this.props.question;
        let answers = this.props.answers;
        return (
            <div className="question">
                <span className="title">{question.prompt}</span>
                {((type) => {
                    switch (type) {
                        case 'select':
                            return <ViewPollQuestionSelect question={question}
                                                           answers={answers}/>;
                        default:
                            return <h1>Unknown type: {type}</h1>
                    }
                })(question.type)}
            </div>
        )
    }

}

export default ViewPollQuestion;



