import React, {Component} from 'react';
import ViewPollQuestion from './ViewPollQuestion';
import Button from '@atlaskit/button';
import TextField, {FieldText} from '@atlaskit/field-text'
import {
    DropdownMenuStateless as Dropdown,
    DropdownItemGroup,
    DropdownItem,
} from '@atlaskit/dropdown-menu';
import axios from 'axios';

class PollActionMenu extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isDropdownOpen: false
        };
    }

    render() {
        return (
            <Dropdown
                isOpen={this.state.isDropdownOpen}
                onOpenChange={(attrs) => {
                    this.setState({ isDropdownOpen: attrs.isOpen });
                }}
                trigger="..."
                triggerType="button"
                position="bottom center"
            >
                <DropdownItemGroup title="Actions" id="actions">
                    {this.props.showDecisionDialog &&
                    <DropdownItem 
                        onClick={() => {this.props.showDecisionDialog()}} 
                        id="createdecisionitem"
                    >
                        Create a decision
                    </DropdownItem>}

                    <DropdownItem 
                        onClick={() => {this.props.inviteTeamToPoll()}} 
                        id="inviteteam"
                    >
                        Invite team to vote
                    </DropdownItem>

                    <DropdownItem 
                        onClick={() => {this.props.closePoll()}} 
                        id="closePoll"
                    >
                        Close this Stroll
                    </DropdownItem>
                    
                    {this.props.showVotingScreen &&
                    <DropdownItem 
                        onClick={() => {this.props.showVotingScreen()}} 
                        id="closePoll"
                    >
                        Change vote
                    </DropdownItem>}

                </DropdownItemGroup>

                <DropdownItemGroup title="Danger Zone" id="danger">
                    <DropdownItem 
                        onClick={() => {this.props.deletePoll()}} 
                        id="deletepoll"
                    >
                        Delete this Stroll
                    </DropdownItem>
                </DropdownItemGroup>
            </Dropdown>
        );
    }

}


export default PollActionMenu;
