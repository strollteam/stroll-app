import React, {Component} from 'react';
import PollQuestion from './PollQuestion';
import PollActionMenu from './PollActionMenu';
import Button from '@atlaskit/button';

class Poll extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inviteEnabled: true
        };
        this.answer = {
            "pollId": this.props.poll.id,
            "answers": {}
        };
        this.setAnswerValue = this.setAnswerValue.bind(this);
    }

    setAnswerValue(id, value) {
        this.answer.answers[id] = value;
    }

    inviteTeam(poll) {
        this.props.inviteTeamToPoll(poll);
        this.setState({inviteEnabled: false});
    }

    closePoll(poll) {
        let that = this;
        this.props.closePoll(poll)
            .then(function(result) {
                that.props.returnHome();
            });
    }

    render() {
        let poll = this.props.poll;
        let submitPoll = this.props.submitPoll;
        let returnHome = this.props.returnHome;
        let showResults = this.props.showResults;
        return (
            <div className="panel submit">
                <div className="questions">
                    {poll.questions.map((q) => {
                        return <PollQuestion key={q.prompt}
                                             question={q}
                                             setAnswerValue={this.setAnswerValue}
                        />
                    })}
                </div>

                <table style={{ display: "inline-block" }}>
                    <tbody>
                        <tr>
                            <td>
                                <span className="poll-action-button">
                                    <Button onClick={() => {
                                        returnHome()
                                    }}>Back</Button>
                                </span></td>
                            <td>

                                <span className="poll-action-button">
                                    <PollActionMenu
                                        inviteTeamToPoll={() => this.props.inviteTeamToPoll(poll)}
                                        closePoll={() => this.closePoll(poll)}
                                        deletePoll={() => this.props.deletePollButtonClick(poll)}
                                    />
                                </span>

                            </td>
                            <td>

                                <span className="poll-action-button">
                                    <Button onClick={() => {
                                        let required = poll.questions.map((q) => q.id);
                                        let actual = Object.keys(this.answer.answers);
                                        let missing = required.filter((id) => actual.indexOf(id) === -1);
                                        if (missing.length == 0) {
                                            submitPoll(poll, this.answer)
                                        }
                                    }}>Vote</Button>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="poll-action-link">
                    <Button
                        appearance="link"
                        onClick={() => {
                            showResults(poll);
                        }}>
                        View results
                    </Button>
                </div>
            </div>
        )
    }

}

export default Poll;