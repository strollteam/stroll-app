import React, {Component} from 'react';

class PollCard extends Component {

    constructor(props) {
        super(props);
        this.clickHandler = this.clickHandler.bind(this);
    }

    clickHandler(poll) {
        if(poll.open && !poll.submitted) {
            this.props.completePollButtonClick(poll);
        } else if(!poll.open || poll.submitted) {
            this.props.viewPollButtonClick(poll);
        } else {
            console.log("clickHandler found no action");
        }
    }

    render() {
        let poll = this.props.poll;

        return (
            <div className="poll-card-container">
                <div className="poll" onClick={()=>{this.clickHandler(poll)}}>
                    <span className="name">{poll.name}</span>
                </div>
            </div>
        )
    }

}

export default PollCard;