import React, {Component} from 'react';
import Spinner from '@atlaskit/spinner';

class SideBarLoading extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return <div className="panel loading"><Spinner size="xlarge"/></div>
    }

}

export default SideBarLoading;