import React, {Component} from 'react';

class ViewPollQuestionSelect extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let question = this.props.question;
        let answers = this.props.answers;

        let total = 0;
        let results = question.options.map((o) => {
            let count = 0;
            for (let i = 0, len = answers.length; i < len; i++) {
                if (answers[i].answers[question.id] === o.id) {
                    count++;
                    total++;
                }
            }
            return {
                id: o.id,
                label: o.value.en,
                count: count
            }
        });

        let colours = [
            '#0747A6',
            '#0052CC',
            '#0065FF',
            '#2684FF',
            '#4C9AFF'
        ]

        return (
            <div className="options">
                {results.map((r, i) => {
                    let per = 0;
                    if (total > 0) {
                        per = 100 * r.count / total;
                    }

                    let style = {
                        width: per + "%",
                        height: "1.2em",
                        backgroundColor: colours[i%colours.length]
                    };
                    return <div className="option-container" key={r.id}>
                        <div className="label">
                            <span className="individual-vote-count">
                                {r.count} vote{r.count !== 1 && <span>s</span>}
                            </span>
                            {r.label}
                        </div><br />
                        <div className="bar-container">
                            <div className="bar">
                                <div className="bar-filled-segment" style={style}>
                                </div>
                            </div>
                        </div>
                    </div>
                })}
            </div>)
    }

}

export default ViewPollQuestionSelect;



