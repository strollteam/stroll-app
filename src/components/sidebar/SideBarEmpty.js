import React, {Component} from 'react';
import Button from '@atlaskit/button';

class SideBarEmpty extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="panel empty">
                <div className="image"><img width="128px" src="/stroll-empty.png"/></div>
                <div className="title">Work Together</div>
                <div className="subtitle">Create a Stroll in this conversation to find out what your team is thinking!
                </div>
                <div className="buttons">
                    <Button id="addStrollButton"
                            onClick={() => {
                                this.props.addPollButtonClick();
                            }}>Add a Stroll</Button>
                </div>
            </div>
        );
    }
}

export default SideBarEmpty;