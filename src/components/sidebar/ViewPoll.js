import React, {Component} from 'react';
import ViewPollQuestion from './ViewPollQuestion';
import Button from '@atlaskit/button';
import TextField from '@atlaskit/field-text'
import PollActionMenu from './PollActionMenu';
import axios from 'axios';

class ViewPoll extends Component {

    constructor(props) {
        super(props);
        this.state = {
            decisionDialogVisible: false,
            decisionText: "",
            isDropdownOpen: false,
            open: this.props.poll.open
        };
    }

    handleKeyPress(e) {
        if (e.key == 'Enter') {
            this.createDecision();
        }
    }

    handleChange(e) {
        this.setState({decisionText: e.target.value});
    }

    createDecision() {
        let data = {
            decisionText: this.state.decisionText
        };

        axios.post('/polls/03820f20-a49f-11e7-a103-071b7c079463/decisions', data);
        this.setState({decisionDialogVisible: false});
    }

    showDecisionDialog() {
        this.setState({
            decisionDialogVisible: true,
            decisionText: `Regarding '${this.props.poll.questions[0].prompt}'`
        })
    }

    closePoll(poll) {
        let that = this;
        this.props.closePoll(poll)
            .then(function(result) {
                that.setState({open: false});
                that.showDecisionDialog();
            });
    }

    showVotingScreen(poll) {
        this.props.showVotingScreen(poll);
    }

    render() {
        let poll = this.props.poll;
        let answers = this.props.answers;
        let returnHome = this.props.returnHome;
        let closed = !this.state.open;

        var backButtonAction = returnHome;
        if (!this.props.poll.submitted) {
            // in this case 'Back' should go back to the voting screen
            backButtonAction = () => this.props.showVotingScreen(poll);
        }

        return (
            <div className="panel view">

                {closed && 
                    <span className="closed-notification">
                        This poll is closed for voting
                    </span>
                }
                {poll.questions.map((q) => {
                    return <ViewPollQuestion key={q.prompt}
                                             question={q}
                                             answers={answers}
                    />
                })}


                <div className="answers-display">
                    <span>{this.props.answers? this.props.answers.length : 0}</span><span> votes</span>
                </div>
                <div style={{marginTop: "16px", width: "100%"}}>
                    <table style={{display:"inline-block"}}>
                        <tbody>
                        <tr>
                            <td>
                                <Button id="return" onClick={backButtonAction}>Back</Button>
                            </td>
                            <td>
                                <PollActionMenu 
                                    showDecisionDialog={() => this.showDecisionDialog()}
                                    inviteTeamToPoll={() => this.props.inviteTeamToPoll(poll)}
                                    closePoll={() => this.closePoll(poll)}
                                    showVotingScreen={() => this.showVotingScreen(poll)}
                                    deletePoll={() => this.props.deletePollButtonClick(poll)}
                                />
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                { this.state.decisionDialogVisible &&
                <div onKeyPress={(e) => this.handleKeyPress(e)}>
                    <TextField
                        placeholder="decision text"
                        label="Create a decision"
                        value={this.state.decisionText}
                        autoFocus={true}
                        shouldFitContainer={true}
                        onChange={(e) => this.handleChange(e)}/>
                </div>
                }

            </div>
        )
    }

}

export default ViewPoll;
