import React, {Component} from 'react';
import SideBarContainer from '../containers/SideBarContainer';
import ReactDOM from 'react-dom';
import Store from './SideBarStore';
import Actions from './SideBarActions';
import {Provider} from "react-redux"

import axios from 'axios';

Store.dispatch(Actions.loadPolls());

socket.on('new_answer', function (answer) {
    Store.dispatch({type: 'NEW_ANSWER', payload: answer});
});

socket.on('new_poll', function (poll) {
    Store.dispatch({type: 'NEW_POLL', payload: poll});
});

function addPollButtonClick() {
    AP.dialog.open({
        key: "poll-dialog"
    });
}

function deletePollButtonClick(poll) {
    axios.delete("/polls/" + poll.id).then(function (response) {
        Store.dispatch(Actions.loadPolls());
    });
}

function editPollButtonClick(poll) {
    console.log("open edit poll dialog for poll", poll)
}

function viewPollButtonClick(poll) {
    Store.dispatch(Actions.viewPoll(poll));
}

function completePollButtonClick(poll) {
    Store.dispatch({'type': 'SELECT_POLL', payload: poll});
}

function submitPoll(poll,answer) {
    Store.dispatch({'type': 'SUBMIT_POLL_START', payload: poll});
    axios.post('/polls/' + answer.pollId + '/answers', answer)
        .then(function (answer) {
            Store.dispatch(Actions.viewPoll(poll));
        })
        .catch(function (e) {
            Store.dispatch(Actions.viewPoll(poll));
        })
}

function showVotingScreen(poll, userId) {
    console.log("view the poll", poll);
    Store.dispatch({'type': 'SELECT_POLL', payload: poll});
}

function inviteTeamToPoll(poll) {
    axios.post(`/polls/${poll.id}/invite`, poll);
}

function closePoll(poll) {
    return axios.post(`/polls/${poll.id}/close`, poll);
}

function returnHome() {
    Store.dispatch(Actions.loadPolls());
}

function showResults(poll) {
    Store.dispatch(Actions.viewPoll(poll))
}

ReactDOM.render(
    <Provider store={Store}>
        <SideBarContainer
            showResults={showResults}
            showVotingScreen={showVotingScreen}
            returnHome={returnHome}
            submitPoll={submitPoll}
            closePoll={closePoll}
            inviteTeamToPoll={inviteTeamToPoll}
            addPollButtonClick={addPollButtonClick}
            editPollButtonClick={editPollButtonClick}
            completePollButtonClick={completePollButtonClick}
            viewPollButtonClick={viewPollButtonClick}
            deletePollButtonClick={deletePollButtonClick}
        />
    </Provider>,
    document.getElementById("sidebar"));

