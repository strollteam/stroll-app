import {createStore, applyMiddleware} from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';

const reducer = (state = {}, action) => {
    let newState = Object.assign({}, state);
    // switch (action.type) {
    //     case 'SAVE_POLL_FINISHED':
    //         return newState;
    // }
    return newState;
};

const Store = createStore(reducer,
    {
        state: "add"
    },
    applyMiddleware(
        thunkMiddleware,
        promiseMiddleware(),
        logger
    ));

export default Store;