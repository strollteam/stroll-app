import {createStore, applyMiddleware} from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import thunkMiddleware from 'redux-thunk';
import logger from 'redux-logger';

const reducer = (state = {}, action) => {
    let newState = Object.assign({}, state);

    switch (action.type) {
        case 'LOAD_POLLS_FINISHED':
            return {...state, polls: action.payload, state: 'list'};
        case 'SELECT_POLL':
            return {...state, current: action.payload, state: 'selected'};

        case 'SUBMIT_POLL_START':
            return {...state,
                polls: state.polls.map((poll) => {
                    return poll.id === action.payload.id ?
                        {...poll, submitted: true} :
                        poll
                }),
                state: 'loading'};
        case 'SUBMIT_POLL_FINISHED':
            return {...state, state: 'loading'};

        case 'VIEW_POLL':
            return {
                ...state, 
                current: state.polls.filter(poll => poll.id === action.payload.id)[0]
            };

        case 'NEW_ANSWER':
            let newAnswer = action.payload;
            if(newState.answers && newState.current && newState.current.id == newAnswer.pollId) {
                newState.answers = [...newState.answers, action.payload];
            }
            return newState;
        case 'NEW_POLL':
            if(newState.polls) {
                newState.polls = [...newState.polls, action.payload];
            }
            return newState;

        case 'LOAD_ANSWERS_START':
            return {...state, state: 'loading'};
        case 'LOAD_ANSWERS_FINISHED':
            return {...state, state: 'view', answers: action.payload};
        default:
            return state;
    }
};

const Store = createStore(reducer,
    {
        state: "loading"
    },
    applyMiddleware(
        thunkMiddleware,
        promiseMiddleware(),
        logger
    ));

export default Store;