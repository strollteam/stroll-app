import React, {Component} from 'react';
import DialogContainer from '../containers/DialogContainer';
import ReactDOM from 'react-dom';
import Store from './DialogStore';
import Actions from './DialogActions';
import {Provider} from "react-redux"

function savePoll(poll) {
    Store.dispatch(Actions.savePoll(poll));
}

ReactDOM.render(
    <Provider store={Store}>
        <DialogContainer
            savePoll={savePoll}
        />
    </Provider>,
    document.getElementById("dialog"));

