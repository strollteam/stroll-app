import axios from 'axios';

const SideBarActions = {
    loadPolls: function() {
        return function (dispatch) {
            dispatch({'type': 'LOAD_POLLS_START'});
            axios.get("/polls")
                .then((response) => {
                    dispatch({'type': 'LOAD_POLLS_FINISHED', payload: response.data})
                })
                .catch((err) => {
                    dispatch({'type': "LOAD_POLLS_ERROR", payload: err})
                })
        }
    },
    viewPoll: function(poll) {
        return function (dispatch) {
            dispatch({'type': 'VIEW_POLL', payload: poll});
            dispatch({'type': 'LOAD_ANSWERS_START'});
            axios.get("/polls/"+poll.id+"/answers")
                .then((response) => {
                    dispatch({'type': 'LOAD_ANSWERS_FINISHED', payload: response.data})
                })
                .catch((err) => {
                    dispatch({'type': "LOAD_ANSWERS_ERROR", payload: err})
                })
        }
    }
};
export default SideBarActions;