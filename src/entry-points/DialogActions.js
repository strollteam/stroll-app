import axios from 'axios';

const DialogActions = {
    savePoll: function (poll) {
        return function (dispatch) {
            dispatch({'type': 'SAVE_POLL_START'});
            axios.post('/polls', poll)
                .then(function (response) {
                    dispatch({'type': 'SAVE_POLL_FINISHED'});
                    AP.broadcast("createpoll-finished");
                    AP.dialog.close();
                })
                .catch(function (error) {
                    dispatch({'type': 'SAVE_POLL_ERROR', payload: error});
                    AP.broadcast("createpoll-finished");
                    AP.dialog.close();
                });
        }
    }
};

export default DialogActions;