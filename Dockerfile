FROM node:6-alpine

COPY . /opt/service/
WORKDIR /opt/service

EXPOSE 8080

RUN apk add --update \
    python \
    py-pip \
    build-base \
  && pip install virtualenv \
  && pip install --upgrade pip \
  && rm -rf /var/cache/apk/*

RUN npm install -g grunt

RUN grunt webpack:build

ENTRYPOINT ["grunt", "prod"]

