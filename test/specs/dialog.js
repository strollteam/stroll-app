const assert = require('chai').assert;
const axios = require('axios');

describe('Dialog', function () {

    before(function(done){
        axios.post('/test/flushdb')
            .then(function () {
                done();
            })
            .catch(function (e) {
                done();
            })
    });

    it('respond with an error with no jwt information', function () {
        browser.url('http://localhost:8080/dialog');
        assert.equal(browser.getText("html"), "Missing JWT");
    });

    it('Dialog defaults to three options', function () {
        browser.url('http://localhost:8080/test.html');
        browser.waitForExist('iframe#dialog');
        browser.frame(browser.element('iframe#dialog').value);

        assert.equal(browser.elements(".choice-field").value.length, 3);
    });

    it('Delete option removes field', function () {
        browser.url('http://localhost:8080/test.html');
        browser.waitForExist('iframe#dialog');
        browser.frame(browser.element('iframe#dialog').value);

        browser.click("#choice_0_delete_button");
        assert.equal(browser.elements(".choice-field").value.length, 2);
    });

    it('No delete for final option', function () {
        browser.url('http://localhost:8080/test.html');
        browser.waitForExist('iframe#dialog');
        browser.frame(browser.element('iframe#dialog').value);

        browser.click("#choice_0_delete_button");
        browser.click("#choice_0_delete_button");
        assert.equal(browser.elements(".choice-field").value.length, 1);
        assert.equal(browser.elements(".delete_button").value.length, 0);
    });

});