const assert = require('chai').assert;

const axios = require('axios');

describe('Sidebar', function () {

    before(function(done){
        axios.post('/test/flushdb')
            .then(function () {
                done();
            })
            .catch(function (e) {
                done();
            })
    });

    it('respond with an error with no jwt information', function () {
        browser.url('http://localhost:8080/sidebar');
        assert.equal(browser.getText("html"),"Missing JWT");
    });

    it('Add a Stroll click opens dialog', function () {
        browser.url('http://localhost:8080/test.html');

        browser.waitForExist('iframe#sidebar');
        browser.frame(browser.element('iframe#sidebar').value);

        browser.click("#addStrollButton");
        browser.frame(null);
        assert.equal(browser.getText("#log-panel"),"dialog opened: {\"key\":\"poll-dialog\"}");
    });

});
