(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.host = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _util = _dereq_("./util");

    var _util2 = _interopRequireDefault(_util);

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
            }
        }return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
        };
    }();

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var PostMessage = function () {
        function PostMessage(data) {
            _classCallCheck(this, PostMessage);

            var d = data || {};
            this._registerListener(d.listenOn);
        }

        _createClass(PostMessage, [{
            key: "_registerListener",
            value: function _registerListener(listenOn) {
                if (!listenOn || !listenOn.addEventListener) {
                    listenOn = window;
                }
                listenOn.addEventListener("message", _util2.default._bind(this, this._receiveMessage), false);
            }
        }, {
            key: "_receiveMessage",
            value: function _receiveMessage(event) {

                var handler = this._messageHandlers[event.data.type],
                    extensionId = event.data.eid,
                    reg = void 0;

                if (extensionId && this._registeredExtensions) {
                    reg = this._registeredExtensions[extensionId];
                }

                if (!handler || !this._checkOrigin(event, reg)) {
                    return false;
                }

                handler.call(this, event, reg);
            }
        }]);

        return PostMessage;
    }();

    exports.default = PostMessage;

    module.exports = exports["default"];

},{"./util":2}],2:[function(_dereq_,module,exports){
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

    var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
        return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
    } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
    };

    var LOG_PREFIX = "[Simple-XDM] ";
    var nativeBind = Function.prototype.bind;
    var util = {
        locationOrigin: function locationOrigin() {
            if (!window.location.origin) {
                return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
            } else {
                return window.location.origin;
            }
        },
        randomString: function randomString() {
            return Math.floor(Math.random() * 1000000000).toString(16);
        },
        isString: function isString(str) {
            return typeof str === "string" || str instanceof String;
        },
        argumentsToArray: function argumentsToArray(arrayLike) {
            return Array.prototype.slice.call(arrayLike);
        },
        argumentNames: function argumentNames(fn) {
            return fn.toString().replace(/((\/\/.*$)|(\/\*[^]*?\*\/))/mg, '') // strip comments
                    .replace(/[^(]+\(([^)]*)[^]+/, '$1') // get signature
                    .match(/([^\s,]+)/g) || [];
        },
        hasCallback: function hasCallback(args) {
            var length = args.length;
            return length > 0 && typeof args[length - 1] === 'function';
        },
        error: function error(msg) {
            if (window.console && window.console.error) {
                var outputError = [];

                if (typeof msg === "string") {
                    outputError.push(LOG_PREFIX + msg);
                    outputError = outputError.concat(Array.prototype.slice.call(arguments, 1));
                } else {
                    outputError.push(LOG_PREFIX);
                    outputError = outputError.concat(Array.prototype.slice.call(arguments));
                }
                window.console.error.apply(null, outputError);
            }
        },
        warn: function warn(msg) {
            if (window.console) {
                console.warn(LOG_PREFIX + msg);
            }
        },
        log: function log(msg) {
            if (window.console) {
                window.console.log(LOG_PREFIX + msg);
            }
        },
        _bind: function _bind(thisp, fn) {
            if (nativeBind && fn.bind === nativeBind) {
                return fn.bind(thisp);
            }
            return function () {
                return fn.apply(thisp, arguments);
            };
        },
        throttle: function throttle(func, wait, context) {
            var previous = 0;
            return function () {
                var now = Date.now();
                if (now - previous > wait) {
                    previous = now;
                    func.apply(context, arguments);
                }
            };
        },
        each: function each(list, iteratee) {
            var length;
            var key;
            if (list) {
                length = list.length;
                if (length != null && typeof list !== 'function') {
                    key = 0;
                    while (key < length) {
                        if (iteratee.call(list[key], key, list[key]) === false) {
                            break;
                        }
                        key += 1;
                    }
                } else {
                    for (key in list) {
                        if (list.hasOwnProperty(key)) {
                            if (iteratee.call(list[key], key, list[key]) === false) {
                                break;
                            }
                        }
                    }
                }
            }
        },
        extend: function extend(dest) {
            var args = arguments;
            var srcs = [].slice.call(args, 1, args.length);
            srcs.forEach(function (source) {
                if ((typeof source === "undefined" ? "undefined" : _typeof(source)) === "object") {
                    Object.getOwnPropertyNames(source).forEach(function (name) {
                        dest[name] = source[name];
                    });
                }
            });
            return dest;
        },
        sanitizeStructuredClone: function sanitizeStructuredClone(object) {
            var whiteList = [Boolean, String, Date, RegExp, Blob, File, FileList, ArrayBuffer];
            var blackList = [Error, Node];
            var warn = util.warn;
            var visitedObjects = [];

            function _clone(value) {
                if (typeof value === 'function') {
                    warn("A function was detected and removed from the message.");
                    return null;
                }

                if (blackList.some(function (t) {
                        if (value instanceof t) {
                            warn(t.name + " object was detected and removed from the message.");
                            return true;
                        }
                        return false;
                    })) {
                    return {};
                }

                if (value && (typeof value === "undefined" ? "undefined" : _typeof(value)) === 'object' && whiteList.every(function (t) {
                        return !(value instanceof t);
                    })) {
                    var newValue = void 0;

                    if (Array.isArray(value)) {
                        newValue = value.map(function (element) {
                            return _clone(element);
                        });
                    } else {
                        if (visitedObjects.indexOf(value) > -1) {
                            warn("A circular reference was detected and removed from the message.");
                            return null;
                        }

                        visitedObjects.push(value);

                        newValue = {};

                        for (var name in value) {
                            if (value.hasOwnProperty(name)) {
                                var clonedValue = _clone(value[name]);
                                if (clonedValue !== null) {
                                    newValue[name] = clonedValue;
                                }
                            }
                        }

                        visitedObjects.pop();
                    }
                    return newValue;
                }
                return value;
            }

            return _clone(object);
        }
    };

    exports.default = util;

    module.exports = exports["default"];

},{}],3:[function(_dereq_,module,exports){
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _xdmrpc = _dereq_("./xdmrpc");

    var _xdmrpc2 = _interopRequireDefault(_xdmrpc);

    var _util = _dereq_("../common/util");

    var _util2 = _interopRequireDefault(_util);

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
            }
        }return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
        };
    }();

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    var Connect = function () {
        function Connect() {
            _classCallCheck(this, Connect);

            this._xdm = new _xdmrpc2.default();
        }

        /**
         * Send a message to iframes matching the targetSpec. This message is added to
         *  a message queue for delivery to ensure the message is received if an iframe
         *  has not yet loaded
         *
         * @param type The name of the event type
         * @param targetSpec The spec to match against extensions when sending this event
         * @param event The event payload
         * @param callback A callback to be executed when the remote iframe calls its callback
         */

        _createClass(Connect, [{
            key: 'dispatch',
            value: function dispatch(type, targetSpec, event, callback) {
                this._xdm.queueEvent(type, targetSpec, event, callback);
                return this.getExtensions(targetSpec);
            }

            /**
             * Send a message to iframes matching the targetSpec immediately. This message will
             *  only be sent to iframes that are already open, and will not be delivered if none
             *  are currently open.
             *
             * @param type The name of the event type
             * @param targetSpec The spec to match against extensions when sending this event
             * @param event The event payload
             */

        }, {
            key: 'broadcast',
            value: function broadcast(type, targetSpec, event) {
                this._xdm.dispatch(type, targetSpec, event, null, null);
                return this.getExtensions(targetSpec);
            }
        }, {
            key: '_createId',
            value: function _createId(extension) {
                if (!extension.addon_key || !extension.key) {
                    throw Error('Extensions require addon_key and key');
                }
                return extension.addon_key + '__' + extension.key + '__' + _util2.default.randomString();
            }
            /**
             * Creates a new iframed module, without actually creating the DOM element.
             * The iframe attributes are passed to the 'setupCallback', which is responsible for creating
             * the DOM element and returning the window reference.
             *
             * @param extension The extension definition. Example:
             *   {
    *     addon_key: 'my-addon',
    *     key: 'my-module',
    *     url: 'https://example.com/my-module',
    *     options: {
    *         autoresize: false,
    *         hostOrigin: 'https://connect-host.example.com/'
    *     }
    *   }
             *
             * @param initCallback The optional initCallback is called when the bridge between host and iframe is established.
             **/

        }, {
            key: 'create',
            value: function create(extension, initCallback) {
                var extension_id = this.registerExtension(extension, initCallback);
                var options = extension.options || {};

                var data = {
                    extension_id: extension_id,
                    api: this._xdm.getApiSpec(),
                    origin: _util2.default.locationOrigin(),
                    options: options
                };

                return {
                    id: extension_id,
                    name: JSON.stringify(data),
                    src: extension.url
                };
            }
        }, {
            key: 'registerRequestNotifier',
            value: function registerRequestNotifier(callback) {
                this._xdm.registerRequestNotifier(callback);
            }
        }, {
            key: 'registerExtension',
            value: function registerExtension(extension, initCallback, unloadCallback) {
                var extension_id = this._createId(extension);
                this._xdm.registerExtension(extension_id, {
                    extension: extension,
                    initCallback: initCallback,
                    unloadCallback: unloadCallback
                });
                return extension_id;
            }
        }, {
            key: 'registerKeyListener',
            value: function registerKeyListener(extension_id, key, modifiers, callback) {
                this._xdm.registerKeyListener(extension_id, key, modifiers, callback);
            }
        }, {
            key: 'unregisterKeyListener',
            value: function unregisterKeyListener(extension_id, key, modifiers, callback) {
                this._xdm.unregisterKeyListener(extension_id, key, modifiers, callback);
            }
        }, {
            key: 'registerClickHandler',
            value: function registerClickHandler(callback) {
                this._xdm.registerClickHandler(callback);
            }
        }, {
            key: 'unregisterClickHandler',
            value: function unregisterClickHandler() {
                this._xdm.unregisterClickHandler();
            }
        }, {
            key: 'defineModule',
            value: function defineModule(moduleName, module, options) {
                this._xdm.defineAPIModule(module, moduleName, options);
            }
        }, {
            key: 'defineGlobals',
            value: function defineGlobals(module) {
                this._xdm.defineAPIModule(module);
            }
        }, {
            key: 'getExtensions',
            value: function getExtensions(filter) {
                return this._xdm.getRegisteredExtensions(filter);
            }
        }, {
            key: 'unregisterExtension',
            value: function unregisterExtension(filter) {
                return this._xdm.unregisterExtension(filter);
            }
        }, {
            key: 'returnsPromise',
            value: function returnsPromise(wrappedMethod) {
                wrappedMethod.returnsPromise = true;
            }
        }]);

        return Connect;
    }();

    exports.default = Connect;

    module.exports = exports['default'];

},{"../common/util":2,"./xdmrpc":5}],4:[function(_dereq_,module,exports){
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _connect = _dereq_('./connect');

    var _connect2 = _interopRequireDefault(_connect);

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

    exports.default = new _connect2.default();

    module.exports = exports['default'];

},{"./connect":3}],5:[function(_dereq_,module,exports){
    "use strict";

    Object.defineProperty(exports, "__esModule", {
        value: true
    });

    var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

    var _util = _dereq_("../common/util");

    var _util2 = _interopRequireDefault(_util);

    var _postmessage = _dereq_("../common/postmessage");

    var _postmessage2 = _interopRequireDefault(_postmessage);

    function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

    var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
        return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
    } : function (obj) {
        return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
    };

    var _createClass = function () {
        function defineProperties(target, props) {
            for (var i = 0; i < props.length; i++) {
                var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ("value" in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
            }
        }return function (Constructor, protoProps, staticProps) {
            if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
        };
    }();

    function _classCallCheck(instance, Constructor) {
        if (!(instance instanceof Constructor)) {
            throw new TypeError("Cannot call a class as a function");
        }
    }

    function _possibleConstructorReturn(self, call) {
        if (!self) {
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }return call && ((typeof call === "undefined" ? "undefined" : _typeof2(call)) === "object" || typeof call === "function") ? call : self;
    }

    function _inherits(subClass, superClass) {
        if (typeof superClass !== "function" && superClass !== null) {
            throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof2(superClass)));
        }subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } });if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
    }

    /**
     * Postmessage format:
     *
     * Initialization
     * --------------
     * {
*   type: 'init',
*   eid: 'my-addon__my-module-xyz'  // the extension identifier, unique across iframes
* }
     *
     * Request
     * -------
     * {
*   type: 'req',
*   eid: 'my-addon__my-module-xyz',  // the extension identifier, unique for iframe
*   mid: 'xyz',  // a unique message identifier, required for callbacks
*   mod: 'cookie',  // the module name
*   fn: 'read',  // the method name
*   args: [arguments]  // the method arguments
* }
     *
     * Response
     * --------
     * {
*   type: 'resp'
*   eid: 'my-addon__my-module-xyz',  // the extension identifier, unique for iframe
*   mid: 'xyz',  // a unique message identifier, obtained from the request
*   args: [arguments]  // the callback arguments
* }
     *
     * Event
     * -----
     * {
*   type: 'evt',
*   etyp: 'some-event',
*   evnt: { ... }  // the event data
*   mid: 'xyz', // a unique message identifier for the event
* }
     **/

    var VALID_EVENT_TIME_MS = 30000; //30 seconds

    var XDMRPC = function (_PostMessage) {
        _inherits(XDMRPC, _PostMessage);

        _createClass(XDMRPC, [{
            key: '_padUndefinedArguments',
            value: function _padUndefinedArguments(array, length) {
                return array.length >= length ? array : array.concat(new Array(length - array.length));
            }
        }]);

        function XDMRPC(config) {
            _classCallCheck(this, XDMRPC);

            config = config || {};

            var _this = _possibleConstructorReturn(this, (XDMRPC.__proto__ || Object.getPrototypeOf(XDMRPC)).call(this, config));

            _this._registeredExtensions = config.extensions || {};
            _this._registeredAPIModules = {};
            _this._registeredAPIModules._globals = {};
            _this._pendingCallbacks = {};
            _this._keycodeCallbacks = {};
            _this._clickHandler = null;
            _this._pendingEvents = {};
            _this._messageHandlers = {
                init: _this._handleInit,
                req: _this._handleRequest,
                resp: _this._handleResponse,
                broadcast: _this._handleBroadcast,
                event_query: _this._handleEventQuery,
                key_triggered: _this._handleKeyTriggered,
                addon_clicked: _this._handleAddonClick,
                get_host_offset: _this._getHostOffset,
                unload: _this._handleUnload,
                sub: _this._handleSubInit
            };
            return _this;
        }

        _createClass(XDMRPC, [{
            key: '_verifyAPI',
            value: function _verifyAPI(event, reg) {
                var untrustedTargets = event.data.targets;
                if (!untrustedTargets) {
                    return;
                }
                var trustedSpec = this.getApiSpec();
                var tampered = false;

                function check(trusted, untrusted) {
                    Object.getOwnPropertyNames(untrusted).forEach(function (name) {
                        if (_typeof(untrusted[name]) === 'object' && trusted[name]) {
                            check(trusted[name], untrusted[name]);
                        } else {
                            if (untrusted[name] === 'parent' && trusted[name]) {
                                tampered = true;
                            }
                        }
                    });
                }
                check(trustedSpec, untrustedTargets);
                event.source.postMessage({
                    type: 'api_tamper',
                    tampered: tampered
                }, reg.extension.url);
            }
        }, {
            key: '_handleInit',
            value: function _handleInit(event, reg) {
                this._registeredExtensions[reg.extension_id].source = event.source;
                if (reg.initCallback) {
                    reg.initCallback(event.data.eid);
                    delete reg.initCallback;
                }
                if (event.data.targets) {
                    this._verifyAPI(event, reg);
                }
            }
            // postMessage method to do registerExtension

        }, {
            key: '_handleSubInit',
            value: function _handleSubInit(event, reg) {
                this.registerExtension(event.data.ext.id, {
                    extension: event.data.ext
                });
            }
        }, {
            key: '_getHostOffset',
            value: function _getHostOffset(event, _window) {
                var hostWindow = event.source;
                var hostFrameOffset = null;
                var windowReference = _window || window; // For testing

                if (windowReference === windowReference.top && typeof windowReference.getHostOffsetFunctionOverride === 'function') {
                    hostFrameOffset = windowReference.getHostOffsetFunctionOverride(hostWindow);
                }

                if (typeof hostFrameOffset !== 'number') {
                    hostFrameOffset = 0;
                    // Find the closest frame that has the same origin as event source
                    while (!this._hasSameOrigin(hostWindow)) {
                        // Climb up the iframe tree 1 layer
                        hostFrameOffset++;
                        hostWindow = hostWindow.parent;
                    }
                }

                event.source.postMessage({
                    hostFrameOffset: hostFrameOffset
                }, event.origin);
            }
        }, {
            key: '_hasSameOrigin',
            value: function _hasSameOrigin(window) {
                if (window === window.top) {
                    return true;
                }

                try {
                    // Try set & read a variable on the given window
                    // If we can successfully read the value then it means the given window has the same origin
                    // as the window that is currently executing the script
                    var testVariableName = 'test_var_' + Math.random().toString(16).substr(2);
                    window[testVariableName] = true;
                    return window[testVariableName];
                } catch (e) {
                    // A exception will be thrown if the windows doesn't have the same origin
                }

                return false;
            }
        }, {
            key: '_handleResponse',
            value: function _handleResponse(event) {
                var data = event.data;
                var pendingCallback = this._pendingCallbacks[data.mid];
                if (pendingCallback) {
                    delete this._pendingCallbacks[data.mid];
                    pendingCallback.apply(window, data.args);
                }
            }
        }, {
            key: 'registerRequestNotifier',
            value: function registerRequestNotifier(cb) {
                this._registeredRequestNotifier = cb;
            }
        }, {
            key: '_handleRequest',
            value: function _handleRequest(event, reg) {
                function sendResponse() {
                    var args = _util2.default.sanitizeStructuredClone(_util2.default.argumentsToArray(arguments));
                    event.source.postMessage({
                        mid: event.data.mid,
                        type: 'resp',
                        forPlugin: true,
                        args: args
                    }, reg.extension.url);
                }

                var data = event.data;
                var module = this._registeredAPIModules[data.mod];
                var extension = this.getRegisteredExtensions(reg.extension)[0];
                if (module) {
                    var fnName = data.fn;
                    if (data._cls) {
                        var Cls = module[data._cls];
                        var ns = data.mod + '-' + data._cls + '-';
                        sendResponse._id = data._id;
                        if (fnName === 'constructor') {
                            if (!Cls._construct) {
                                Cls.constructor.prototype._destroy = function () {
                                    delete this._context._proxies[ns + this._id];
                                };
                                Cls._construct = function () {
                                    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                                        args[_key] = arguments[_key];
                                    }

                                    var inst = new (Function.prototype.bind.apply(Cls.constructor, [null].concat(args)))();
                                    var callback = args[args.length - 1];
                                    inst._id = callback._id;
                                    inst._context = callback._context;
                                    inst._context._proxies[ns + inst._id] = inst;
                                    return inst;
                                };
                            }
                            module = Cls;
                            fnName = '_construct';
                        } else {
                            module = extension._proxies[ns + data._id];
                        }
                    }
                    var method = module[fnName];
                    if (method) {
                        var methodArgs = data.args;
                        var padLength = method.length - 1;
                        if (fnName === '_construct') {
                            padLength = module.constructor.length - 1;
                        }
                        sendResponse._context = extension;
                        methodArgs = this._padUndefinedArguments(methodArgs, padLength);
                        methodArgs.push(sendResponse);
                        var promiseResult = method.apply(module, methodArgs);

                        if (method.returnsPromise) {
                            if (!promiseResult || !(promiseResult instanceof Promise)) {
                                sendResponse('Defined module method did not return a promise.');
                            } else {
                                promiseResult.then(function (result) {
                                    sendResponse(undefined, result);
                                }).catch(function (err) {
                                    err = err instanceof Error ? err.message : err;
                                    sendResponse(err);
                                });
                            }
                        }

                        if (this._registeredRequestNotifier) {
                            this._registeredRequestNotifier.call(null, {
                                module: data.mod,
                                fn: data.fn,
                                type: data.type,
                                addon_key: reg.extension.addon_key,
                                key: reg.extension.key,
                                extension_id: reg.extension_id
                            });
                        }
                    }
                }
            }
        }, {
            key: '_handleBroadcast',
            value: function _handleBroadcast(event, reg) {
                var event_data = event.data;
                var targetSpec = function targetSpec(r) {
                    return r.extension.addon_key === reg.extension.addon_key && r.extension_id !== reg.extension_id;
                };
                this.dispatch(event_data.etyp, targetSpec, event_data.evnt, null, null);
            }
        }, {
            key: '_handleKeyTriggered',
            value: function _handleKeyTriggered(event, reg) {
                var eventData = event.data;
                var keycodeEntry = this._keycodeKey(eventData.keycode, eventData.modifiers, reg.extension_id);
                var listeners = this._keycodeCallbacks[keycodeEntry];
                if (listeners) {
                    listeners.forEach(function (listener) {
                        listener.call(null, {
                            addon_key: reg.extension.addon_key,
                            key: reg.extension.key,
                            extension_id: reg.extension_id,
                            keycode: eventData.keycode,
                            modifiers: eventData.modifiers
                        });
                    }, this);
                }
            }
        }, {
            key: 'defineAPIModule',
            value: function defineAPIModule(module, moduleName) {
                moduleName = moduleName || '_globals';
                this._registeredAPIModules[moduleName] = _util2.default.extend({}, this._registeredAPIModules[moduleName] || {}, module);
                return this._registeredAPIModules;
            }
        }, {
            key: '_fullKey',
            value: function _fullKey(targetSpec) {
                var key = targetSpec.addon_key || 'global';
                if (targetSpec.key) {
                    key = key + '@@' + targetSpec.key;
                }

                return key;
            }
        }, {
            key: 'queueEvent',
            value: function queueEvent(type, targetSpec, event, callback) {
                var loaded_frame,
                    targets = this._findRegistrations(targetSpec);

                loaded_frame = targets.some(function (target) {
                    return target.registered_events !== undefined;
                }, this);

                if (loaded_frame) {
                    this.dispatch(type, targetSpec, event, callback);
                } else {
                    this._pendingEvents[this._fullKey(targetSpec)] = {
                        type: type,
                        targetSpec: targetSpec,
                        event: event,
                        callback: callback,
                        time: new Date().getTime(),
                        uid: _util2.default.randomString()
                    };
                }
            }
        }, {
            key: '_handleEventQuery',
            value: function _handleEventQuery(message, extension) {
                var _this2 = this;

                var executed = {};
                var now = new Date().getTime();
                var keys = Object.keys(this._pendingEvents);
                keys.forEach(function (index) {
                    var element = _this2._pendingEvents[index];
                    var eventIsValid = now - element.time <= VALID_EVENT_TIME_MS;
                    var isSameTarget = !element.targetSpec || _this2._findRegistrations(element.targetSpec).length !== 0;

                    if (isSameTarget && element.targetSpec.key) {
                        isSameTarget = element.targetSpec.addon_key === extension.extension.addon_key && element.targetSpec.key === extension.extension.key;
                    }

                    if (eventIsValid && isSameTarget) {
                        executed[index] = element;
                        element.targetSpec = element.targetSpec || {};
                        _this2.dispatch(element.type, element.targetSpec, element.event, element.callback, message.source);
                    } else if (!eventIsValid) {
                        delete _this2._pendingEvents[index];
                    }
                });

                this._registeredExtensions[extension.extension_id].registered_events = message.data.args;

                return executed;
            }
        }, {
            key: '_handleUnload',
            value: function _handleUnload(event, reg) {
                if (!reg) {
                    return;
                }

                if (reg.extension_id && this._registeredExtensions[reg.extension_id]) {
                    delete this._registeredExtensions[reg.extension_id].source;
                }

                if (reg.unloadCallback) {
                    reg.unloadCallback(event.data.eid);
                }
            }
        }, {
            key: 'dispatch',
            value: function dispatch(type, targetSpec, event, callback, source) {
                function sendEvent(reg, evnt) {
                    if (reg.source && reg.source.postMessage) {
                        var mid;
                        if (callback) {
                            mid = _util2.default.randomString();
                            this._pendingCallbacks[mid] = callback;
                        }

                        reg.source.postMessage({
                            type: 'evt',
                            mid: mid,
                            etyp: type,
                            evnt: evnt
                        }, reg.extension.url);
                    }
                }

                var registrations = this._findRegistrations(targetSpec || {});
                registrations.forEach(function (reg) {
                    if (source && !reg.source) {
                        reg.source = source;
                    }

                    if (reg.source) {
                        _util2.default._bind(this, sendEvent)(reg, event);
                    }
                }, this);
            }
        }, {
            key: '_findRegistrations',
            value: function _findRegistrations(targetSpec) {
                var _this3 = this;

                if (this._registeredExtensions.length === 0) {
                    _util2.default.error('no registered extensions', this._registeredExtensions);
                    return [];
                }
                var keys = Object.getOwnPropertyNames(targetSpec);
                var registrations = Object.getOwnPropertyNames(this._registeredExtensions).map(function (key) {
                    return _this3._registeredExtensions[key];
                });

                if (targetSpec instanceof Function) {
                    return registrations.filter(targetSpec);
                } else {
                    return registrations.filter(function (reg) {
                        return keys.every(function (key) {
                            return reg.extension[key] === targetSpec[key];
                        });
                    });
                }
            }
        }, {
            key: 'registerExtension',
            value: function registerExtension(extension_id, data) {
                data._proxies = {};
                data.extension_id = extension_id;
                this._registeredExtensions[extension_id] = data;
            }
        }, {
            key: '_keycodeKey',
            value: function _keycodeKey(key, modifiers, extension_id) {
                var code = key;

                if (modifiers) {
                    if (typeof modifiers === "string") {
                        modifiers = [modifiers];
                    }
                    modifiers.sort();
                    modifiers.forEach(function (modifier) {
                        code += '$$' + modifier;
                    }, this);
                }

                return code + '__' + extension_id;
            }
        }, {
            key: 'registerKeyListener',
            value: function registerKeyListener(extension_id, key, modifiers, callback) {
                if (typeof modifiers === "string") {
                    modifiers = [modifiers];
                }
                var reg = this._registeredExtensions[extension_id];
                var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);
                if (!this._keycodeCallbacks[keycodeEntry]) {
                    this._keycodeCallbacks[keycodeEntry] = [];
                    reg.source.postMessage({
                        type: 'key_listen',
                        keycode: key,
                        modifiers: modifiers,
                        action: 'add'
                    }, reg.extension.url);
                }
                this._keycodeCallbacks[keycodeEntry].push(callback);
            }
        }, {
            key: 'unregisterKeyListener',
            value: function unregisterKeyListener(extension_id, key, modifiers, callback) {
                var keycodeEntry = this._keycodeKey(key, modifiers, extension_id);
                var potentialCallbacks = this._keycodeCallbacks[keycodeEntry];
                var reg = this._registeredExtensions[extension_id];

                if (potentialCallbacks) {
                    if (callback) {
                        var index = potentialCallbacks.indexOf(callback);
                        this._keycodeCallbacks[keycodeEntry].splice(index, 1);
                    } else {
                        delete this._keycodeCallbacks[keycodeEntry];
                    }
                    if (reg.source && reg.source.postMessage) {
                        reg.source.postMessage({
                            type: 'key_listen',
                            keycode: key,
                            modifiers: modifiers,
                            action: 'remove'
                        }, reg.extension.url);
                    }
                }
            }
        }, {
            key: 'registerClickHandler',
            value: function registerClickHandler(callback) {
                if (typeof callback !== 'function') {
                    throw new Error('callback must be a function');
                }
                if (this._clickHandler !== null) {
                    throw new Error('ClickHandler already registered');
                }
                this._clickHandler = callback;
            }
        }, {
            key: '_handleAddonClick',
            value: function _handleAddonClick(event, reg) {
                if (typeof this._clickHandler === 'function') {
                    this._clickHandler({
                        addon_key: reg.extension.addon_key,
                        key: reg.extension.key,
                        extension_id: reg.extension_id
                    });
                }
            }
        }, {
            key: 'unregisterClickHandler',
            value: function unregisterClickHandler() {
                this._clickHandler = null;
            }
        }, {
            key: 'getApiSpec',
            value: function getApiSpec() {
                var that = this;
                function createModule(moduleName) {
                    var module = that._registeredAPIModules[moduleName];
                    if (!module) {
                        throw new Error("unregistered API module: " + moduleName);
                    }
                    function getModuleDefinition(mod) {
                        return Object.getOwnPropertyNames(mod).reduce(function (accumulator, memberName) {
                            var member = mod[memberName];
                            switch (typeof member === 'undefined' ? 'undefined' : _typeof(member)) {
                                case 'function':
                                    accumulator[memberName] = {
                                        args: _util2.default.argumentNames(member),
                                        returnsPromise: member.returnsPromise || false
                                    };
                                    break;
                                case 'object':
                                    if (member.hasOwnProperty('constructor')) {
                                        accumulator[memberName] = getModuleDefinition(member);
                                    }
                                    break;
                            }

                            return accumulator;
                        }, {});
                    }
                    return getModuleDefinition(module);
                }
                return Object.getOwnPropertyNames(this._registeredAPIModules).reduce(function (accumulator, moduleName) {
                    accumulator[moduleName] = createModule(moduleName);
                    return accumulator;
                }, {});
            }

            // validate origin of postMessage

        }, {
            key: '_checkOrigin',
            value: function _checkOrigin(event, reg) {
                var no_source_types = ['init', 'event_query'];
                var isNoSourceType = reg && !reg.source && no_source_types.indexOf(event.data.type) > -1;
                var sourceTypeMatches = reg && event.source === reg.source;
                var hasExtensionUrl = reg && reg.extension.url.indexOf(event.origin) === 0;
                var isValidOrigin = hasExtensionUrl && (isNoSourceType || sourceTypeMatches);

                // get_host_offset fires before init
                if (event.data.type === 'get_host_offset' && window === window.top) {
                    isValidOrigin = true;
                }

                // check undefined for chromium (Issue 395010)
                if (event.data.type === 'unload' && (sourceTypeMatches || event.source === undefined)) {
                    isValidOrigin = true;
                }

                if (!isValidOrigin) {
                    _util2.default.log("Failed to validate origin: " + event.origin);
                }
                return isValidOrigin;
            }
        }, {
            key: 'getRegisteredExtensions',
            value: function getRegisteredExtensions(filter) {
                if (filter) {
                    return this._findRegistrations(filter);
                }
                return this._registeredExtensions;
            }
        }, {
            key: 'unregisterExtension',
            value: function unregisterExtension(filter) {
                var registrations = this._findRegistrations(filter);
                if (registrations.length !== 0) {
                    registrations.forEach(function (registration) {
                        var _this4 = this;

                        var keys = Object.keys(this._pendingEvents);
                        keys.forEach(function (index) {
                            var element = _this4._pendingEvents[index];
                            var targetSpec = element.targetSpec || {};

                            if (targetSpec.addon_key === registration.extension.addon_key) {
                                delete _this4._pendingEvents[index];
                            }
                        });

                        delete this._registeredExtensions[registration.extension_id];
                    }, this);
                }
            }
        }]);

        return XDMRPC;
    }(_postmessage2.default);

    exports.default = XDMRPC;

    module.exports = exports['default'];

},{"../common/postmessage":1,"../common/util":2}]},{},[4])(4)
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvY29tbW9uL3Bvc3RtZXNzYWdlLmpzIiwic3JjL2NvbW1vbi91dGlsLmpzIiwic3JjL2hvc3QvY29ubmVjdC5qcyIsInNyYy9ob3N0L2luZGV4LmpzIiwic3JjL2hvc3QveGRtcnBjLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7O0FDQUEsQUFBTyxBQUFVOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0ksQUFDWCwwQkFFSjt1QkFBQSxBQUFZLE1BQU07MEJBQ2hCOztRQUFJLElBQUksUUFBUixBQUFnQixBQUNoQjtTQUFBLEFBQUssa0JBQWtCLEVBQXZCLEFBQXlCLEFBQzFCOzs7OztzQyxBQUVpQixVQUFVLEFBQzFCO1VBQUcsQ0FBQSxBQUFDLFlBQVksQ0FBQyxTQUFqQixBQUEwQixrQkFBa0IsQUFDMUM7bUJBQUEsQUFBVyxBQUNaO0FBQ0Q7ZUFBQSxBQUFTLGlCQUFULEFBQTBCLFdBQVcsZUFBQSxBQUFLLE1BQUwsQUFBVyxNQUFNLEtBQXRELEFBQXFDLEFBQXNCLGtCQUEzRCxBQUE2RSxBQUM5RTs7OztvQyxBQUVnQixPQUFPLEFBRXRCOztVQUFJLFVBQVUsS0FBQSxBQUFLLGlCQUFpQixNQUFBLEFBQU0sS0FBMUMsQUFBYyxBQUFpQztVQUM3QyxjQUFjLE1BQUEsQUFBTSxLQUR0QixBQUMyQjtVQUN6QixXQUZGLEFBSUE7O1VBQUcsZUFBZSxLQUFsQixBQUF1Qix1QkFBc0IsQUFDM0M7Y0FBTSxLQUFBLEFBQUssc0JBQVgsQUFBTSxBQUEyQixBQUNsQztBQUVEOztVQUFJLENBQUEsQUFBQyxXQUFXLENBQUMsS0FBQSxBQUFLLGFBQUwsQUFBa0IsT0FBbkMsQUFBaUIsQUFBeUIsTUFBTSxBQUM5QztlQUFBLEFBQU8sQUFDUjtBQUVEOztjQUFBLEFBQVEsS0FBUixBQUFhLE1BQWIsQUFBbUIsT0FBbkIsQUFBMEIsQUFDM0I7Ozs7O0FBSUg7O2tCQUFBLEFBQWU7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsQ2YsSUFBTSxhQUFOLEFBQW1CO0FBQ25CLElBQU0sYUFBYSxTQUFBLEFBQVMsVUFBNUIsQUFBc0M7QUFDdEMsSUFBTTtBQUFPLDRDQUVNLEFBQ2Y7UUFBSSxDQUFDLE9BQUEsQUFBTyxTQUFaLEFBQXFCLFFBQVEsQUFDM0I7YUFBTyxPQUFBLEFBQU8sU0FBUCxBQUFnQixXQUFoQixBQUEyQixPQUFPLE9BQUEsQUFBTyxTQUF6QyxBQUFrRCxZQUFZLE9BQUEsQUFBTyxTQUFQLEFBQWdCLE9BQU8sTUFBTSxPQUFBLEFBQU8sU0FBcEMsQUFBNkMsT0FBbEgsQUFBTyxBQUFrSCxBQUMxSDtBQUZELFdBRU8sQUFDTDthQUFPLE9BQUEsQUFBTyxTQUFkLEFBQXVCLEFBQ3hCO0FBQ0Y7QUFSVSxBQVVYO0FBVlcsd0NBVUksQUFDYjtXQUFPLEtBQUEsQUFBSyxNQUFNLEtBQUEsQUFBSyxXQUFoQixBQUEyQixZQUEzQixBQUF1QyxTQUE5QyxBQUFPLEFBQWdELEFBQ3hEO0FBWlUsQUFjWDtBQWRXLDhCQUFBLEFBY0YsS0FBSyxBQUNaO1dBQU8sT0FBQSxBQUFPLFFBQVAsQUFBZSxZQUFZLGVBQWxDLEFBQWlELEFBQ2xEO0FBaEJVLEFBa0JYO0FBbEJXLDhDQUFBLEFBa0JNLFdBQVcsQUFDMUI7V0FBTyxNQUFBLEFBQU0sVUFBTixBQUFnQixNQUFoQixBQUFzQixLQUE3QixBQUFPLEFBQTJCLEFBQ25DO0FBcEJVLEFBc0JYO0FBdEJXLHdDQUFBLEFBc0JHLElBQUksQUFDaEI7Y0FBTyxBQUFHLFdBQUgsQUFDSixRQURJLEFBQ0ksaUNBREosQUFDcUMsSUFEckMsQUFDeUM7QUFEekMsS0FBQSxBQUVKLFFBRkksQUFFSSxzQkFGSixBQUUwQixNQUYxQixBQUVnQztLQUZoQyxBQUdKLE1BSEksQUFHRSxpQkFIVCxBQUcwQixBQUMzQjtBQTNCVSxBQTZCWDtBQTdCVyxvQ0FBQSxBQTZCQyxNQUFNLEFBQ2hCO1FBQUksU0FBUyxLQUFiLEFBQWtCLEFBQ2xCO1dBQU8sU0FBQSxBQUFTLEtBQUssT0FBTyxLQUFLLFNBQVosQUFBTyxBQUFjLE9BQTFDLEFBQWlELEFBQ2xEO0FBaENVLEFBa0NYO0FBbENXLHdCQUFBLEFBa0NMLEtBQUssQUFDVDtRQUFJLE9BQUEsQUFBTyxXQUFXLE9BQUEsQUFBTyxRQUE3QixBQUFxQyxPQUFPLEFBQzFDO1VBQUksY0FBSixBQUFrQixBQUVsQjs7VUFBSSxPQUFBLEFBQU8sUUFBWCxBQUFtQixVQUFVLEFBQzNCO29CQUFBLEFBQVksS0FBSyxhQUFqQixBQUE4QixBQUM5QjtzQkFBYyxZQUFBLEFBQVksT0FBTyxNQUFBLEFBQU0sVUFBTixBQUFnQixNQUFoQixBQUFzQixLQUF0QixBQUEyQixXQUE1RCxBQUFjLEFBQW1CLEFBQXNDLEFBQ3hFO0FBSEQsYUFHTyxBQUNMO29CQUFBLEFBQVksS0FBWixBQUFpQixBQUNqQjtzQkFBYyxZQUFBLEFBQVksT0FBTyxNQUFBLEFBQU0sVUFBTixBQUFnQixNQUFoQixBQUFzQixLQUF2RCxBQUFjLEFBQW1CLEFBQTJCLEFBQzdEO0FBQ0Q7YUFBQSxBQUFPLFFBQVAsQUFBZSxNQUFmLEFBQXFCLE1BQXJCLEFBQTJCLE1BQTNCLEFBQWlDLEFBQ2xDO0FBQ0Y7QUEvQ1UsQUFpRFg7QUFqRFcsc0JBQUEsQUFpRE4sS0FBSyxBQUNSO1FBQUksT0FBSixBQUFXLFNBQVMsQUFDbEI7Y0FBQSxBQUFRLEtBQUssYUFBYixBQUEwQixBQUMzQjtBQUNGO0FBckRVLEFBc0RYO0FBdERXLG9CQUFBLEFBc0RQLEtBQUssQUFDUDtRQUFHLE9BQUgsQUFBVSxTQUFTLEFBQ2pCO2FBQUEsQUFBTyxRQUFQLEFBQWUsSUFBSSxhQUFuQixBQUFnQyxBQUNqQztBQUNGO0FBMURVLEFBNERYO0FBNURXLHdCQUFBLEFBNERMLE9BNURLLEFBNERFLElBQUksQUFDZjtRQUFJLGNBQWMsR0FBQSxBQUFHLFNBQXJCLEFBQThCLFlBQVksQUFDeEM7YUFBTyxHQUFBLEFBQUcsS0FBVixBQUFPLEFBQVEsQUFDaEI7QUFDRDtXQUFPLFlBQVksQUFDakI7YUFBTyxHQUFBLEFBQUcsTUFBSCxBQUFTLE9BQWhCLEFBQU8sQUFBZ0IsQUFDeEI7QUFGRCxBQUdEO0FBbkVVLEFBcUVYO0FBckVXLDhCQUFBLEFBcUVGLE1BckVFLEFBcUVJLE1BckVKLEFBcUVVLFNBQVMsQUFDNUI7UUFBSSxXQUFKLEFBQWUsQUFDZjtXQUFPLFlBQVksQUFDakI7VUFBSSxNQUFNLEtBQVYsQUFBVSxBQUFLLEFBQ2Y7VUFBSSxNQUFBLEFBQU0sV0FBVixBQUFxQixNQUFNLEFBQ3pCO21CQUFBLEFBQVcsQUFDWDthQUFBLEFBQUssTUFBTCxBQUFXLFNBQVgsQUFBb0IsQUFDckI7QUFDRjtBQU5ELEFBT0Q7QUE5RVUsQUFnRlg7QUFoRlcsc0JBQUEsQUFnRk4sTUFoRk0sQUFnRkEsVUFBVSxBQUNuQjtRQUFBLEFBQUksQUFDSjtRQUFBLEFBQUksQUFDSjtRQUFBLEFBQUksTUFBTSxBQUNSO2VBQVMsS0FBVCxBQUFjLEFBQ2Q7VUFBSSxVQUFBLEFBQVUsUUFBUSxPQUFBLEFBQU8sU0FBN0IsQUFBc0MsWUFBWSxBQUNoRDtjQUFBLEFBQU0sQUFDTjtlQUFPLE1BQVAsQUFBYSxRQUFRLEFBQ25CO2NBQUksU0FBQSxBQUFTLEtBQUssS0FBZCxBQUFjLEFBQUssTUFBbkIsQUFBeUIsS0FBSyxLQUE5QixBQUE4QixBQUFLLFVBQXZDLEFBQWlELE9BQU8sQUFDdEQ7QUFDRDtBQUNEO2lCQUFBLEFBQU8sQUFDUjtBQUNGO0FBUkQsYUFRTyxBQUNMO2FBQUEsQUFBSyxPQUFMLEFBQVksTUFBTSxBQUNoQjtjQUFJLEtBQUEsQUFBSyxlQUFULEFBQUksQUFBb0IsTUFBTSxBQUM1QjtnQkFBSSxTQUFBLEFBQVMsS0FBSyxLQUFkLEFBQWMsQUFBSyxNQUFuQixBQUF5QixLQUFLLEtBQTlCLEFBQThCLEFBQUssVUFBdkMsQUFBaUQsT0FBTyxBQUN0RDtBQUNEO0FBQ0Y7QUFDRjtBQUNGO0FBQ0Y7QUFDRjtBQXZHVSxBQXlHWDtBQXpHVywwQkFBQSxBQXlHSixNQUFNLEFBQ1g7UUFBSSxPQUFKLEFBQVcsQUFDWDtRQUFJLE9BQU8sR0FBQSxBQUFHLE1BQUgsQUFBUyxLQUFULEFBQWMsTUFBZCxBQUFvQixHQUFHLEtBQWxDLEFBQVcsQUFBNEIsQUFDdkM7U0FBQSxBQUFLLFFBQVEsVUFBQSxBQUFVLFFBQVEsQUFDN0I7VUFBSSxRQUFBLEFBQU8sK0NBQVAsQUFBTyxhQUFYLEFBQXNCLFVBQVUsQUFDOUI7ZUFBQSxBQUFPLG9CQUFQLEFBQTJCLFFBQTNCLEFBQW1DLFFBQVEsVUFBQSxBQUFVLE1BQU0sQUFDekQ7ZUFBQSxBQUFLLFFBQVEsT0FBYixBQUFhLEFBQU8sQUFDckI7QUFGRCxBQUdEO0FBQ0Y7QUFORCxBQU9BO1dBQUEsQUFBTyxBQUNSO0FBcEhVLEFBc0hYO0FBdEhXLDREQUFBLEFBc0hhLFFBQVEsQUFDOUI7UUFBTSxZQUFZLENBQUEsQUFBQyxTQUFELEFBQVUsUUFBVixBQUFrQixNQUFsQixBQUF3QixRQUF4QixBQUFnQyxNQUFoQyxBQUFzQyxNQUF0QyxBQUE0QyxVQUE5RCxBQUFrQixBQUFzRCxBQUN4RTtRQUFNLFlBQVksQ0FBQSxBQUFDLE9BQW5CLEFBQWtCLEFBQVEsQUFDMUI7UUFBTSxPQUFPLEtBQWIsQUFBa0IsQUFDbEI7UUFBSSxpQkFBSixBQUFxQixBQUVyQjs7YUFBQSxBQUFTLE9BQVQsQUFBZ0IsT0FBTyxBQUNyQjtVQUFJLE9BQUEsQUFBTyxVQUFYLEFBQXFCLFlBQVksQUFDL0I7YUFBQSxBQUFLLEFBQ0w7ZUFBQSxBQUFPLEFBQ1I7QUFFRDs7b0JBQUksQUFBVSxLQUFLLGFBQUssQUFDdEI7WUFBSSxpQkFBSixBQUFxQixHQUFHLEFBQ3RCO2VBQVEsRUFBUixBQUFVLE9BQ1Y7aUJBQUEsQUFBTyxBQUNSO0FBQ0Q7ZUFBQSxBQUFPLEFBQ1I7QUFORCxBQUFJLE9BQUEsR0FNQSxBQUNGO2VBQUEsQUFBTyxBQUNSO0FBRUQ7O1VBQUksU0FBUyxRQUFBLEFBQU8sOENBQVAsQUFBTyxZQUFoQixBQUEwQixzQkFBWSxBQUFVLE1BQU0sYUFBQTtlQUFLLEVBQUUsaUJBQVAsQUFBSyxBQUFtQjtBQUFsRixBQUEwQyxPQUFBLEdBQTZDLEFBQ3JGO1lBQUksZ0JBQUosQUFFQTs7WUFBSSxNQUFBLEFBQU0sUUFBVixBQUFJLEFBQWMsUUFBUSxBQUN4QjsyQkFBVyxBQUFNLElBQUksVUFBQSxBQUFVLFNBQVMsQUFDdEM7bUJBQU8sT0FBUCxBQUFPLEFBQU8sQUFDZjtBQUZELEFBQVcsQUFHWixXQUhZO0FBRGIsZUFJTyxBQUNMO2NBQUksZUFBQSxBQUFlLFFBQWYsQUFBdUIsU0FBUyxDQUFwQyxBQUFxQyxHQUFHLEFBQ3RDO2lCQUFBLEFBQUssQUFDTDttQkFBQSxBQUFPLEFBQ1I7QUFFRDs7eUJBQUEsQUFBZSxLQUFmLEFBQW9CLEFBRXBCOztxQkFBQSxBQUFXLEFBRVg7O2VBQUssSUFBTCxBQUFTLFFBQVQsQUFBaUIsT0FBTyxBQUN0QjtnQkFBSSxNQUFBLEFBQU0sZUFBVixBQUFJLEFBQXFCLE9BQU8sQUFDOUI7a0JBQUksY0FBYyxPQUFPLE1BQXpCLEFBQWtCLEFBQU8sQUFBTSxBQUMvQjtrQkFBSSxnQkFBSixBQUFvQixNQUFNLEFBQ3hCO3lCQUFBLEFBQVMsUUFBVCxBQUFpQixBQUNsQjtBQUNGO0FBQ0Y7QUFFRDs7eUJBQUEsQUFBZSxBQUNoQjtBQUNEO2VBQUEsQUFBTyxBQUNSO0FBQ0Q7YUFBQSxBQUFPLEFBQ1I7QUFFRDs7V0FBTyxPQUFQLEFBQU8sQUFBTyxBQUNmO0FBOUtILEFBQWEsQUFpTGI7QUFqTGEsQUFFWDs7a0JBK0tGLEFBQWU7Ozs7Ozs7Ozs7O0FDbkxmLEFBQU8sQUFBWTs7OztBQUNuQixBQUFPLEFBQVc7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7SSxBQUNaLHNCQUVKO3FCQUFjOzBCQUNaOztTQUFBLEFBQUssT0FBTCxBQUFZLEFBQUksQUFDakI7QUFFRDs7Ozs7Ozs7Ozs7Ozs7OzZCLEFBVVMsTSxBQUFNLFksQUFBWSxPLEFBQU8sVUFBVSxBQUMxQztXQUFBLEFBQUssS0FBTCxBQUFVLFdBQVYsQUFBcUIsTUFBckIsQUFBMkIsWUFBM0IsQUFBdUMsT0FBdkMsQUFBOEMsQUFDOUM7YUFBTyxLQUFBLEFBQUssY0FBWixBQUFPLEFBQW1CLEFBQzNCO0FBRUQ7Ozs7Ozs7Ozs7Ozs7OzhCLEFBU1UsTSxBQUFNLFksQUFBWSxPQUFPLEFBQ2pDO1dBQUEsQUFBSyxLQUFMLEFBQVUsU0FBVixBQUFtQixNQUFuQixBQUF5QixZQUF6QixBQUFxQyxPQUFyQyxBQUE0QyxNQUE1QyxBQUFrRCxBQUNsRDthQUFPLEtBQUEsQUFBSyxjQUFaLEFBQU8sQUFBbUIsQUFDM0I7Ozs7OEIsQUFFUyxXQUFXLEFBQ25CO1VBQUcsQ0FBQyxVQUFELEFBQVcsYUFBYSxDQUFDLFVBQTVCLEFBQXNDLEtBQUksQUFDeEM7Y0FBTSxNQUFOLEFBQU0sQUFBTSxBQUNiO0FBQ0Q7YUFBTyxVQUFBLEFBQVUsWUFBVixBQUFzQixPQUFPLFVBQTdCLEFBQXVDLE1BQXZDLEFBQTZDLE9BQU8sZUFBM0QsQUFBMkQsQUFBTSxBQUNsRTtBQUNEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OzJCLEFBa0JPLFcsQUFBVyxjQUFjLEFBQzlCO1VBQUksZUFBZSxLQUFBLEFBQUssa0JBQUwsQUFBdUIsV0FBMUMsQUFBbUIsQUFBa0MsQUFDckQ7VUFBSSxVQUFVLFVBQUEsQUFBVSxXQUF4QixBQUFtQyxBQUVuQzs7VUFBSTtzQkFBTyxBQUNLLEFBQ2Q7YUFBSyxLQUFBLEFBQUssS0FGRCxBQUVKLEFBQVUsQUFDZjtnQkFBUSxlQUhDLEFBR0QsQUFBTSxBQUNkO2lCQUpGLEFBQVcsQUFJQSxBQUdYO0FBUFcsQUFDVDs7O1lBTUssQUFDRCxBQUNKO2NBQU0sS0FBQSxBQUFLLFVBRk4sQUFFQyxBQUFlLEFBQ3JCO2FBQUssVUFIUCxBQUFPLEFBR1UsQUFFbEI7QUFMUSxBQUNMOzs7OzRDLEFBTW9CLFVBQVUsQUFDaEM7V0FBQSxBQUFLLEtBQUwsQUFBVSx3QkFBVixBQUFrQyxBQUNuQzs7OztzQyxBQUVpQixXLEFBQVcsYyxBQUFjLGdCQUFnQixBQUN6RDtVQUFJLGVBQWUsS0FBQSxBQUFLLFVBQXhCLEFBQW1CLEFBQWUsQUFDbEM7V0FBQSxBQUFLLEtBQUwsQUFBVSxrQkFBVixBQUE0QjttQkFBYyxBQUM3QixBQUNYO3NCQUZ3QyxBQUUxQixBQUNkO3dCQUhGLEFBQTBDLEFBR3hCLEFBRWxCO0FBTDBDLEFBQ3hDO2FBSUYsQUFBTyxBQUNSOzs7O3dDLEFBRW1CLGMsQUFBYyxLLEFBQUssVyxBQUFXLFVBQVUsQUFDMUQ7V0FBQSxBQUFLLEtBQUwsQUFBVSxvQkFBVixBQUE4QixjQUE5QixBQUE0QyxLQUE1QyxBQUFpRCxXQUFqRCxBQUE0RCxBQUM3RDs7OzswQyxBQUVxQixjLEFBQWMsSyxBQUFLLFcsQUFBVyxVQUFVLEFBQzVEO1dBQUEsQUFBSyxLQUFMLEFBQVUsc0JBQVYsQUFBZ0MsY0FBaEMsQUFBOEMsS0FBOUMsQUFBbUQsV0FBbkQsQUFBOEQsQUFDL0Q7Ozs7eUMsQUFFb0IsVUFBVSxBQUM3QjtXQUFBLEFBQUssS0FBTCxBQUFVLHFCQUFWLEFBQStCLEFBQ2hDOzs7OzZDQUV3QixBQUN2QjtXQUFBLEFBQUssS0FBTCxBQUFVLEFBQ1g7Ozs7aUMsQUFFWSxZLEFBQVksUSxBQUFRLFNBQVMsQUFDeEM7V0FBQSxBQUFLLEtBQUwsQUFBVSxnQkFBVixBQUEwQixRQUExQixBQUFrQyxZQUFsQyxBQUE4QyxBQUMvQzs7OztrQyxBQUVhLFFBQVEsQUFDcEI7V0FBQSxBQUFLLEtBQUwsQUFBVSxnQkFBVixBQUEwQixBQUMzQjs7OztrQyxBQUVhLFFBQVEsQUFDcEI7YUFBTyxLQUFBLEFBQUssS0FBTCxBQUFVLHdCQUFqQixBQUFPLEFBQWtDLEFBQzFDOzs7O3dDLEFBRW1CLFFBQVEsQUFDMUI7YUFBTyxLQUFBLEFBQUssS0FBTCxBQUFVLG9CQUFqQixBQUFPLEFBQThCLEFBQ3RDOzs7O21DLEFBRWMsZUFBZSxBQUM1QjtvQkFBQSxBQUFjLGlCQUFkLEFBQStCLEFBQ2hDOzs7OztBQUlIOztrQkFBQSxBQUFlOzs7Ozs7Ozs7OztBQ25JZixBQUFPLEFBQWEsQUFDcEI7Ozs7OztrQkFBQSxBQUFlLEFBQUk7Ozs7Ozs7Ozs7Ozs7QUN1Q25CLEFBQU8sQUFBVzs7OztBQUNsQixBQUFPLEFBQWlCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBekN4Qjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTJDQSxJQUFJLHNCLEFBQUosQUFBMEIsT0FBTzs7SSxBQUUzQjs7Ozs7MkMsQUFDbUIsTyxBQUFPLFFBQU8sQUFDbkM7YUFBUSxNQUFBLEFBQU0sVUFBUCxBQUFpQixTQUFqQixBQUEyQixRQUFRLE1BQUEsQUFBTSxPQUFPLElBQUEsQUFBSSxNQUFNLFNBQVMsTUFBMUUsQUFBMEMsQUFBYSxBQUF5QixBQUNqRjtBQUVEOzs7a0JBQUEsQUFBWSxRQUFROzBCQUNsQjs7YUFBUyxVQURTLEFBQ2xCLEFBQW1COztnSEFERCxBQUVaLEFBQ047O1VBQUEsQUFBSyx3QkFBd0IsT0FBQSxBQUFPLGNBQXBDLEFBQWtELEFBQ2xEO1VBQUEsQUFBSyx3QkFBTCxBQUE2QixBQUM3QjtVQUFBLEFBQUssc0JBQUwsQUFBMkIsV0FBM0IsQUFBc0MsQUFDdEM7VUFBQSxBQUFLLG9CQUFMLEFBQXlCLEFBQ3pCO1VBQUEsQUFBSyxvQkFBTCxBQUF5QixBQUN6QjtVQUFBLEFBQUssZ0JBQUwsQUFBcUIsQUFDckI7VUFBQSxBQUFLLGlCQUFMLEFBQXNCLEFBQ3RCO1VBQUEsQUFBSztZQUNHLE1BRGdCLEFBQ1gsQUFDWDtXQUFLLE1BRmlCLEFBRVosQUFDVjtZQUFNLE1BSGdCLEFBR1gsQUFDWDtpQkFBVyxNQUpXLEFBSU4sQUFDaEI7bUJBQWEsTUFMUyxBQUtKLEFBQ2xCO3FCQUFlLE1BTk8sQUFNRixBQUNwQjtxQkFBZSxNQVBPLEFBT0YsQUFDcEI7dUJBQWlCLE1BUkssQUFRQSxBQUN0QjtjQUFRLE1BVGMsQUFTVCxBQUNiO1dBQUssTUFwQlcsQUFVbEIsQUFBd0IsQUFVWjtBQVZZLEFBQ3RCO1dBV0g7Ozs7OytCLEFBRVUsTyxBQUFPLEtBQUssQUFDckI7VUFBSSxtQkFBbUIsTUFBQSxBQUFNLEtBQTdCLEFBQWtDLEFBQ2xDO1VBQUcsQ0FBSCxBQUFJLGtCQUFrQixBQUNwQjtBQUNEO0FBQ0Q7VUFBSSxjQUFjLEtBQWxCLEFBQWtCLEFBQUssQUFDdkI7VUFBSSxXQUFKLEFBQWUsQUFFZjs7ZUFBQSxBQUFTLE1BQVQsQUFBZSxTQUFmLEFBQXdCLFdBQVUsQUFDaEM7ZUFBQSxBQUFPLG9CQUFQLEFBQTJCLFdBQTNCLEFBQXNDLFFBQVEsVUFBQSxBQUFTLE1BQUssQUFDMUQ7Y0FBRyxRQUFPLFVBQVAsQUFBTyxBQUFVLFdBQWpCLEFBQTJCLFlBQVksUUFBMUMsQUFBMEMsQUFBUSxPQUFPLEFBQ3ZEO2tCQUFNLFFBQU4sQUFBTSxBQUFRLE9BQU8sVUFBckIsQUFBcUIsQUFBVSxBQUNoQztBQUZELGlCQUVPLEFBQ0w7Z0JBQUcsVUFBQSxBQUFVLFVBQVYsQUFBb0IsWUFBWSxRQUFuQyxBQUFtQyxBQUFRLE9BQU8sQUFDaEQ7eUJBQUEsQUFBVyxBQUNaO0FBQ0Y7QUFDRjtBQVJELEFBU0Q7QUFDRDtZQUFBLEFBQU0sYUFBTixBQUFtQixBQUNuQjtZQUFBLEFBQU0sT0FBTixBQUFhO2NBQVksQUFDakIsQUFDTjtrQkFGRixBQUF5QixBQUViO0FBRmEsQUFDdkIsU0FFQyxJQUFBLEFBQUksVUFIUCxBQUdpQixBQUNsQjs7OztnQyxBQUVXLE8sQUFBTyxLQUFLLEFBQ3RCO1dBQUEsQUFBSyxzQkFBc0IsSUFBM0IsQUFBK0IsY0FBL0IsQUFBNkMsU0FBUyxNQUF0RCxBQUE0RCxBQUM1RDtVQUFJLElBQUosQUFBUSxjQUFjLEFBQ3BCO1lBQUEsQUFBSSxhQUFhLE1BQUEsQUFBTSxLQUF2QixBQUE0QixBQUM1QjtlQUFPLElBQVAsQUFBVyxBQUNaO0FBQ0Q7VUFBRyxNQUFBLEFBQU0sS0FBVCxBQUFjLFNBQVMsQUFDckI7YUFBQSxBQUFLLFdBQUwsQUFBZ0IsT0FBaEIsQUFBdUIsQUFDeEI7QUFDRjtBQUNEOzs7OzttQyxBQUNlLE8sQUFBTyxLQUFLLEFBQ3pCO1dBQUEsQUFBSyxrQkFBa0IsTUFBQSxBQUFNLEtBQU4sQUFBVyxJQUFsQyxBQUFzQzttQkFDekIsTUFBQSxBQUFNLEtBRG5CLEFBQTBDLEFBQ2xCLEFBRXpCO0FBSDJDLEFBQ3hDOzs7O21DLEFBSVcsTyxBQUFPO1VBQ2hCLGFBQWEsTUFBakIsQUFBdUIsQUFDdkI7VUFBSSxrQkFBSixBQUFzQixBQUN0QjtVQUFJLGtCQUFrQixXQUhPLEFBRzdCLEFBQWlDLE9BSEosQUFDN0IsQ0FFeUMsQUFFekM7O1VBQUksb0JBQW9CLGdCQUFwQixBQUFvQyxPQUFPLE9BQU8sZ0JBQVAsQUFBdUIsa0NBQXRFLEFBQXdHLFlBQVksQUFDbEg7MEJBQWtCLGdCQUFBLEFBQWdCLDhCQUFsQyxBQUFrQixBQUE4QyxBQUNqRTtBQUVEOztVQUFJLE9BQUEsQUFBTyxvQkFBWCxBQUErQixVQUFVLEFBQ3ZDOzBCQUFBLEFBQWtCLEFBQ2xCO0FBQ0E7ZUFBTyxDQUFDLEtBQUEsQUFBSyxlQUFiLEFBQVEsQUFBb0IsYUFBYSxBQUN2QztBQUNBO0FBQ0E7dUJBQWEsV0FBYixBQUF3QixBQUN6QjtBQUNGO0FBRUQ7O1lBQUEsQUFBTSxPQUFOLEFBQWE7eUJBQWIsQUFBeUIsQUFDTjtBQURNLEFBQ3ZCLFNBQ0MsTUFGSCxBQUVTLEFBQ1Y7Ozs7bUMsQUFFYyxRQUFRLEFBQ3JCO1VBQUksV0FBVyxPQUFmLEFBQXNCLEtBQUssQUFDekI7ZUFBQSxBQUFPLEFBQ1I7QUFFRDs7VUFBSSxBQUNGO0FBQ0E7QUFDQTtBQUNBO1lBQUksbUJBQW1CLGNBQWMsS0FBQSxBQUFLLFNBQUwsQUFBYyxTQUFkLEFBQXVCLElBQXZCLEFBQTJCLE9BQWhFLEFBQXFDLEFBQWtDLEFBQ3ZFO2VBQUEsQUFBTyxvQkFBUCxBQUEyQixBQUMzQjtlQUFPLE9BQVAsQUFBTyxBQUFPLEFBQ2Y7QUFQRCxRQU9FLE9BQUEsQUFBTSxHQUFHLEFBQ1Q7QUFDRDtBQUVEOzthQUFBLEFBQU8sQUFDUjs7OztvQyxBQUVlLE9BQU8sQUFDckI7VUFBSSxPQUFPLE1BQVgsQUFBaUIsQUFDakI7VUFBSSxrQkFBa0IsS0FBQSxBQUFLLGtCQUFrQixLQUE3QyxBQUFzQixBQUE0QixBQUNsRDtVQUFBLEFBQUksaUJBQWlCLEFBQ25CO2VBQU8sS0FBQSxBQUFLLGtCQUFrQixLQUE5QixBQUFPLEFBQTRCLEFBQ25DO3dCQUFBLEFBQWdCLE1BQWhCLEFBQXNCLFFBQVEsS0FBOUIsQUFBbUMsQUFDcEM7QUFDRjs7Ozs0QyxBQUV1QixJQUFJLEFBQzFCO1dBQUEsQUFBSyw2QkFBTCxBQUFrQyxBQUNuQzs7OzttQyxBQUVjLE8sQUFBTyxLQUFLLEFBQ3pCO2VBQUEsQUFBUyxlQUFlLEFBQ3RCO1lBQUksT0FBTyxlQUFBLEFBQU0sd0JBQXdCLGVBQUEsQUFBTSxpQkFBL0MsQUFBVyxBQUE4QixBQUF1QixBQUNoRTtjQUFBLEFBQU0sT0FBTixBQUFhO2VBQ04sTUFBQSxBQUFNLEtBRFksQUFDUCxBQUNoQjtnQkFGdUIsQUFFakIsQUFDTjtxQkFIdUIsQUFHWixBQUNYO2dCQUpGLEFBQXlCLEFBSWpCO0FBSmlCLEFBQ3ZCLFdBSUMsSUFBQSxBQUFJLFVBTFAsQUFLaUIsQUFDbEI7QUFFRDs7VUFBSSxPQUFPLE1BQVgsQUFBaUIsQUFDakI7VUFBSSxTQUFTLEtBQUEsQUFBSyxzQkFBc0IsS0FBeEMsQUFBYSxBQUFnQyxBQUM3QztVQUFNLFlBQVksS0FBQSxBQUFLLHdCQUF3QixJQUE3QixBQUFpQyxXQUFuRCxBQUFrQixBQUE0QyxBQUM5RDtVQUFBLEFBQUksUUFBUSxBQUNWO1lBQUksU0FBUyxLQUFiLEFBQWtCLEFBQ2xCO1lBQUksS0FBSixBQUFTLE1BQU0sQUFDYjtjQUFNLE1BQU0sT0FBTyxLQUFuQixBQUFZLEFBQVksQUFDeEI7Y0FBTSxLQUFLLEtBQUEsQUFBSyxNQUFMLEFBQVcsTUFBTSxLQUFqQixBQUFzQixPQUFqQyxBQUF3QyxBQUN4Qzt1QkFBQSxBQUFhLE1BQU0sS0FBbkIsQUFBd0IsQUFDeEI7Y0FBSSxXQUFKLEFBQWUsZUFBZSxBQUM1QjtnQkFBSSxDQUFDLElBQUwsQUFBUyxZQUFZLEFBQ25CO2tCQUFBLEFBQUksWUFBSixBQUFnQixVQUFoQixBQUEwQixXQUFXLFlBQVcsQUFDOUM7dUJBQU8sS0FBQSxBQUFLLFNBQUwsQUFBYyxTQUFTLEtBQUssS0FBbkMsQUFBTyxBQUFpQyxBQUN6QztBQUZELEFBR0E7a0JBQUEsQUFBSSxhQUFhLFlBQWtCO2tEQUFOLEFBQU0sbURBQU47QUFBTSx5Q0FBQTtBQUNqQzs7b0JBQU0sMENBQVcsSUFBWCxBQUFlLDJCQUFyQixBQUFNLEFBQThCLEFBQ3BDO29CQUFNLFdBQVcsS0FBSyxLQUFBLEFBQUssU0FBM0IsQUFBaUIsQUFBbUIsQUFDcEM7cUJBQUEsQUFBSyxNQUFNLFNBQVgsQUFBb0IsQUFDcEI7cUJBQUEsQUFBSyxXQUFXLFNBQWhCLEFBQXlCLEFBQ3pCO3FCQUFBLEFBQUssU0FBTCxBQUFjLFNBQVMsS0FBSyxLQUE1QixBQUFpQyxPQUFqQyxBQUF3QyxBQUN4Qzt1QkFBQSxBQUFPLEFBQ1I7QUFQRCxBQVFEO0FBQ0Q7cUJBQUEsQUFBUyxBQUNUO3FCQUFBLEFBQVMsQUFDVjtBQWhCRCxpQkFnQk8sQUFDTDtxQkFBUyxVQUFBLEFBQVUsU0FBUyxLQUFLLEtBQWpDLEFBQVMsQUFBNkIsQUFDdkM7QUFDRjtBQUNEO1lBQUksU0FBUyxPQUFiLEFBQWEsQUFBTyxBQUNwQjtZQUFBLEFBQUksUUFBUSxBQUNWO2NBQUksYUFBYSxLQUFqQixBQUFzQixBQUN0QjtjQUFJLFlBQVksT0FBQSxBQUFPLFNBQXZCLEFBQWdDLEFBQ2hDO2NBQUcsV0FBSCxBQUFjLGNBQWMsQUFDMUI7d0JBQVksT0FBQSxBQUFPLFlBQVAsQUFBbUIsU0FBL0IsQUFBd0MsQUFDekM7QUFDRDt1QkFBQSxBQUFhLFdBQWIsQUFBd0IsQUFDeEI7dUJBQWEsS0FBQSxBQUFLLHVCQUFMLEFBQTRCLFlBQXpDLEFBQWEsQUFBd0MsQUFDckQ7cUJBQUEsQUFBVyxLQUFYLEFBQWdCLEFBQ2hCO2NBQU0sZ0JBQWdCLE9BQUEsQUFBTyxNQUFQLEFBQWEsUUFBbkMsQUFBc0IsQUFBcUIsQUFFM0M7O2NBQUksT0FBSixBQUFXLGdCQUFnQixBQUN6QjtnQkFBSSxDQUFBLEFBQUMsaUJBQWlCLEVBQUUseUJBQXhCLEFBQXNCLEFBQTJCLFVBQVUsQUFDekQ7MkJBQUEsQUFBYSxBQUNkO0FBRkQsbUJBRU8sQUFDTDs0QkFBQSxBQUFjLEtBQUssa0JBQVUsQUFDM0I7NkJBQUEsQUFBYSxXQUFiLEFBQXdCLEFBQ3pCO0FBRkQsaUJBQUEsQUFFRyxNQUFNLGVBQU8sQUFDZDtzQkFBTSxlQUFBLEFBQWUsUUFBUSxJQUF2QixBQUEyQixVQUFqQyxBQUEyQyxBQUMzQzs2QkFBQSxBQUFhLEFBQ2Q7QUFMRCxBQU1EO0FBQ0Y7QUFFRDs7Y0FBRyxLQUFILEFBQVEsNEJBQTRCLEFBQ2xDO2lCQUFBLEFBQUssMkJBQUwsQUFBZ0MsS0FBaEMsQUFBcUM7c0JBQzNCLEtBRGlDLEFBQzVCLEFBQ2I7a0JBQUksS0FGcUMsQUFFaEMsQUFDVDtvQkFBTSxLQUhtQyxBQUc5QixBQUNYO3lCQUFXLElBQUEsQUFBSSxVQUowQixBQUloQixBQUN6QjttQkFBSyxJQUFBLEFBQUksVUFMZ0MsQUFLdEIsQUFDbkI7NEJBQWMsSUFOaEIsQUFBMkMsQUFNdkIsQUFFckI7QUFSNEMsQUFDekM7QUFRTDtBQUNGO0FBQ0Y7Ozs7cUMsQUFFZ0IsTyxBQUFPLEtBQUssQUFDM0I7VUFBSSxhQUFhLE1BQWpCLEFBQXVCLEFBQ3ZCO1VBQUksYUFBYSxTQUFiLEFBQWEsY0FBQTtlQUFLLEVBQUEsQUFBRSxVQUFGLEFBQVksY0FBYyxJQUFBLEFBQUksVUFBOUIsQUFBd0MsYUFBYSxFQUFBLEFBQUUsaUJBQWlCLElBQTdFLEFBQWlGO0FBQWxHLEFBQ0E7V0FBQSxBQUFLLFNBQVMsV0FBZCxBQUF5QixNQUF6QixBQUErQixZQUFZLFdBQTNDLEFBQXNELE1BQXRELEFBQTRELE1BQTVELEFBQWtFLEFBQ25FOzs7O3dDLEFBRW1CLE8sQUFBTyxLQUFLLEFBQzlCO1VBQUksWUFBWSxNQUFoQixBQUFzQixBQUN0QjtVQUFJLGVBQWUsS0FBQSxBQUFLLFlBQVksVUFBakIsQUFBMkIsU0FBUyxVQUFwQyxBQUE4QyxXQUFXLElBQTVFLEFBQW1CLEFBQTZELEFBQ2hGO1VBQUksWUFBWSxLQUFBLEFBQUssa0JBQXJCLEFBQWdCLEFBQXVCLEFBQ3ZDO1VBQUEsQUFBRyxXQUFXLEFBQ1o7a0JBQUEsQUFBVSxRQUFRLFVBQUEsQUFBQyxVQUFhLEFBQzlCO21CQUFBLEFBQVMsS0FBVCxBQUFjO3VCQUNELElBQUEsQUFBSSxVQURHLEFBQ08sQUFDekI7aUJBQUssSUFBQSxBQUFJLFVBRlMsQUFFQyxBQUNuQjswQkFBYyxJQUhJLEFBR0EsQUFDbEI7cUJBQVMsVUFKUyxBQUlDLEFBQ25CO3VCQUFXLFVBTGIsQUFBb0IsQUFLRyxBQUV4QjtBQVBxQixBQUNsQjtBQUZKLFdBQUEsQUFRRyxBQUNKO0FBQ0Y7Ozs7b0MsQUFFZSxRLEFBQVEsWUFBVyxBQUNqQzttQkFBYSxjQUFiLEFBQTJCLEFBQzNCO1dBQUEsQUFBSyxzQkFBTCxBQUEyQixjQUFjLGVBQUEsQUFBTSxPQUFOLEFBQWEsSUFBSSxLQUFBLEFBQUssc0JBQUwsQUFBMkIsZUFBNUMsQUFBMkQsSUFBcEcsQUFBeUMsQUFBK0QsQUFDeEc7YUFBTyxLQUFQLEFBQVksQUFDYjs7Ozs2QixBQUVRLFlBQVcsQUFDbEI7VUFBSSxNQUFNLFdBQUEsQUFBVyxhQUFyQixBQUFrQyxBQUNsQztVQUFHLFdBQUgsQUFBYyxLQUFJLEFBQ2hCO2NBQUEsQUFBUyxhQUFRLFdBQWpCLEFBQTRCLEFBQzdCO0FBRUQ7O2FBQUEsQUFBTyxBQUNSOzs7OytCLEFBRVUsTSxBQUFNLFksQUFBWSxPLEFBQU8sVUFBVSxBQUM1QztVQUFBLEFBQUk7VUFDRixVQUFVLEtBQUEsQUFBSyxtQkFEakIsQUFDWSxBQUF3QixBQUVwQzs7NkJBQWUsQUFBUSxLQUFLLFVBQUEsQUFBQyxRQUFXLEFBQ3RDO2VBQU8sT0FBQSxBQUFPLHNCQUFkLEFBQW9DLEFBQ3JDO0FBRmMsT0FBQSxFQUFmLEFBQWUsQUFFWixBQUVIOztVQUFBLEFBQUcsY0FBYSxBQUNkO2FBQUEsQUFBSyxTQUFMLEFBQWMsTUFBZCxBQUFvQixZQUFwQixBQUFnQyxPQUFoQyxBQUF1QyxBQUN4QztBQUZELGFBRU8sQUFDTDthQUFBLEFBQUssZUFBZSxLQUFBLEFBQUssU0FBekIsQUFBb0IsQUFBYztnQkFBZSxBQUUvQztzQkFGK0MsQUFHL0M7aUJBSCtDLEFBSS9DO29CQUorQyxBQUsvQztnQkFBTSxJQUFBLEFBQUksT0FMcUMsQUFLekMsQUFBVyxBQUNqQjtlQUFLLGVBTlAsQUFBaUQsQUFNMUMsQUFBTSxBQUVkO0FBUmtELEFBQy9DO0FBUUw7Ozs7c0MsQUFFaUIsUyxBQUFTLFdBQVc7bUJBQ3BDOztVQUFJLFdBQUosQUFBZSxBQUNmO1VBQUksTUFBTSxJQUFBLEFBQUksT0FBZCxBQUFVLEFBQVcsQUFDckI7VUFBSSxPQUFPLE9BQUEsQUFBTyxLQUFLLEtBQXZCLEFBQVcsQUFBaUIsQUFDNUI7V0FBQSxBQUFLLFFBQVEsVUFBQSxBQUFDLE9BQVUsQUFDdEI7WUFBSSxVQUFVLE9BQUEsQUFBSyxlQUFuQixBQUFjLEFBQW9CLEFBQ2xDO1lBQUksZUFBZ0IsTUFBTSxRQUFQLEFBQWUsUUFBbEMsQUFBMkMsQUFDM0M7WUFBSSxlQUFlLENBQUMsUUFBRCxBQUFTLGNBQWMsT0FBQSxBQUFLLG1CQUFtQixRQUF4QixBQUFnQyxZQUFoQyxBQUE0QyxXQUF0RixBQUFpRyxBQUVqRzs7WUFBSSxnQkFBZ0IsUUFBQSxBQUFRLFdBQTVCLEFBQXVDLEtBQUssQUFDMUM7eUJBQWdCLFFBQUEsQUFBUSxXQUFSLEFBQW1CLGNBQWMsVUFBQSxBQUFVLFVBQTNDLEFBQXFELGFBQWEsUUFBQSxBQUFRLFdBQVIsQUFBbUIsUUFBUSxVQUFBLEFBQVUsVUFBdkgsQUFBaUksQUFDbEk7QUFFRDs7WUFBSSxnQkFBSixBQUFvQixjQUFjLEFBQ2hDO21CQUFBLEFBQVMsU0FBVCxBQUFrQixBQUNsQjtrQkFBQSxBQUFRLGFBQWEsUUFBQSxBQUFRLGNBQTdCLEFBQTJDLEFBQzNDO2lCQUFBLEFBQUssU0FBUyxRQUFkLEFBQXNCLE1BQU0sUUFBNUIsQUFBb0MsWUFBWSxRQUFoRCxBQUF3RCxPQUFPLFFBQS9ELEFBQXVFLFVBQVUsUUFBakYsQUFBeUYsQUFDMUY7QUFKRCxlQUlPLElBQUksQ0FBSixBQUFLLGNBQWMsQUFDeEI7aUJBQU8sT0FBQSxBQUFLLGVBQVosQUFBTyxBQUFvQixBQUM1QjtBQUNGO0FBaEJELEFBa0JBOztXQUFBLEFBQUssc0JBQXNCLFVBQTNCLEFBQXFDLGNBQXJDLEFBQW1ELG9CQUFvQixRQUFBLEFBQVEsS0FBL0UsQUFBb0YsQUFFcEY7O2FBQUEsQUFBTyxBQUNSOzs7O2tDLEFBRWEsTyxBQUFPLEtBQUssQUFDeEI7VUFBSSxDQUFKLEFBQUssS0FBSyxBQUNSO0FBQ0Q7QUFFRDs7VUFBSSxJQUFBLEFBQUksZ0JBQWdCLEtBQUEsQUFBSyxzQkFBc0IsSUFBbkQsQUFBd0IsQUFBK0IsZUFBZSxBQUNwRTtlQUFPLEtBQUEsQUFBSyxzQkFBc0IsSUFBM0IsQUFBK0IsY0FBdEMsQUFBb0QsQUFDckQ7QUFFRDs7VUFBSSxJQUFKLEFBQVEsZ0JBQWdCLEFBQ3RCO1lBQUEsQUFBSSxlQUFlLE1BQUEsQUFBTSxLQUF6QixBQUE4QixBQUMvQjtBQUNGOzs7OzZCLEFBRVEsTSxBQUFNLFksQUFBWSxPLEFBQU8sVSxBQUFVLFFBQVEsQUFDbEQ7ZUFBQSxBQUFTLFVBQVQsQUFBbUIsS0FBbkIsQUFBd0IsTUFBTSxBQUM1QjtZQUFJLElBQUEsQUFBSSxVQUFVLElBQUEsQUFBSSxPQUF0QixBQUE2QixhQUFhLEFBQ3hDO2NBQUEsQUFBSSxBQUNKO2NBQUEsQUFBSSxVQUFVLEFBQ1o7a0JBQU0sZUFBTixBQUFNLEFBQU0sQUFDWjtpQkFBQSxBQUFLLGtCQUFMLEFBQXVCLE9BQXZCLEFBQThCLEFBQy9CO0FBRUQ7O2NBQUEsQUFBSSxPQUFKLEFBQVc7a0JBQVksQUFDZixBQUNOO2lCQUZxQixBQUVoQixBQUNMO2tCQUhxQixBQUdmLEFBQ047a0JBSkYsQUFBdUIsQUFJZjtBQUplLEFBQ3JCLGFBSUMsSUFBQSxBQUFJLFVBTFAsQUFLaUIsQUFDbEI7QUFDRjtBQUVEOztVQUFJLGdCQUFnQixLQUFBLEFBQUssbUJBQW1CLGNBQTVDLEFBQW9CLEFBQXNDLEFBQzFEO29CQUFBLEFBQWMsUUFBUSxVQUFBLEFBQVUsS0FBSyxBQUNuQztZQUFHLFVBQVUsQ0FBQyxJQUFkLEFBQWtCLFFBQU8sQUFDdkI7Y0FBQSxBQUFJLFNBQUosQUFBYSxBQUNkO0FBRUQ7O1lBQUksSUFBSixBQUFRLFFBQVEsQUFDZDt5QkFBQSxBQUFNLE1BQU4sQUFBWSxNQUFaLEFBQWtCLFdBQWxCLEFBQTZCLEtBQTdCLEFBQWtDLEFBQ25DO0FBQ0Y7QUFSRCxTQUFBLEFBUUcsQUFDSjs7Ozt1QyxBQUVrQixZQUFZO21CQUM3Qjs7VUFBRyxLQUFBLEFBQUssc0JBQUwsQUFBMkIsV0FBOUIsQUFBeUMsR0FBRSxBQUN6Qzt1QkFBQSxBQUFNLE1BQU4sQUFBWSw0QkFBNEIsS0FBeEMsQUFBNkMsQUFDN0M7ZUFBQSxBQUFPLEFBQ1I7QUFDRDtVQUFJLE9BQU8sT0FBQSxBQUFPLG9CQUFsQixBQUFXLEFBQTJCLEFBQ3RDO1VBQUksdUJBQWdCLEFBQU8sb0JBQW9CLEtBQTNCLEFBQWdDLHVCQUFoQyxBQUF1RCxJQUFJLFVBQUEsQUFBQyxLQUFRLEFBQ3RGO2VBQU8sT0FBQSxBQUFLLHNCQUFaLEFBQU8sQUFBMkIsQUFDbkM7QUFGRCxBQUFvQixBQUlwQixPQUpvQjs7VUFJaEIsc0JBQUosQUFBMEIsVUFBVSxBQUNsQztlQUFPLGNBQUEsQUFBYyxPQUFyQixBQUFPLEFBQXFCLEFBQzdCO0FBRkQsYUFFTyxBQUNMOzZCQUFPLEFBQWMsT0FBTyxVQUFBLEFBQVUsS0FBSyxBQUN6QztzQkFBTyxBQUFLLE1BQU0sVUFBQSxBQUFVLEtBQUssQUFDL0I7bUJBQU8sSUFBQSxBQUFJLFVBQUosQUFBYyxTQUFTLFdBQTlCLEFBQThCLEFBQVcsQUFDMUM7QUFGRCxBQUFPLEFBR1IsV0FIUTtBQURULEFBQU8sQUFLUixTQUxRO0FBTVY7Ozs7c0MsQUFFaUIsYyxBQUFjLE1BQU0sQUFDcEM7V0FBQSxBQUFLLFdBQUwsQUFBZ0IsQUFDaEI7V0FBQSxBQUFLLGVBQUwsQUFBb0IsQUFDcEI7V0FBQSxBQUFLLHNCQUFMLEFBQTJCLGdCQUEzQixBQUEyQyxBQUM1Qzs7OztnQyxBQUVXLEssQUFBSyxXLEFBQVcsY0FBYyxBQUN4QztVQUFJLE9BQUosQUFBVyxBQUVYOztVQUFBLEFBQUcsV0FBVyxBQUNaO1lBQUcsT0FBQSxBQUFPLGNBQVYsQUFBd0IsVUFBVSxBQUNoQztzQkFBWSxDQUFaLEFBQVksQUFBQyxBQUNkO0FBQ0Q7a0JBQUEsQUFBVSxBQUNWO2tCQUFBLEFBQVUsUUFBUSxVQUFBLEFBQUMsVUFBYSxBQUM5QjtrQkFBUSxPQUFSLEFBQWUsQUFDaEI7QUFGRCxXQUFBLEFBRUcsQUFDSjtBQUVEOzthQUFPLE9BQUEsQUFBTyxPQUFkLEFBQXFCLEFBQ3RCOzs7O3dDLEFBRW1CLGMsQUFBYyxLLEFBQUssVyxBQUFXLFVBQVUsQUFDMUQ7VUFBRyxPQUFBLEFBQU8sY0FBVixBQUF3QixVQUFVLEFBQ2hDO29CQUFZLENBQVosQUFBWSxBQUFDLEFBQ2Q7QUFDRDtVQUFJLE1BQU0sS0FBQSxBQUFLLHNCQUFmLEFBQVUsQUFBMkIsQUFDckM7VUFBSSxlQUFlLEtBQUEsQUFBSyxZQUFMLEFBQWlCLEtBQWpCLEFBQXNCLFdBQXpDLEFBQW1CLEFBQWlDLEFBQ3BEO1VBQUcsQ0FBQyxLQUFBLEFBQUssa0JBQVQsQUFBSSxBQUF1QixlQUFlLEFBQ3hDO2FBQUEsQUFBSyxrQkFBTCxBQUF1QixnQkFBdkIsQUFBdUMsQUFDdkM7WUFBQSxBQUFJLE9BQUosQUFBVztnQkFBWSxBQUNmLEFBQ047bUJBRnFCLEFBRVosQUFDVDtxQkFIcUIsQUFHVixBQUNYO2tCQUpGLEFBQXVCLEFBSWI7QUFKYSxBQUNyQixXQUlDLElBQUEsQUFBSSxVQUxQLEFBS2lCLEFBQ2xCO0FBQ0Q7V0FBQSxBQUFLLGtCQUFMLEFBQXVCLGNBQXZCLEFBQXFDLEtBQXJDLEFBQTBDLEFBQzNDOzs7OzBDLEFBRXFCLGMsQUFBYyxLLEFBQUssVyxBQUFXLFVBQVUsQUFDNUQ7VUFBSSxlQUFlLEtBQUEsQUFBSyxZQUFMLEFBQWlCLEtBQWpCLEFBQXNCLFdBQXpDLEFBQW1CLEFBQWlDLEFBQ3BEO1VBQUkscUJBQXFCLEtBQUEsQUFBSyxrQkFBOUIsQUFBeUIsQUFBdUIsQUFDaEQ7VUFBSSxNQUFNLEtBQUEsQUFBSyxzQkFBZixBQUFVLEFBQTJCLEFBRXJDOztVQUFBLEFBQUcsb0JBQW9CLEFBQ3JCO1lBQUEsQUFBRyxVQUFVLEFBQ1g7Y0FBSSxRQUFRLG1CQUFBLEFBQW1CLFFBQS9CLEFBQVksQUFBMkIsQUFDdkM7ZUFBQSxBQUFLLGtCQUFMLEFBQXVCLGNBQXZCLEFBQXFDLE9BQXJDLEFBQTRDLE9BQTVDLEFBQW1ELEFBQ3BEO0FBSEQsZUFHTyxBQUNMO2lCQUFPLEtBQUEsQUFBSyxrQkFBWixBQUFPLEFBQXVCLEFBQy9CO0FBQ0Q7WUFBRyxJQUFBLEFBQUksVUFBVSxJQUFBLEFBQUksT0FBckIsQUFBNEIsYUFBYSxBQUN2QztjQUFBLEFBQUksT0FBSixBQUFXO2tCQUFZLEFBQ2YsQUFDTjtxQkFGcUIsQUFFWixBQUNUO3VCQUhxQixBQUdWLEFBQ1g7b0JBSkYsQUFBdUIsQUFJYjtBQUphLEFBQ3JCLGFBSUMsSUFBQSxBQUFJLFVBTFAsQUFLaUIsQUFDbEI7QUFDRjtBQUNGOzs7O3lDLEFBRW9CLFVBQVUsQUFDN0I7VUFBSSxPQUFBLEFBQU8sYUFBWCxBQUF3QixZQUFZLEFBQ2xDO2NBQU0sSUFBQSxBQUFJLE1BQVYsQUFBTSxBQUFVLEFBQ2pCO0FBQ0Q7VUFBSSxLQUFBLEFBQUssa0JBQVQsQUFBMkIsTUFBTSxBQUMvQjtjQUFNLElBQUEsQUFBSSxNQUFWLEFBQU0sQUFBVSxBQUNqQjtBQUNEO1dBQUEsQUFBSyxnQkFBTCxBQUFxQixBQUN0Qjs7OztzQyxBQUVpQixPLEFBQU8sS0FBSyxBQUM1QjtVQUFJLE9BQU8sS0FBUCxBQUFZLGtCQUFoQixBQUFrQyxZQUFZLEFBQzVDO2FBQUEsQUFBSztxQkFDUSxJQUFBLEFBQUksVUFERSxBQUNRLEFBQ3pCO2VBQUssSUFBQSxBQUFJLFVBRlEsQUFFRSxBQUNuQjt3QkFBYyxJQUhoQixBQUFtQixBQUdDLEFBRXJCO0FBTG9CLEFBQ2pCO0FBS0w7Ozs7NkNBRXdCLEFBQ3ZCO1dBQUEsQUFBSyxnQkFBTCxBQUFxQixBQUN0Qjs7OztpQ0FFWSxBQUNYO1VBQUksT0FBSixBQUFXLEFBQ1g7ZUFBQSxBQUFTLGFBQVQsQUFBc0IsWUFBWSxBQUNoQztZQUFJLFNBQVMsS0FBQSxBQUFLLHNCQUFsQixBQUFhLEFBQTJCLEFBQ3hDO1lBQUcsQ0FBSCxBQUFJLFFBQU8sQUFDVDtnQkFBTSxJQUFBLEFBQUksTUFBTSw4QkFBaEIsQUFBTSxBQUF3QyxBQUMvQztBQUNEO2lCQUFBLEFBQVMsb0JBQVQsQUFBNkIsS0FBSyxBQUNoQzt3QkFBTyxBQUFPLG9CQUFQLEFBQTJCLEtBQTNCLEFBQWdDLE9BQU8sVUFBQSxBQUFDLGFBQUQsQUFBYyxZQUFlLEFBQ3pFO2dCQUFNLFNBQVMsSUFBZixBQUFlLEFBQUksQUFDbkI7MkJBQUEsQUFBZSwrQ0FBZixBQUFlLEFBQ2Y7bUJBQUEsQUFBSyxBQUNIOzRCQUFBLEFBQVk7d0JBQ0osZUFBQSxBQUFNLGNBRFksQUFDbEIsQUFBb0IsQUFDMUI7a0NBQWdCLE9BQUEsQUFBTyxrQkFGekIsQUFBMEIsQUFFaUIsQUFFM0M7QUFKMEIsQUFDeEI7QUFJSjttQkFBQSxBQUFLLEFBQ0g7b0JBQUksT0FBQSxBQUFPLGVBQVgsQUFBSSxBQUFzQixnQkFBZ0IsQUFDeEM7OEJBQUEsQUFBWSxjQUFjLG9CQUExQixBQUEwQixBQUFvQixBQUMvQztBQUNEO0FBWEYsQUFjQTs7O21CQUFBLEFBQU8sQUFDUjtBQWpCTSxXQUFBLEVBQVAsQUFBTyxBQWlCSixBQUNKO0FBQ0Q7ZUFBTyxvQkFBUCxBQUFPLEFBQW9CLEFBQzVCO0FBQ0Q7b0JBQU8sQUFBTyxvQkFBb0IsS0FBM0IsQUFBZ0MsdUJBQWhDLEFBQXVELE9BQU8sVUFBQSxBQUFDLGFBQUQsQUFBYyxZQUFlLEFBQ2hHO29CQUFBLEFBQVksY0FBYyxhQUExQixBQUEwQixBQUFhLEFBQ3ZDO2VBQUEsQUFBTyxBQUNSO0FBSE0sT0FBQSxFQUFQLEFBQU8sQUFHSixBQUNKO0FBR0Q7Ozs7OztpQyxBQUNhLE8sQUFBTyxLQUFLLEFBQ3ZCO1VBQUksa0JBQWtCLENBQUEsQUFBQyxRQUF2QixBQUFzQixBQUFTLEFBQy9CO1VBQUksaUJBQWlCLE9BQU8sQ0FBQyxJQUFSLEFBQVksVUFBVSxnQkFBQSxBQUFnQixRQUFRLE1BQUEsQUFBTSxLQUE5QixBQUFtQyxRQUFRLENBQXRGLEFBQXVGLEFBQ3ZGO1VBQUksb0JBQW9CLE9BQU8sTUFBQSxBQUFNLFdBQVcsSUFBaEQsQUFBb0QsQUFDcEQ7VUFBSSxrQkFBa0IsT0FBTyxJQUFBLEFBQUksVUFBSixBQUFjLElBQWQsQUFBa0IsUUFBUSxNQUExQixBQUFnQyxZQUE3RCxBQUF5RSxBQUN6RTtVQUFJLGdCQUFnQixvQkFBb0Isa0JBQXhDLEFBQW9CLEFBQXNDLEFBRTFEOztBQUNBO1VBQUcsTUFBQSxBQUFNLEtBQU4sQUFBVyxTQUFYLEFBQW9CLHFCQUFxQixXQUFXLE9BQXZELEFBQThELEtBQUssQUFDakU7d0JBQUEsQUFBZ0IsQUFDakI7QUFFRDs7QUFDQTtVQUFHLE1BQUEsQUFBTSxLQUFOLEFBQVcsU0FBWCxBQUFvQixhQUFhLHFCQUFxQixNQUFBLEFBQU0sV0FBL0QsQUFBRyxBQUF1RSxZQUFZLEFBQ3BGO3dCQUFBLEFBQWdCLEFBQ2pCO0FBRUQ7O1VBQUcsQ0FBSCxBQUFJLGVBQWUsQUFDakI7dUJBQUEsQUFBTSxJQUFJLGdDQUFnQyxNQUExQyxBQUFnRCxBQUNqRDtBQUNEO2FBQUEsQUFBTyxBQUNSOzs7OzRDLEFBRXVCLFFBQVEsQUFDOUI7VUFBQSxBQUFHLFFBQVEsQUFDVDtlQUFPLEtBQUEsQUFBSyxtQkFBWixBQUFPLEFBQXdCLEFBQ2hDO0FBQ0Q7YUFBTyxLQUFQLEFBQVksQUFDYjs7Ozt3QyxBQUVtQixRQUFRLEFBQzFCO1VBQUksZ0JBQWdCLEtBQUEsQUFBSyxtQkFBekIsQUFBb0IsQUFBd0IsQUFDNUM7VUFBRyxjQUFBLEFBQWMsV0FBakIsQUFBNEIsR0FBRSxBQUM1QjtzQkFBQSxBQUFjLFFBQVEsVUFBQSxBQUFTLGNBQWM7dUJBQzNDOztjQUFJLE9BQU8sT0FBQSxBQUFPLEtBQUssS0FBdkIsQUFBVyxBQUFpQixBQUM1QjtlQUFBLEFBQUssUUFBUSxVQUFBLEFBQUMsT0FBVSxBQUN0QjtnQkFBSSxVQUFVLE9BQUEsQUFBSyxlQUFuQixBQUFjLEFBQW9CLEFBQ2xDO2dCQUFJLGFBQWEsUUFBQSxBQUFRLGNBQXpCLEFBQXVDLEFBRXZDOztnQkFBSSxXQUFBLEFBQVcsY0FBYyxhQUFBLEFBQWEsVUFBMUMsQUFBb0QsV0FBVyxBQUM3RDtxQkFBTyxPQUFBLEFBQUssZUFBWixBQUFPLEFBQW9CLEFBQzVCO0FBQ0Y7QUFQRCxBQVNBOztpQkFBTyxLQUFBLEFBQUssc0JBQXNCLGFBQWxDLEFBQU8sQUFBd0MsQUFDaEQ7QUFaRCxXQUFBLEFBWUcsQUFDSjtBQUNGOzs7OztBLEFBbGhCa0IsQUFzaEJyQjs7a0JBQUEsQUFBZSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCJpbXBvcnQgVXRpbCBmcm9tICcuL3V0aWwnO1xuY2xhc3MgUG9zdE1lc3NhZ2Uge1xuXG4gIGNvbnN0cnVjdG9yKGRhdGEpIHtcbiAgICBsZXQgZCA9IGRhdGEgfHwge307XG4gICAgdGhpcy5fcmVnaXN0ZXJMaXN0ZW5lcihkLmxpc3Rlbk9uKTtcbiAgfVxuXG4gIF9yZWdpc3Rlckxpc3RlbmVyKGxpc3Rlbk9uKSB7XG4gICAgaWYoIWxpc3Rlbk9uIHx8ICFsaXN0ZW5Pbi5hZGRFdmVudExpc3RlbmVyKSB7XG4gICAgICBsaXN0ZW5PbiA9IHdpbmRvdztcbiAgICB9XG4gICAgbGlzdGVuT24uYWRkRXZlbnRMaXN0ZW5lcihcIm1lc3NhZ2VcIiwgVXRpbC5fYmluZCh0aGlzLCB0aGlzLl9yZWNlaXZlTWVzc2FnZSksIGZhbHNlKTtcbiAgfVxuXG4gIF9yZWNlaXZlTWVzc2FnZSAoZXZlbnQpIHtcblxuICAgIGxldCBoYW5kbGVyID0gdGhpcy5fbWVzc2FnZUhhbmRsZXJzW2V2ZW50LmRhdGEudHlwZV0sXG4gICAgICBleHRlbnNpb25JZCA9IGV2ZW50LmRhdGEuZWlkLFxuICAgICAgcmVnO1xuXG4gICAgaWYoZXh0ZW5zaW9uSWQgJiYgdGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnMpe1xuICAgICAgcmVnID0gdGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnNbZXh0ZW5zaW9uSWRdO1xuICAgIH1cblxuICAgIGlmICghaGFuZGxlciB8fCAhdGhpcy5fY2hlY2tPcmlnaW4oZXZlbnQsIHJlZykpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBoYW5kbGVyLmNhbGwodGhpcywgZXZlbnQsIHJlZyk7XG4gIH1cblxufVxuXG5leHBvcnQgZGVmYXVsdCBQb3N0TWVzc2FnZTtcbiIsImNvbnN0IExPR19QUkVGSVggPSBcIltTaW1wbGUtWERNXSBcIjtcbmNvbnN0IG5hdGl2ZUJpbmQgPSBGdW5jdGlvbi5wcm90b3R5cGUuYmluZDtcbmNvbnN0IHV0aWwgPSB7XG5cbiAgbG9jYXRpb25PcmlnaW4oKSB7XG4gICAgaWYgKCF3aW5kb3cubG9jYXRpb24ub3JpZ2luKSB7XG4gICAgICByZXR1cm4gd2luZG93LmxvY2F0aW9uLnByb3RvY29sICsgXCIvL1wiICsgd2luZG93LmxvY2F0aW9uLmhvc3RuYW1lICsgKHdpbmRvdy5sb2NhdGlvbi5wb3J0ID8gJzonICsgd2luZG93LmxvY2F0aW9uLnBvcnQgOiAnJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiB3aW5kb3cubG9jYXRpb24ub3JpZ2luO1xuICAgIH1cbiAgfSxcblxuICByYW5kb21TdHJpbmcoKSB7XG4gICAgcmV0dXJuIE1hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIDEwMDAwMDAwMDApLnRvU3RyaW5nKDE2KTtcbiAgfSxcblxuICBpc1N0cmluZyhzdHIpIHtcbiAgICByZXR1cm4gdHlwZW9mIHN0ciA9PT0gXCJzdHJpbmdcIiB8fCBzdHIgaW5zdGFuY2VvZiBTdHJpbmc7XG4gIH0sXG5cbiAgYXJndW1lbnRzVG9BcnJheShhcnJheUxpa2UpIHtcbiAgICByZXR1cm4gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoYXJyYXlMaWtlKTtcbiAgfSxcblxuICBhcmd1bWVudE5hbWVzKGZuKSB7XG4gICAgcmV0dXJuIGZuLnRvU3RyaW5nKClcbiAgICAgIC5yZXBsYWNlKC8oKFxcL1xcLy4qJCl8KFxcL1xcKlteXSo/XFwqXFwvKSkvbWcsICcnKSAvLyBzdHJpcCBjb21tZW50c1xuICAgICAgLnJlcGxhY2UoL1teKF0rXFwoKFteKV0qKVteXSsvLCAnJDEnKSAvLyBnZXQgc2lnbmF0dXJlXG4gICAgICAubWF0Y2goLyhbXlxccyxdKykvZykgfHwgW107XG4gIH0sXG5cbiAgaGFzQ2FsbGJhY2soYXJncykge1xuICAgIHZhciBsZW5ndGggPSBhcmdzLmxlbmd0aDtcbiAgICByZXR1cm4gbGVuZ3RoID4gMCAmJiB0eXBlb2YgYXJnc1tsZW5ndGggLSAxXSA9PT0gJ2Z1bmN0aW9uJztcbiAgfSxcblxuICBlcnJvcihtc2cpIHtcbiAgICBpZiAod2luZG93LmNvbnNvbGUgJiYgd2luZG93LmNvbnNvbGUuZXJyb3IpIHtcbiAgICAgIHZhciBvdXRwdXRFcnJvciA9IFtdO1xuXG4gICAgICBpZiAodHlwZW9mIG1zZyA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgICBvdXRwdXRFcnJvci5wdXNoKExPR19QUkVGSVggKyBtc2cpO1xuICAgICAgICBvdXRwdXRFcnJvciA9IG91dHB1dEVycm9yLmNvbmNhdChBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChhcmd1bWVudHMsIDEpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIG91dHB1dEVycm9yLnB1c2goTE9HX1BSRUZJWCk7XG4gICAgICAgIG91dHB1dEVycm9yID0gb3V0cHV0RXJyb3IuY29uY2F0KEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKGFyZ3VtZW50cykpO1xuICAgICAgfVxuICAgICAgd2luZG93LmNvbnNvbGUuZXJyb3IuYXBwbHkobnVsbCwgb3V0cHV0RXJyb3IpO1xuICAgIH1cbiAgfSxcblxuICB3YXJuKG1zZykge1xuICAgIGlmICh3aW5kb3cuY29uc29sZSkge1xuICAgICAgY29uc29sZS53YXJuKExPR19QUkVGSVggKyBtc2cpO1xuICAgIH1cbiAgfSxcbiAgbG9nKG1zZykge1xuICAgIGlmKHdpbmRvdy5jb25zb2xlKSB7XG4gICAgICB3aW5kb3cuY29uc29sZS5sb2coTE9HX1BSRUZJWCArIG1zZyk7XG4gICAgfVxuICB9LFxuXG4gIF9iaW5kKHRoaXNwLCBmbikge1xuICAgIGlmIChuYXRpdmVCaW5kICYmIGZuLmJpbmQgPT09IG5hdGl2ZUJpbmQpIHtcbiAgICAgIHJldHVybiBmbi5iaW5kKHRoaXNwKTtcbiAgICB9XG4gICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiBmbi5hcHBseSh0aGlzcCwgYXJndW1lbnRzKTtcbiAgICB9O1xuICB9LFxuXG4gIHRocm90dGxlKGZ1bmMsIHdhaXQsIGNvbnRleHQpIHtcbiAgICB2YXIgcHJldmlvdXMgPSAwO1xuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICB2YXIgbm93ID0gRGF0ZS5ub3coKTtcbiAgICAgIGlmIChub3cgLSBwcmV2aW91cyA+IHdhaXQpIHtcbiAgICAgICAgcHJldmlvdXMgPSBub3c7XG4gICAgICAgIGZ1bmMuYXBwbHkoY29udGV4dCwgYXJndW1lbnRzKTtcbiAgICAgIH1cbiAgICB9O1xuICB9LFxuXG4gIGVhY2gobGlzdCwgaXRlcmF0ZWUpIHtcbiAgICB2YXIgbGVuZ3RoO1xuICAgIHZhciBrZXk7XG4gICAgaWYgKGxpc3QpIHtcbiAgICAgIGxlbmd0aCA9IGxpc3QubGVuZ3RoO1xuICAgICAgaWYgKGxlbmd0aCAhPSBudWxsICYmIHR5cGVvZiBsaXN0ICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIGtleSA9IDA7XG4gICAgICAgIHdoaWxlIChrZXkgPCBsZW5ndGgpIHtcbiAgICAgICAgICBpZiAoaXRlcmF0ZWUuY2FsbChsaXN0W2tleV0sIGtleSwgbGlzdFtrZXldKSA9PT0gZmFsc2UpIHtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIH1cbiAgICAgICAgICBrZXkgKz0gMTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZm9yIChrZXkgaW4gbGlzdCkge1xuICAgICAgICAgIGlmIChsaXN0Lmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgIGlmIChpdGVyYXRlZS5jYWxsKGxpc3Rba2V5XSwga2V5LCBsaXN0W2tleV0pID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH0sXG5cbiAgZXh0ZW5kKGRlc3QpIHtcbiAgICB2YXIgYXJncyA9IGFyZ3VtZW50cztcbiAgICB2YXIgc3JjcyA9IFtdLnNsaWNlLmNhbGwoYXJncywgMSwgYXJncy5sZW5ndGgpO1xuICAgIHNyY3MuZm9yRWFjaChmdW5jdGlvbiAoc291cmNlKSB7XG4gICAgICBpZiAodHlwZW9mIHNvdXJjZSA9PT0gXCJvYmplY3RcIikge1xuICAgICAgICBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyhzb3VyY2UpLmZvckVhY2goZnVuY3Rpb24gKG5hbWUpIHtcbiAgICAgICAgICBkZXN0W25hbWVdID0gc291cmNlW25hbWVdO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gZGVzdDtcbiAgfSxcblxuICBzYW5pdGl6ZVN0cnVjdHVyZWRDbG9uZShvYmplY3QpIHtcbiAgICBjb25zdCB3aGl0ZUxpc3QgPSBbQm9vbGVhbiwgU3RyaW5nLCBEYXRlLCBSZWdFeHAsIEJsb2IsIEZpbGUsIEZpbGVMaXN0LCBBcnJheUJ1ZmZlcl07XG4gICAgY29uc3QgYmxhY2tMaXN0ID0gW0Vycm9yLCBOb2RlXTtcbiAgICBjb25zdCB3YXJuID0gdXRpbC53YXJuO1xuICAgIHZhciB2aXNpdGVkT2JqZWN0cyA9IFtdO1xuXG4gICAgZnVuY3Rpb24gX2Nsb25lKHZhbHVlKSB7XG4gICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICAgIHdhcm4oXCJBIGZ1bmN0aW9uIHdhcyBkZXRlY3RlZCBhbmQgcmVtb3ZlZCBmcm9tIHRoZSBtZXNzYWdlLlwiKTtcbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgICB9XG5cbiAgICAgIGlmIChibGFja0xpc3Quc29tZSh0ID0+IHtcbiAgICAgICAgaWYgKHZhbHVlIGluc3RhbmNlb2YgdCkge1xuICAgICAgICAgIHdhcm4oYCR7dC5uYW1lfSBvYmplY3Qgd2FzIGRldGVjdGVkIGFuZCByZW1vdmVkIGZyb20gdGhlIG1lc3NhZ2UuYCk7XG4gICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgICAgfSkpIHtcbiAgICAgICAgcmV0dXJuIHt9O1xuICAgICAgfVxuXG4gICAgICBpZiAodmFsdWUgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB3aGl0ZUxpc3QuZXZlcnkodCA9PiAhKHZhbHVlIGluc3RhbmNlb2YgdCkpKSB7XG4gICAgICAgIGxldCBuZXdWYWx1ZTtcblxuICAgICAgICBpZiAoQXJyYXkuaXNBcnJheSh2YWx1ZSkpIHtcbiAgICAgICAgICBuZXdWYWx1ZSA9IHZhbHVlLm1hcChmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICAgICAgcmV0dXJuIF9jbG9uZShlbGVtZW50KTtcbiAgICAgICAgICB9KTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAodmlzaXRlZE9iamVjdHMuaW5kZXhPZih2YWx1ZSkgPiAtMSkge1xuICAgICAgICAgICAgd2FybihcIkEgY2lyY3VsYXIgcmVmZXJlbmNlIHdhcyBkZXRlY3RlZCBhbmQgcmVtb3ZlZCBmcm9tIHRoZSBtZXNzYWdlLlwiKTtcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIHZpc2l0ZWRPYmplY3RzLnB1c2godmFsdWUpO1xuXG4gICAgICAgICAgbmV3VmFsdWUgPSB7fTtcblxuICAgICAgICAgIGZvciAobGV0IG5hbWUgaW4gdmFsdWUpIHtcbiAgICAgICAgICAgIGlmICh2YWx1ZS5oYXNPd25Qcm9wZXJ0eShuYW1lKSkge1xuICAgICAgICAgICAgICBsZXQgY2xvbmVkVmFsdWUgPSBfY2xvbmUodmFsdWVbbmFtZV0pO1xuICAgICAgICAgICAgICBpZiAoY2xvbmVkVmFsdWUgIT09IG51bGwpIHtcbiAgICAgICAgICAgICAgICBuZXdWYWx1ZVtuYW1lXSA9IGNsb25lZFZhbHVlO1xuICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgdmlzaXRlZE9iamVjdHMucG9wKCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG5ld1ZhbHVlO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHZhbHVlO1xuICAgIH1cblxuICAgIHJldHVybiBfY2xvbmUob2JqZWN0KTtcbiAgfVxufTtcblxuZXhwb3J0IGRlZmF1bHQgdXRpbDtcbiIsImltcG9ydCBYRE1SUEMgZnJvbSAnLi94ZG1ycGMnO1xuaW1wb3J0IFV0aWxzIGZyb20gJy4uL2NvbW1vbi91dGlsJztcbmNsYXNzIENvbm5lY3Qge1xuXG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHRoaXMuX3hkbSA9IG5ldyBYRE1SUEMoKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZW5kIGEgbWVzc2FnZSB0byBpZnJhbWVzIG1hdGNoaW5nIHRoZSB0YXJnZXRTcGVjLiBUaGlzIG1lc3NhZ2UgaXMgYWRkZWQgdG9cbiAgICogIGEgbWVzc2FnZSBxdWV1ZSBmb3IgZGVsaXZlcnkgdG8gZW5zdXJlIHRoZSBtZXNzYWdlIGlzIHJlY2VpdmVkIGlmIGFuIGlmcmFtZVxuICAgKiAgaGFzIG5vdCB5ZXQgbG9hZGVkXG4gICAqXG4gICAqIEBwYXJhbSB0eXBlIFRoZSBuYW1lIG9mIHRoZSBldmVudCB0eXBlXG4gICAqIEBwYXJhbSB0YXJnZXRTcGVjIFRoZSBzcGVjIHRvIG1hdGNoIGFnYWluc3QgZXh0ZW5zaW9ucyB3aGVuIHNlbmRpbmcgdGhpcyBldmVudFxuICAgKiBAcGFyYW0gZXZlbnQgVGhlIGV2ZW50IHBheWxvYWRcbiAgICogQHBhcmFtIGNhbGxiYWNrIEEgY2FsbGJhY2sgdG8gYmUgZXhlY3V0ZWQgd2hlbiB0aGUgcmVtb3RlIGlmcmFtZSBjYWxscyBpdHMgY2FsbGJhY2tcbiAgICovXG4gIGRpc3BhdGNoKHR5cGUsIHRhcmdldFNwZWMsIGV2ZW50LCBjYWxsYmFjaykge1xuICAgIHRoaXMuX3hkbS5xdWV1ZUV2ZW50KHR5cGUsIHRhcmdldFNwZWMsIGV2ZW50LCBjYWxsYmFjayk7XG4gICAgcmV0dXJuIHRoaXMuZ2V0RXh0ZW5zaW9ucyh0YXJnZXRTcGVjKTtcbiAgfVxuXG4gIC8qKlxuICAgKiBTZW5kIGEgbWVzc2FnZSB0byBpZnJhbWVzIG1hdGNoaW5nIHRoZSB0YXJnZXRTcGVjIGltbWVkaWF0ZWx5LiBUaGlzIG1lc3NhZ2Ugd2lsbFxuICAgKiAgb25seSBiZSBzZW50IHRvIGlmcmFtZXMgdGhhdCBhcmUgYWxyZWFkeSBvcGVuLCBhbmQgd2lsbCBub3QgYmUgZGVsaXZlcmVkIGlmIG5vbmVcbiAgICogIGFyZSBjdXJyZW50bHkgb3Blbi5cbiAgICpcbiAgICogQHBhcmFtIHR5cGUgVGhlIG5hbWUgb2YgdGhlIGV2ZW50IHR5cGVcbiAgICogQHBhcmFtIHRhcmdldFNwZWMgVGhlIHNwZWMgdG8gbWF0Y2ggYWdhaW5zdCBleHRlbnNpb25zIHdoZW4gc2VuZGluZyB0aGlzIGV2ZW50XG4gICAqIEBwYXJhbSBldmVudCBUaGUgZXZlbnQgcGF5bG9hZFxuICAgKi9cbiAgYnJvYWRjYXN0KHR5cGUsIHRhcmdldFNwZWMsIGV2ZW50KSB7XG4gICAgdGhpcy5feGRtLmRpc3BhdGNoKHR5cGUsIHRhcmdldFNwZWMsIGV2ZW50LCBudWxsLCBudWxsKTtcbiAgICByZXR1cm4gdGhpcy5nZXRFeHRlbnNpb25zKHRhcmdldFNwZWMpO1xuICB9XG5cbiAgX2NyZWF0ZUlkKGV4dGVuc2lvbikge1xuICAgIGlmKCFleHRlbnNpb24uYWRkb25fa2V5IHx8ICFleHRlbnNpb24ua2V5KXtcbiAgICAgIHRocm93IEVycm9yKCdFeHRlbnNpb25zIHJlcXVpcmUgYWRkb25fa2V5IGFuZCBrZXknKTtcbiAgICB9XG4gICAgcmV0dXJuIGV4dGVuc2lvbi5hZGRvbl9rZXkgKyAnX18nICsgZXh0ZW5zaW9uLmtleSArICdfXycgKyBVdGlscy5yYW5kb21TdHJpbmcoKTtcbiAgfVxuICAvKipcbiAgKiBDcmVhdGVzIGEgbmV3IGlmcmFtZWQgbW9kdWxlLCB3aXRob3V0IGFjdHVhbGx5IGNyZWF0aW5nIHRoZSBET00gZWxlbWVudC5cbiAgKiBUaGUgaWZyYW1lIGF0dHJpYnV0ZXMgYXJlIHBhc3NlZCB0byB0aGUgJ3NldHVwQ2FsbGJhY2snLCB3aGljaCBpcyByZXNwb25zaWJsZSBmb3IgY3JlYXRpbmdcbiAgKiB0aGUgRE9NIGVsZW1lbnQgYW5kIHJldHVybmluZyB0aGUgd2luZG93IHJlZmVyZW5jZS5cbiAgKlxuICAqIEBwYXJhbSBleHRlbnNpb24gVGhlIGV4dGVuc2lvbiBkZWZpbml0aW9uLiBFeGFtcGxlOlxuICAqICAge1xuICAqICAgICBhZGRvbl9rZXk6ICdteS1hZGRvbicsXG4gICogICAgIGtleTogJ215LW1vZHVsZScsXG4gICogICAgIHVybDogJ2h0dHBzOi8vZXhhbXBsZS5jb20vbXktbW9kdWxlJyxcbiAgKiAgICAgb3B0aW9uczoge1xuICAqICAgICAgICAgYXV0b3Jlc2l6ZTogZmFsc2UsXG4gICogICAgICAgICBob3N0T3JpZ2luOiAnaHR0cHM6Ly9jb25uZWN0LWhvc3QuZXhhbXBsZS5jb20vJ1xuICAqICAgICB9XG4gICogICB9XG4gICpcbiAgKiBAcGFyYW0gaW5pdENhbGxiYWNrIFRoZSBvcHRpb25hbCBpbml0Q2FsbGJhY2sgaXMgY2FsbGVkIHdoZW4gdGhlIGJyaWRnZSBiZXR3ZWVuIGhvc3QgYW5kIGlmcmFtZSBpcyBlc3RhYmxpc2hlZC5cbiAgKiovXG4gIGNyZWF0ZShleHRlbnNpb24sIGluaXRDYWxsYmFjaykge1xuICAgIGxldCBleHRlbnNpb25faWQgPSB0aGlzLnJlZ2lzdGVyRXh0ZW5zaW9uKGV4dGVuc2lvbiwgaW5pdENhbGxiYWNrKTtcbiAgICBsZXQgb3B0aW9ucyA9IGV4dGVuc2lvbi5vcHRpb25zIHx8IHt9O1xuXG4gICAgbGV0IGRhdGEgPSB7XG4gICAgICBleHRlbnNpb25faWQ6IGV4dGVuc2lvbl9pZCxcbiAgICAgIGFwaTogdGhpcy5feGRtLmdldEFwaVNwZWMoKSxcbiAgICAgIG9yaWdpbjogVXRpbHMubG9jYXRpb25PcmlnaW4oKSxcbiAgICAgIG9wdGlvbnM6IG9wdGlvbnNcbiAgICB9O1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIGlkOiBleHRlbnNpb25faWQsXG4gICAgICBuYW1lOiBKU09OLnN0cmluZ2lmeShkYXRhKSxcbiAgICAgIHNyYzogZXh0ZW5zaW9uLnVybFxuICAgIH07XG4gIH1cblxuICByZWdpc3RlclJlcXVlc3ROb3RpZmllcihjYWxsYmFjaykge1xuICAgIHRoaXMuX3hkbS5yZWdpc3RlclJlcXVlc3ROb3RpZmllcihjYWxsYmFjayk7XG4gIH1cblxuICByZWdpc3RlckV4dGVuc2lvbihleHRlbnNpb24sIGluaXRDYWxsYmFjaywgdW5sb2FkQ2FsbGJhY2spIHtcbiAgICBsZXQgZXh0ZW5zaW9uX2lkID0gdGhpcy5fY3JlYXRlSWQoZXh0ZW5zaW9uKTtcbiAgICB0aGlzLl94ZG0ucmVnaXN0ZXJFeHRlbnNpb24oZXh0ZW5zaW9uX2lkLCB7XG4gICAgICBleHRlbnNpb246IGV4dGVuc2lvbixcbiAgICAgIGluaXRDYWxsYmFjazogaW5pdENhbGxiYWNrLFxuICAgICAgdW5sb2FkQ2FsbGJhY2s6IHVubG9hZENhbGxiYWNrXG4gICAgfSk7XG4gICAgcmV0dXJuIGV4dGVuc2lvbl9pZDtcbiAgfVxuXG4gIHJlZ2lzdGVyS2V5TGlzdGVuZXIoZXh0ZW5zaW9uX2lkLCBrZXksIG1vZGlmaWVycywgY2FsbGJhY2spIHtcbiAgICB0aGlzLl94ZG0ucmVnaXN0ZXJLZXlMaXN0ZW5lcihleHRlbnNpb25faWQsIGtleSwgbW9kaWZpZXJzLCBjYWxsYmFjayk7XG4gIH1cblxuICB1bnJlZ2lzdGVyS2V5TGlzdGVuZXIoZXh0ZW5zaW9uX2lkLCBrZXksIG1vZGlmaWVycywgY2FsbGJhY2spIHtcbiAgICB0aGlzLl94ZG0udW5yZWdpc3RlcktleUxpc3RlbmVyKGV4dGVuc2lvbl9pZCwga2V5LCBtb2RpZmllcnMsIGNhbGxiYWNrKTtcbiAgfVxuXG4gIHJlZ2lzdGVyQ2xpY2tIYW5kbGVyKGNhbGxiYWNrKSB7XG4gICAgdGhpcy5feGRtLnJlZ2lzdGVyQ2xpY2tIYW5kbGVyKGNhbGxiYWNrKTtcbiAgfVxuXG4gIHVucmVnaXN0ZXJDbGlja0hhbmRsZXIoKSB7XG4gICAgdGhpcy5feGRtLnVucmVnaXN0ZXJDbGlja0hhbmRsZXIoKTtcbiAgfVxuXG4gIGRlZmluZU1vZHVsZShtb2R1bGVOYW1lLCBtb2R1bGUsIG9wdGlvbnMpIHtcbiAgICB0aGlzLl94ZG0uZGVmaW5lQVBJTW9kdWxlKG1vZHVsZSwgbW9kdWxlTmFtZSwgb3B0aW9ucyk7XG4gIH1cblxuICBkZWZpbmVHbG9iYWxzKG1vZHVsZSkge1xuICAgIHRoaXMuX3hkbS5kZWZpbmVBUElNb2R1bGUobW9kdWxlKTtcbiAgfVxuXG4gIGdldEV4dGVuc2lvbnMoZmlsdGVyKSB7XG4gICAgcmV0dXJuIHRoaXMuX3hkbS5nZXRSZWdpc3RlcmVkRXh0ZW5zaW9ucyhmaWx0ZXIpO1xuICB9XG5cbiAgdW5yZWdpc3RlckV4dGVuc2lvbihmaWx0ZXIpIHtcbiAgICByZXR1cm4gdGhpcy5feGRtLnVucmVnaXN0ZXJFeHRlbnNpb24oZmlsdGVyKTtcbiAgfVxuXG4gIHJldHVybnNQcm9taXNlKHdyYXBwZWRNZXRob2QpIHtcbiAgICB3cmFwcGVkTWV0aG9kLnJldHVybnNQcm9taXNlID0gdHJ1ZTtcbiAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IENvbm5lY3Q7XG4iLCJpbXBvcnQgQ29ubmVjdCBmcm9tICcuL2Nvbm5lY3QnO1xuZXhwb3J0IGRlZmF1bHQgbmV3IENvbm5lY3QoKTsiLCIvKipcbiogUG9zdG1lc3NhZ2UgZm9ybWF0OlxuKlxuKiBJbml0aWFsaXphdGlvblxuKiAtLS0tLS0tLS0tLS0tLVxuKiB7XG4qICAgdHlwZTogJ2luaXQnLFxuKiAgIGVpZDogJ215LWFkZG9uX19teS1tb2R1bGUteHl6JyAgLy8gdGhlIGV4dGVuc2lvbiBpZGVudGlmaWVyLCB1bmlxdWUgYWNyb3NzIGlmcmFtZXNcbiogfVxuKlxuKiBSZXF1ZXN0XG4qIC0tLS0tLS1cbioge1xuKiAgIHR5cGU6ICdyZXEnLFxuKiAgIGVpZDogJ215LWFkZG9uX19teS1tb2R1bGUteHl6JywgIC8vIHRoZSBleHRlbnNpb24gaWRlbnRpZmllciwgdW5pcXVlIGZvciBpZnJhbWVcbiogICBtaWQ6ICd4eXonLCAgLy8gYSB1bmlxdWUgbWVzc2FnZSBpZGVudGlmaWVyLCByZXF1aXJlZCBmb3IgY2FsbGJhY2tzXG4qICAgbW9kOiAnY29va2llJywgIC8vIHRoZSBtb2R1bGUgbmFtZVxuKiAgIGZuOiAncmVhZCcsICAvLyB0aGUgbWV0aG9kIG5hbWVcbiogICBhcmdzOiBbYXJndW1lbnRzXSAgLy8gdGhlIG1ldGhvZCBhcmd1bWVudHNcbiogfVxuKlxuKiBSZXNwb25zZVxuKiAtLS0tLS0tLVxuKiB7XG4qICAgdHlwZTogJ3Jlc3AnXG4qICAgZWlkOiAnbXktYWRkb25fX215LW1vZHVsZS14eXonLCAgLy8gdGhlIGV4dGVuc2lvbiBpZGVudGlmaWVyLCB1bmlxdWUgZm9yIGlmcmFtZVxuKiAgIG1pZDogJ3h5eicsICAvLyBhIHVuaXF1ZSBtZXNzYWdlIGlkZW50aWZpZXIsIG9idGFpbmVkIGZyb20gdGhlIHJlcXVlc3RcbiogICBhcmdzOiBbYXJndW1lbnRzXSAgLy8gdGhlIGNhbGxiYWNrIGFyZ3VtZW50c1xuKiB9XG4qXG4qIEV2ZW50XG4qIC0tLS0tXG4qIHtcbiogICB0eXBlOiAnZXZ0JyxcbiogICBldHlwOiAnc29tZS1ldmVudCcsXG4qICAgZXZudDogeyAuLi4gfSAgLy8gdGhlIGV2ZW50IGRhdGFcbiogICBtaWQ6ICd4eXonLCAvLyBhIHVuaXF1ZSBtZXNzYWdlIGlkZW50aWZpZXIgZm9yIHRoZSBldmVudFxuKiB9XG4qKi9cblxuaW1wb3J0IFV0aWxzIGZyb20gJy4uL2NvbW1vbi91dGlsJztcbmltcG9ydCBQb3N0TWVzc2FnZSBmcm9tICcuLi9jb21tb24vcG9zdG1lc3NhZ2UnO1xuXG5sZXQgVkFMSURfRVZFTlRfVElNRV9NUyA9IDMwMDAwOyAvLzMwIHNlY29uZHNcblxuY2xhc3MgWERNUlBDIGV4dGVuZHMgUG9zdE1lc3NhZ2Uge1xuICBfcGFkVW5kZWZpbmVkQXJndW1lbnRzKGFycmF5LCBsZW5ndGgpe1xuICAgIHJldHVybiAoYXJyYXkubGVuZ3RoID49IGxlbmd0aCkgPyBhcnJheSA6IGFycmF5LmNvbmNhdChuZXcgQXJyYXkobGVuZ3RoIC0gYXJyYXkubGVuZ3RoKSk7XG4gIH1cblxuICBjb25zdHJ1Y3Rvcihjb25maWcpIHtcbiAgICBjb25maWcgPSBjb25maWcgfHwge307XG4gICAgc3VwZXIoY29uZmlnKTtcbiAgICB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9ucyA9IGNvbmZpZy5leHRlbnNpb25zIHx8IHt9O1xuICAgIHRoaXMuX3JlZ2lzdGVyZWRBUElNb2R1bGVzID0ge307XG4gICAgdGhpcy5fcmVnaXN0ZXJlZEFQSU1vZHVsZXMuX2dsb2JhbHMgPSB7fTtcbiAgICB0aGlzLl9wZW5kaW5nQ2FsbGJhY2tzID0ge307XG4gICAgdGhpcy5fa2V5Y29kZUNhbGxiYWNrcyA9IHt9O1xuICAgIHRoaXMuX2NsaWNrSGFuZGxlciA9IG51bGw7XG4gICAgdGhpcy5fcGVuZGluZ0V2ZW50cyA9IHt9O1xuICAgIHRoaXMuX21lc3NhZ2VIYW5kbGVycyA9IHtcbiAgICAgIGluaXQ6IHRoaXMuX2hhbmRsZUluaXQsXG4gICAgICByZXE6IHRoaXMuX2hhbmRsZVJlcXVlc3QsXG4gICAgICByZXNwOiB0aGlzLl9oYW5kbGVSZXNwb25zZSxcbiAgICAgIGJyb2FkY2FzdDogdGhpcy5faGFuZGxlQnJvYWRjYXN0LFxuICAgICAgZXZlbnRfcXVlcnk6IHRoaXMuX2hhbmRsZUV2ZW50UXVlcnksXG4gICAgICBrZXlfdHJpZ2dlcmVkOiB0aGlzLl9oYW5kbGVLZXlUcmlnZ2VyZWQsXG4gICAgICBhZGRvbl9jbGlja2VkOiB0aGlzLl9oYW5kbGVBZGRvbkNsaWNrLFxuICAgICAgZ2V0X2hvc3Rfb2Zmc2V0OiB0aGlzLl9nZXRIb3N0T2Zmc2V0LFxuICAgICAgdW5sb2FkOiB0aGlzLl9oYW5kbGVVbmxvYWQsXG4gICAgICBzdWI6IHRoaXMuX2hhbmRsZVN1YkluaXRcbiAgICB9O1xuICB9XG5cbiAgX3ZlcmlmeUFQSShldmVudCwgcmVnKSB7XG4gICAgdmFyIHVudHJ1c3RlZFRhcmdldHMgPSBldmVudC5kYXRhLnRhcmdldHM7XG4gICAgaWYoIXVudHJ1c3RlZFRhcmdldHMpIHtcbiAgICAgIHJldHVybjtcbiAgICB9XG4gICAgdmFyIHRydXN0ZWRTcGVjID0gdGhpcy5nZXRBcGlTcGVjKCk7XG4gICAgdmFyIHRhbXBlcmVkID0gZmFsc2U7XG5cbiAgICBmdW5jdGlvbiBjaGVjayh0cnVzdGVkLCB1bnRydXN0ZWQpe1xuICAgICAgT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModW50cnVzdGVkKS5mb3JFYWNoKGZ1bmN0aW9uKG5hbWUpe1xuICAgICAgICBpZih0eXBlb2YgdW50cnVzdGVkW25hbWVdID09PSAnb2JqZWN0JyAmJiB0cnVzdGVkW25hbWVdKSB7XG4gICAgICAgICAgY2hlY2sodHJ1c3RlZFtuYW1lXSwgdW50cnVzdGVkW25hbWVdKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZih1bnRydXN0ZWRbbmFtZV0gPT09ICdwYXJlbnQnICYmIHRydXN0ZWRbbmFtZV0pIHtcbiAgICAgICAgICAgIHRhbXBlcmVkID0gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH1cbiAgICBjaGVjayh0cnVzdGVkU3BlYywgdW50cnVzdGVkVGFyZ2V0cyk7XG4gICAgZXZlbnQuc291cmNlLnBvc3RNZXNzYWdlKHtcbiAgICAgIHR5cGU6ICdhcGlfdGFtcGVyJyxcbiAgICAgIHRhbXBlcmVkOiB0YW1wZXJlZFxuICAgIH0sIHJlZy5leHRlbnNpb24udXJsKTtcbiAgfVxuXG4gIF9oYW5kbGVJbml0KGV2ZW50LCByZWcpIHtcbiAgICB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9uc1tyZWcuZXh0ZW5zaW9uX2lkXS5zb3VyY2UgPSBldmVudC5zb3VyY2U7XG4gICAgaWYgKHJlZy5pbml0Q2FsbGJhY2spIHtcbiAgICAgIHJlZy5pbml0Q2FsbGJhY2soZXZlbnQuZGF0YS5laWQpO1xuICAgICAgZGVsZXRlIHJlZy5pbml0Q2FsbGJhY2s7XG4gICAgfVxuICAgIGlmKGV2ZW50LmRhdGEudGFyZ2V0cykge1xuICAgICAgdGhpcy5fdmVyaWZ5QVBJKGV2ZW50LCByZWcpO1xuICAgIH1cbiAgfVxuICAvLyBwb3N0TWVzc2FnZSBtZXRob2QgdG8gZG8gcmVnaXN0ZXJFeHRlbnNpb25cbiAgX2hhbmRsZVN1YkluaXQoZXZlbnQsIHJlZykge1xuICAgIHRoaXMucmVnaXN0ZXJFeHRlbnNpb24oZXZlbnQuZGF0YS5leHQuaWQsIHtcbiAgICAgIGV4dGVuc2lvbjogZXZlbnQuZGF0YS5leHRcbiAgICB9KTtcbiAgfVxuXG4gIF9nZXRIb3N0T2Zmc2V0KGV2ZW50LCBfd2luZG93KSB7XG4gICAgdmFyIGhvc3RXaW5kb3cgPSBldmVudC5zb3VyY2U7XG4gICAgdmFyIGhvc3RGcmFtZU9mZnNldCA9IG51bGw7XG4gICAgdmFyIHdpbmRvd1JlZmVyZW5jZSA9IF93aW5kb3cgfHwgd2luZG93OyAvLyBGb3IgdGVzdGluZ1xuXG4gICAgaWYgKHdpbmRvd1JlZmVyZW5jZSA9PT0gd2luZG93UmVmZXJlbmNlLnRvcCAmJiB0eXBlb2Ygd2luZG93UmVmZXJlbmNlLmdldEhvc3RPZmZzZXRGdW5jdGlvbk92ZXJyaWRlID09PSAnZnVuY3Rpb24nKSB7XG4gICAgICBob3N0RnJhbWVPZmZzZXQgPSB3aW5kb3dSZWZlcmVuY2UuZ2V0SG9zdE9mZnNldEZ1bmN0aW9uT3ZlcnJpZGUoaG9zdFdpbmRvdyk7XG4gICAgfVxuXG4gICAgaWYgKHR5cGVvZiBob3N0RnJhbWVPZmZzZXQgIT09ICdudW1iZXInKSB7XG4gICAgICBob3N0RnJhbWVPZmZzZXQgPSAwO1xuICAgICAgLy8gRmluZCB0aGUgY2xvc2VzdCBmcmFtZSB0aGF0IGhhcyB0aGUgc2FtZSBvcmlnaW4gYXMgZXZlbnQgc291cmNlXG4gICAgICB3aGlsZSAoIXRoaXMuX2hhc1NhbWVPcmlnaW4oaG9zdFdpbmRvdykpIHtcbiAgICAgICAgLy8gQ2xpbWIgdXAgdGhlIGlmcmFtZSB0cmVlIDEgbGF5ZXJcbiAgICAgICAgaG9zdEZyYW1lT2Zmc2V0Kys7XG4gICAgICAgIGhvc3RXaW5kb3cgPSBob3N0V2luZG93LnBhcmVudDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBldmVudC5zb3VyY2UucG9zdE1lc3NhZ2Uoe1xuICAgICAgaG9zdEZyYW1lT2Zmc2V0OiBob3N0RnJhbWVPZmZzZXRcbiAgICB9LCBldmVudC5vcmlnaW4pO1xuICB9XG5cbiAgX2hhc1NhbWVPcmlnaW4od2luZG93KSB7XG4gICAgaWYgKHdpbmRvdyA9PT0gd2luZG93LnRvcCkge1xuICAgICAgcmV0dXJuIHRydWU7XG4gICAgfVxuXG4gICAgdHJ5IHtcbiAgICAgIC8vIFRyeSBzZXQgJiByZWFkIGEgdmFyaWFibGUgb24gdGhlIGdpdmVuIHdpbmRvd1xuICAgICAgLy8gSWYgd2UgY2FuIHN1Y2Nlc3NmdWxseSByZWFkIHRoZSB2YWx1ZSB0aGVuIGl0IG1lYW5zIHRoZSBnaXZlbiB3aW5kb3cgaGFzIHRoZSBzYW1lIG9yaWdpblxuICAgICAgLy8gYXMgdGhlIHdpbmRvdyB0aGF0IGlzIGN1cnJlbnRseSBleGVjdXRpbmcgdGhlIHNjcmlwdFxuICAgICAgdmFyIHRlc3RWYXJpYWJsZU5hbWUgPSAndGVzdF92YXJfJyArIE1hdGgucmFuZG9tKCkudG9TdHJpbmcoMTYpLnN1YnN0cigyKTtcbiAgICAgIHdpbmRvd1t0ZXN0VmFyaWFibGVOYW1lXSA9IHRydWU7XG4gICAgICByZXR1cm4gd2luZG93W3Rlc3RWYXJpYWJsZU5hbWVdO1xuICAgIH0gY2F0Y2goZSkge1xuICAgICAgLy8gQSBleGNlcHRpb24gd2lsbCBiZSB0aHJvd24gaWYgdGhlIHdpbmRvd3MgZG9lc24ndCBoYXZlIHRoZSBzYW1lIG9yaWdpblxuICAgIH1cblxuICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIF9oYW5kbGVSZXNwb25zZShldmVudCkge1xuICAgIHZhciBkYXRhID0gZXZlbnQuZGF0YTtcbiAgICB2YXIgcGVuZGluZ0NhbGxiYWNrID0gdGhpcy5fcGVuZGluZ0NhbGxiYWNrc1tkYXRhLm1pZF07XG4gICAgaWYgKHBlbmRpbmdDYWxsYmFjaykge1xuICAgICAgZGVsZXRlIHRoaXMuX3BlbmRpbmdDYWxsYmFja3NbZGF0YS5taWRdO1xuICAgICAgcGVuZGluZ0NhbGxiYWNrLmFwcGx5KHdpbmRvdywgZGF0YS5hcmdzKTtcbiAgICB9XG4gIH1cblxuICByZWdpc3RlclJlcXVlc3ROb3RpZmllcihjYikge1xuICAgIHRoaXMuX3JlZ2lzdGVyZWRSZXF1ZXN0Tm90aWZpZXIgPSBjYjtcbiAgfVxuXG4gIF9oYW5kbGVSZXF1ZXN0KGV2ZW50LCByZWcpIHtcbiAgICBmdW5jdGlvbiBzZW5kUmVzcG9uc2UoKSB7XG4gICAgICB2YXIgYXJncyA9IFV0aWxzLnNhbml0aXplU3RydWN0dXJlZENsb25lKFV0aWxzLmFyZ3VtZW50c1RvQXJyYXkoYXJndW1lbnRzKSk7XG4gICAgICBldmVudC5zb3VyY2UucG9zdE1lc3NhZ2Uoe1xuICAgICAgICBtaWQ6IGV2ZW50LmRhdGEubWlkLFxuICAgICAgICB0eXBlOiAncmVzcCcsXG4gICAgICAgIGZvclBsdWdpbjogdHJ1ZSxcbiAgICAgICAgYXJnczogYXJnc1xuICAgICAgfSwgcmVnLmV4dGVuc2lvbi51cmwpO1xuICAgIH1cblxuICAgIHZhciBkYXRhID0gZXZlbnQuZGF0YTtcbiAgICB2YXIgbW9kdWxlID0gdGhpcy5fcmVnaXN0ZXJlZEFQSU1vZHVsZXNbZGF0YS5tb2RdO1xuICAgIGNvbnN0IGV4dGVuc2lvbiA9IHRoaXMuZ2V0UmVnaXN0ZXJlZEV4dGVuc2lvbnMocmVnLmV4dGVuc2lvbilbMF07XG4gICAgaWYgKG1vZHVsZSkge1xuICAgICAgbGV0IGZuTmFtZSA9IGRhdGEuZm47XG4gICAgICBpZiAoZGF0YS5fY2xzKSB7XG4gICAgICAgIGNvbnN0IENscyA9IG1vZHVsZVtkYXRhLl9jbHNdO1xuICAgICAgICBjb25zdCBucyA9IGRhdGEubW9kICsgJy0nICsgZGF0YS5fY2xzICsgJy0nO1xuICAgICAgICBzZW5kUmVzcG9uc2UuX2lkID0gZGF0YS5faWQ7XG4gICAgICAgIGlmIChmbk5hbWUgPT09ICdjb25zdHJ1Y3RvcicpIHtcbiAgICAgICAgICBpZiAoIUNscy5fY29uc3RydWN0KSB7XG4gICAgICAgICAgICBDbHMuY29uc3RydWN0b3IucHJvdG90eXBlLl9kZXN0cm95ID0gZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgIGRlbGV0ZSB0aGlzLl9jb250ZXh0Ll9wcm94aWVzW25zICsgdGhpcy5faWRdO1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIENscy5fY29uc3RydWN0ID0gZnVuY3Rpb24oLi4uYXJncykge1xuICAgICAgICAgICAgICBjb25zdCBpbnN0ID0gbmV3IENscy5jb25zdHJ1Y3RvciguLi5hcmdzKTtcbiAgICAgICAgICAgICAgY29uc3QgY2FsbGJhY2sgPSBhcmdzW2FyZ3MubGVuZ3RoIC0gMV07XG4gICAgICAgICAgICAgIGluc3QuX2lkID0gY2FsbGJhY2suX2lkO1xuICAgICAgICAgICAgICBpbnN0Ll9jb250ZXh0ID0gY2FsbGJhY2suX2NvbnRleHQ7XG4gICAgICAgICAgICAgIGluc3QuX2NvbnRleHQuX3Byb3hpZXNbbnMgKyBpbnN0Ll9pZF0gPSBpbnN0O1xuICAgICAgICAgICAgICByZXR1cm4gaW5zdDtcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgfVxuICAgICAgICAgIG1vZHVsZSA9IENscztcbiAgICAgICAgICBmbk5hbWUgPSAnX2NvbnN0cnVjdCc7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgbW9kdWxlID0gZXh0ZW5zaW9uLl9wcm94aWVzW25zICsgZGF0YS5faWRdO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgICBsZXQgbWV0aG9kID0gbW9kdWxlW2ZuTmFtZV07XG4gICAgICBpZiAobWV0aG9kKSB7XG4gICAgICAgIHZhciBtZXRob2RBcmdzID0gZGF0YS5hcmdzO1xuICAgICAgICB2YXIgcGFkTGVuZ3RoID0gbWV0aG9kLmxlbmd0aCAtIDE7XG4gICAgICAgIGlmKGZuTmFtZSA9PT0gJ19jb25zdHJ1Y3QnKSB7XG4gICAgICAgICAgcGFkTGVuZ3RoID0gbW9kdWxlLmNvbnN0cnVjdG9yLmxlbmd0aCAtIDE7XG4gICAgICAgIH1cbiAgICAgICAgc2VuZFJlc3BvbnNlLl9jb250ZXh0ID0gZXh0ZW5zaW9uO1xuICAgICAgICBtZXRob2RBcmdzID0gdGhpcy5fcGFkVW5kZWZpbmVkQXJndW1lbnRzKG1ldGhvZEFyZ3MsIHBhZExlbmd0aCk7XG4gICAgICAgIG1ldGhvZEFyZ3MucHVzaChzZW5kUmVzcG9uc2UpO1xuICAgICAgICBjb25zdCBwcm9taXNlUmVzdWx0ID0gbWV0aG9kLmFwcGx5KG1vZHVsZSwgbWV0aG9kQXJncyk7XG5cbiAgICAgICAgaWYgKG1ldGhvZC5yZXR1cm5zUHJvbWlzZSkge1xuICAgICAgICAgIGlmICghcHJvbWlzZVJlc3VsdCB8fCAhKHByb21pc2VSZXN1bHQgaW5zdGFuY2VvZiBQcm9taXNlKSkge1xuICAgICAgICAgICAgc2VuZFJlc3BvbnNlKCdEZWZpbmVkIG1vZHVsZSBtZXRob2QgZGlkIG5vdCByZXR1cm4gYSBwcm9taXNlLicpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBwcm9taXNlUmVzdWx0LnRoZW4ocmVzdWx0ID0+IHtcbiAgICAgICAgICAgICAgc2VuZFJlc3BvbnNlKHVuZGVmaW5lZCwgcmVzdWx0KTtcbiAgICAgICAgICAgIH0pLmNhdGNoKGVyciA9PiB7XG4gICAgICAgICAgICAgIGVyciA9IGVyciBpbnN0YW5jZW9mIEVycm9yID8gZXJyLm1lc3NhZ2UgOiBlcnI7XG4gICAgICAgICAgICAgIHNlbmRSZXNwb25zZShlcnIpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgaWYodGhpcy5fcmVnaXN0ZXJlZFJlcXVlc3ROb3RpZmllcikge1xuICAgICAgICAgIHRoaXMuX3JlZ2lzdGVyZWRSZXF1ZXN0Tm90aWZpZXIuY2FsbChudWxsLCB7XG4gICAgICAgICAgICBtb2R1bGU6IGRhdGEubW9kLFxuICAgICAgICAgICAgZm46IGRhdGEuZm4sXG4gICAgICAgICAgICB0eXBlOiBkYXRhLnR5cGUsXG4gICAgICAgICAgICBhZGRvbl9rZXk6IHJlZy5leHRlbnNpb24uYWRkb25fa2V5LFxuICAgICAgICAgICAga2V5OiByZWcuZXh0ZW5zaW9uLmtleSxcbiAgICAgICAgICAgIGV4dGVuc2lvbl9pZDogcmVnLmV4dGVuc2lvbl9pZFxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgX2hhbmRsZUJyb2FkY2FzdChldmVudCwgcmVnKSB7XG4gICAgdmFyIGV2ZW50X2RhdGEgPSBldmVudC5kYXRhO1xuICAgIHZhciB0YXJnZXRTcGVjID0gciA9PiByLmV4dGVuc2lvbi5hZGRvbl9rZXkgPT09IHJlZy5leHRlbnNpb24uYWRkb25fa2V5ICYmIHIuZXh0ZW5zaW9uX2lkICE9PSByZWcuZXh0ZW5zaW9uX2lkO1xuICAgIHRoaXMuZGlzcGF0Y2goZXZlbnRfZGF0YS5ldHlwLCB0YXJnZXRTcGVjLCBldmVudF9kYXRhLmV2bnQsIG51bGwsIG51bGwpO1xuICB9XG5cbiAgX2hhbmRsZUtleVRyaWdnZXJlZChldmVudCwgcmVnKSB7XG4gICAgdmFyIGV2ZW50RGF0YSA9IGV2ZW50LmRhdGE7XG4gICAgdmFyIGtleWNvZGVFbnRyeSA9IHRoaXMuX2tleWNvZGVLZXkoZXZlbnREYXRhLmtleWNvZGUsIGV2ZW50RGF0YS5tb2RpZmllcnMsIHJlZy5leHRlbnNpb25faWQpO1xuICAgIHZhciBsaXN0ZW5lcnMgPSB0aGlzLl9rZXljb2RlQ2FsbGJhY2tzW2tleWNvZGVFbnRyeV07XG4gICAgaWYobGlzdGVuZXJzKSB7XG4gICAgICBsaXN0ZW5lcnMuZm9yRWFjaCgobGlzdGVuZXIpID0+IHtcbiAgICAgICAgbGlzdGVuZXIuY2FsbChudWxsLCB7XG4gICAgICAgICAgYWRkb25fa2V5OiByZWcuZXh0ZW5zaW9uLmFkZG9uX2tleSxcbiAgICAgICAgICBrZXk6IHJlZy5leHRlbnNpb24ua2V5LFxuICAgICAgICAgIGV4dGVuc2lvbl9pZDogcmVnLmV4dGVuc2lvbl9pZCxcbiAgICAgICAgICBrZXljb2RlOiBldmVudERhdGEua2V5Y29kZSxcbiAgICAgICAgICBtb2RpZmllcnM6IGV2ZW50RGF0YS5tb2RpZmllcnNcbiAgICAgICAgfSk7XG4gICAgICB9LCB0aGlzKTtcbiAgICB9XG4gIH1cblxuICBkZWZpbmVBUElNb2R1bGUobW9kdWxlLCBtb2R1bGVOYW1lKXtcbiAgICBtb2R1bGVOYW1lID0gbW9kdWxlTmFtZSB8fCAnX2dsb2JhbHMnO1xuICAgIHRoaXMuX3JlZ2lzdGVyZWRBUElNb2R1bGVzW21vZHVsZU5hbWVdID0gVXRpbHMuZXh0ZW5kKHt9LCB0aGlzLl9yZWdpc3RlcmVkQVBJTW9kdWxlc1ttb2R1bGVOYW1lXSB8fCB7fSwgbW9kdWxlKTtcbiAgICByZXR1cm4gdGhpcy5fcmVnaXN0ZXJlZEFQSU1vZHVsZXM7XG4gIH1cblxuICBfZnVsbEtleSh0YXJnZXRTcGVjKXtcbiAgICB2YXIga2V5ID0gdGFyZ2V0U3BlYy5hZGRvbl9rZXkgfHwgJ2dsb2JhbCc7XG4gICAgaWYodGFyZ2V0U3BlYy5rZXkpe1xuICAgICAga2V5ID0gYCR7a2V5fUBAJHt0YXJnZXRTcGVjLmtleX1gO1xuICAgIH1cblxuICAgIHJldHVybiBrZXk7XG4gIH1cblxuICBxdWV1ZUV2ZW50KHR5cGUsIHRhcmdldFNwZWMsIGV2ZW50LCBjYWxsYmFjaykge1xuICAgIHZhciBsb2FkZWRfZnJhbWUsXG4gICAgICB0YXJnZXRzID0gdGhpcy5fZmluZFJlZ2lzdHJhdGlvbnModGFyZ2V0U3BlYyk7XG5cbiAgICBsb2FkZWRfZnJhbWUgPSB0YXJnZXRzLnNvbWUoKHRhcmdldCkgPT4ge1xuICAgICAgcmV0dXJuIHRhcmdldC5yZWdpc3RlcmVkX2V2ZW50cyAhPT0gdW5kZWZpbmVkO1xuICAgIH0sIHRoaXMpO1xuXG4gICAgaWYobG9hZGVkX2ZyYW1lKXtcbiAgICAgIHRoaXMuZGlzcGF0Y2godHlwZSwgdGFyZ2V0U3BlYywgZXZlbnQsIGNhbGxiYWNrKTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5fcGVuZGluZ0V2ZW50c1t0aGlzLl9mdWxsS2V5KHRhcmdldFNwZWMpXSA9IHtcbiAgICAgICAgdHlwZSxcbiAgICAgICAgdGFyZ2V0U3BlYyxcbiAgICAgICAgZXZlbnQsXG4gICAgICAgIGNhbGxiYWNrLFxuICAgICAgICB0aW1lOiBuZXcgRGF0ZSgpLmdldFRpbWUoKSxcbiAgICAgICAgdWlkOiBVdGlscy5yYW5kb21TdHJpbmcoKVxuICAgICAgfTtcbiAgICB9XG4gIH1cblxuICBfaGFuZGxlRXZlbnRRdWVyeShtZXNzYWdlLCBleHRlbnNpb24pIHtcbiAgICBsZXQgZXhlY3V0ZWQgPSB7fTtcbiAgICBsZXQgbm93ID0gbmV3IERhdGUoKS5nZXRUaW1lKCk7XG4gICAgbGV0IGtleXMgPSBPYmplY3Qua2V5cyh0aGlzLl9wZW5kaW5nRXZlbnRzKTtcbiAgICBrZXlzLmZvckVhY2goKGluZGV4KSA9PiB7XG4gICAgICBsZXQgZWxlbWVudCA9IHRoaXMuX3BlbmRpbmdFdmVudHNbaW5kZXhdO1xuICAgICAgbGV0IGV2ZW50SXNWYWxpZCA9IChub3cgLSBlbGVtZW50LnRpbWUpIDw9IFZBTElEX0VWRU5UX1RJTUVfTVM7XG4gICAgICBsZXQgaXNTYW1lVGFyZ2V0ID0gIWVsZW1lbnQudGFyZ2V0U3BlYyB8fCB0aGlzLl9maW5kUmVnaXN0cmF0aW9ucyhlbGVtZW50LnRhcmdldFNwZWMpLmxlbmd0aCAhPT0gMDtcblxuICAgICAgaWYgKGlzU2FtZVRhcmdldCAmJiBlbGVtZW50LnRhcmdldFNwZWMua2V5KSB7XG4gICAgICAgIGlzU2FtZVRhcmdldCA9IChlbGVtZW50LnRhcmdldFNwZWMuYWRkb25fa2V5ID09PSBleHRlbnNpb24uZXh0ZW5zaW9uLmFkZG9uX2tleSAmJiBlbGVtZW50LnRhcmdldFNwZWMua2V5ID09PSBleHRlbnNpb24uZXh0ZW5zaW9uLmtleSk7XG4gICAgICB9XG5cbiAgICAgIGlmIChldmVudElzVmFsaWQgJiYgaXNTYW1lVGFyZ2V0KSB7XG4gICAgICAgIGV4ZWN1dGVkW2luZGV4XSA9IGVsZW1lbnQ7XG4gICAgICAgIGVsZW1lbnQudGFyZ2V0U3BlYyA9IGVsZW1lbnQudGFyZ2V0U3BlYyB8fCB7fTtcbiAgICAgICAgdGhpcy5kaXNwYXRjaChlbGVtZW50LnR5cGUsIGVsZW1lbnQudGFyZ2V0U3BlYywgZWxlbWVudC5ldmVudCwgZWxlbWVudC5jYWxsYmFjaywgbWVzc2FnZS5zb3VyY2UpO1xuICAgICAgfSBlbHNlIGlmICghZXZlbnRJc1ZhbGlkKSB7XG4gICAgICAgIGRlbGV0ZSB0aGlzLl9wZW5kaW5nRXZlbnRzW2luZGV4XTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zW2V4dGVuc2lvbi5leHRlbnNpb25faWRdLnJlZ2lzdGVyZWRfZXZlbnRzID0gbWVzc2FnZS5kYXRhLmFyZ3M7XG5cbiAgICByZXR1cm4gZXhlY3V0ZWQ7XG4gIH1cblxuICBfaGFuZGxlVW5sb2FkKGV2ZW50LCByZWcpIHtcbiAgICBpZiAoIXJlZykge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGlmIChyZWcuZXh0ZW5zaW9uX2lkICYmIHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zW3JlZy5leHRlbnNpb25faWRdKSB7XG4gICAgICBkZWxldGUgdGhpcy5fcmVnaXN0ZXJlZEV4dGVuc2lvbnNbcmVnLmV4dGVuc2lvbl9pZF0uc291cmNlO1xuICAgIH1cblxuICAgIGlmIChyZWcudW5sb2FkQ2FsbGJhY2spIHtcbiAgICAgIHJlZy51bmxvYWRDYWxsYmFjayhldmVudC5kYXRhLmVpZCk7XG4gICAgfVxuICB9XG5cbiAgZGlzcGF0Y2godHlwZSwgdGFyZ2V0U3BlYywgZXZlbnQsIGNhbGxiYWNrLCBzb3VyY2UpIHtcbiAgICBmdW5jdGlvbiBzZW5kRXZlbnQocmVnLCBldm50KSB7XG4gICAgICBpZiAocmVnLnNvdXJjZSAmJiByZWcuc291cmNlLnBvc3RNZXNzYWdlKSB7XG4gICAgICAgIHZhciBtaWQ7XG4gICAgICAgIGlmIChjYWxsYmFjaykge1xuICAgICAgICAgIG1pZCA9IFV0aWxzLnJhbmRvbVN0cmluZygpO1xuICAgICAgICAgIHRoaXMuX3BlbmRpbmdDYWxsYmFja3NbbWlkXSA9IGNhbGxiYWNrO1xuICAgICAgICB9XG5cbiAgICAgICAgcmVnLnNvdXJjZS5wb3N0TWVzc2FnZSh7XG4gICAgICAgICAgdHlwZTogJ2V2dCcsXG4gICAgICAgICAgbWlkOiBtaWQsXG4gICAgICAgICAgZXR5cDogdHlwZSxcbiAgICAgICAgICBldm50OiBldm50XG4gICAgICAgIH0sIHJlZy5leHRlbnNpb24udXJsKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB2YXIgcmVnaXN0cmF0aW9ucyA9IHRoaXMuX2ZpbmRSZWdpc3RyYXRpb25zKHRhcmdldFNwZWMgfHwge30pO1xuICAgIHJlZ2lzdHJhdGlvbnMuZm9yRWFjaChmdW5jdGlvbiAocmVnKSB7XG4gICAgICBpZihzb3VyY2UgJiYgIXJlZy5zb3VyY2Upe1xuICAgICAgICByZWcuc291cmNlID0gc291cmNlO1xuICAgICAgfVxuXG4gICAgICBpZiAocmVnLnNvdXJjZSkge1xuICAgICAgICBVdGlscy5fYmluZCh0aGlzLCBzZW5kRXZlbnQpKHJlZywgZXZlbnQpO1xuICAgICAgfVxuICAgIH0sIHRoaXMpO1xuICB9XG5cbiAgX2ZpbmRSZWdpc3RyYXRpb25zKHRhcmdldFNwZWMpIHtcbiAgICBpZih0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9ucy5sZW5ndGggPT09IDApe1xuICAgICAgVXRpbHMuZXJyb3IoJ25vIHJlZ2lzdGVyZWQgZXh0ZW5zaW9ucycsIHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zKTtcbiAgICAgIHJldHVybiBbXTtcbiAgICB9XG4gICAgdmFyIGtleXMgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyh0YXJnZXRTcGVjKTtcbiAgICB2YXIgcmVnaXN0cmF0aW9ucyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eU5hbWVzKHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zKS5tYXAoKGtleSkgPT4ge1xuICAgICAgcmV0dXJuIHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zW2tleV07XG4gICAgfSk7XG5cbiAgICBpZiAodGFyZ2V0U3BlYyBpbnN0YW5jZW9mIEZ1bmN0aW9uKSB7XG4gICAgICByZXR1cm4gcmVnaXN0cmF0aW9ucy5maWx0ZXIodGFyZ2V0U3BlYyk7XG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiByZWdpc3RyYXRpb25zLmZpbHRlcihmdW5jdGlvbiAocmVnKSB7XG4gICAgICAgIHJldHVybiBrZXlzLmV2ZXJ5KGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgICByZXR1cm4gcmVnLmV4dGVuc2lvbltrZXldID09PSB0YXJnZXRTcGVjW2tleV07XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgcmVnaXN0ZXJFeHRlbnNpb24oZXh0ZW5zaW9uX2lkLCBkYXRhKSB7XG4gICAgZGF0YS5fcHJveGllcyA9IHt9O1xuICAgIGRhdGEuZXh0ZW5zaW9uX2lkID0gZXh0ZW5zaW9uX2lkO1xuICAgIHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zW2V4dGVuc2lvbl9pZF0gPSBkYXRhO1xuICB9XG5cbiAgX2tleWNvZGVLZXkoa2V5LCBtb2RpZmllcnMsIGV4dGVuc2lvbl9pZCkge1xuICAgIHZhciBjb2RlID0ga2V5O1xuXG4gICAgaWYobW9kaWZpZXJzKSB7XG4gICAgICBpZih0eXBlb2YgbW9kaWZpZXJzID09PSBcInN0cmluZ1wiKSB7XG4gICAgICAgIG1vZGlmaWVycyA9IFttb2RpZmllcnNdO1xuICAgICAgfVxuICAgICAgbW9kaWZpZXJzLnNvcnQoKTtcbiAgICAgIG1vZGlmaWVycy5mb3JFYWNoKChtb2RpZmllcikgPT4ge1xuICAgICAgICBjb2RlICs9ICckJCcgKyBtb2RpZmllcjtcbiAgICAgIH0sIHRoaXMpO1xuICAgIH1cblxuICAgIHJldHVybiBjb2RlICsgJ19fJyArIGV4dGVuc2lvbl9pZDtcbiAgfVxuXG4gIHJlZ2lzdGVyS2V5TGlzdGVuZXIoZXh0ZW5zaW9uX2lkLCBrZXksIG1vZGlmaWVycywgY2FsbGJhY2spIHtcbiAgICBpZih0eXBlb2YgbW9kaWZpZXJzID09PSBcInN0cmluZ1wiKSB7XG4gICAgICBtb2RpZmllcnMgPSBbbW9kaWZpZXJzXTtcbiAgICB9XG4gICAgdmFyIHJlZyA9IHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zW2V4dGVuc2lvbl9pZF07XG4gICAgdmFyIGtleWNvZGVFbnRyeSA9IHRoaXMuX2tleWNvZGVLZXkoa2V5LCBtb2RpZmllcnMsIGV4dGVuc2lvbl9pZCk7XG4gICAgaWYoIXRoaXMuX2tleWNvZGVDYWxsYmFja3Nba2V5Y29kZUVudHJ5XSkge1xuICAgICAgdGhpcy5fa2V5Y29kZUNhbGxiYWNrc1trZXljb2RlRW50cnldID0gW107XG4gICAgICByZWcuc291cmNlLnBvc3RNZXNzYWdlKHtcbiAgICAgICAgdHlwZTogJ2tleV9saXN0ZW4nLFxuICAgICAgICBrZXljb2RlOiBrZXksXG4gICAgICAgIG1vZGlmaWVyczogbW9kaWZpZXJzLFxuICAgICAgICBhY3Rpb246ICdhZGQnXG4gICAgICB9LCByZWcuZXh0ZW5zaW9uLnVybCk7XG4gICAgfVxuICAgIHRoaXMuX2tleWNvZGVDYWxsYmFja3Nba2V5Y29kZUVudHJ5XS5wdXNoKGNhbGxiYWNrKTtcbiAgfVxuXG4gIHVucmVnaXN0ZXJLZXlMaXN0ZW5lcihleHRlbnNpb25faWQsIGtleSwgbW9kaWZpZXJzLCBjYWxsYmFjaykge1xuICAgIHZhciBrZXljb2RlRW50cnkgPSB0aGlzLl9rZXljb2RlS2V5KGtleSwgbW9kaWZpZXJzLCBleHRlbnNpb25faWQpO1xuICAgIHZhciBwb3RlbnRpYWxDYWxsYmFja3MgPSB0aGlzLl9rZXljb2RlQ2FsbGJhY2tzW2tleWNvZGVFbnRyeV07XG4gICAgdmFyIHJlZyA9IHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zW2V4dGVuc2lvbl9pZF07XG5cbiAgICBpZihwb3RlbnRpYWxDYWxsYmFja3MpIHtcbiAgICAgIGlmKGNhbGxiYWNrKSB7XG4gICAgICAgIHZhciBpbmRleCA9IHBvdGVudGlhbENhbGxiYWNrcy5pbmRleE9mKGNhbGxiYWNrKTtcbiAgICAgICAgdGhpcy5fa2V5Y29kZUNhbGxiYWNrc1trZXljb2RlRW50cnldLnNwbGljZShpbmRleCwgMSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBkZWxldGUgdGhpcy5fa2V5Y29kZUNhbGxiYWNrc1trZXljb2RlRW50cnldO1xuICAgICAgfVxuICAgICAgaWYocmVnLnNvdXJjZSAmJiByZWcuc291cmNlLnBvc3RNZXNzYWdlKSB7XG4gICAgICAgIHJlZy5zb3VyY2UucG9zdE1lc3NhZ2Uoe1xuICAgICAgICAgIHR5cGU6ICdrZXlfbGlzdGVuJyxcbiAgICAgICAgICBrZXljb2RlOiBrZXksXG4gICAgICAgICAgbW9kaWZpZXJzOiBtb2RpZmllcnMsXG4gICAgICAgICAgYWN0aW9uOiAncmVtb3ZlJ1xuICAgICAgICB9LCByZWcuZXh0ZW5zaW9uLnVybCk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmVnaXN0ZXJDbGlja0hhbmRsZXIoY2FsbGJhY2spIHtcbiAgICBpZiAodHlwZW9mIGNhbGxiYWNrICE9PSAnZnVuY3Rpb24nKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ2NhbGxiYWNrIG11c3QgYmUgYSBmdW5jdGlvbicpO1xuICAgIH1cbiAgICBpZiAodGhpcy5fY2xpY2tIYW5kbGVyICE9PSBudWxsKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ0NsaWNrSGFuZGxlciBhbHJlYWR5IHJlZ2lzdGVyZWQnKTtcbiAgICB9XG4gICAgdGhpcy5fY2xpY2tIYW5kbGVyID0gY2FsbGJhY2s7XG4gIH1cblxuICBfaGFuZGxlQWRkb25DbGljayhldmVudCwgcmVnKSB7XG4gICAgaWYgKHR5cGVvZiB0aGlzLl9jbGlja0hhbmRsZXIgPT09ICdmdW5jdGlvbicpIHtcbiAgICAgIHRoaXMuX2NsaWNrSGFuZGxlcih7XG4gICAgICAgIGFkZG9uX2tleTogcmVnLmV4dGVuc2lvbi5hZGRvbl9rZXksXG4gICAgICAgIGtleTogcmVnLmV4dGVuc2lvbi5rZXksXG4gICAgICAgIGV4dGVuc2lvbl9pZDogcmVnLmV4dGVuc2lvbl9pZFxuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbiAgdW5yZWdpc3RlckNsaWNrSGFuZGxlcigpIHtcbiAgICB0aGlzLl9jbGlja0hhbmRsZXIgPSBudWxsO1xuICB9XG5cbiAgZ2V0QXBpU3BlYygpIHtcbiAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgZnVuY3Rpb24gY3JlYXRlTW9kdWxlKG1vZHVsZU5hbWUpIHtcbiAgICAgIHZhciBtb2R1bGUgPSB0aGF0Ll9yZWdpc3RlcmVkQVBJTW9kdWxlc1ttb2R1bGVOYW1lXTtcbiAgICAgIGlmKCFtb2R1bGUpe1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJ1bnJlZ2lzdGVyZWQgQVBJIG1vZHVsZTogXCIgKyBtb2R1bGVOYW1lKTtcbiAgICAgIH1cbiAgICAgIGZ1bmN0aW9uIGdldE1vZHVsZURlZmluaXRpb24obW9kKSB7XG4gICAgICAgIHJldHVybiBPYmplY3QuZ2V0T3duUHJvcGVydHlOYW1lcyhtb2QpLnJlZHVjZSgoYWNjdW11bGF0b3IsIG1lbWJlck5hbWUpID0+IHtcbiAgICAgICAgICBjb25zdCBtZW1iZXIgPSBtb2RbbWVtYmVyTmFtZV07XG4gICAgICAgICAgc3dpdGNoICh0eXBlb2YgbWVtYmVyKSB7XG4gICAgICAgICAgY2FzZSAnZnVuY3Rpb24nOlxuICAgICAgICAgICAgYWNjdW11bGF0b3JbbWVtYmVyTmFtZV0gPSB7XG4gICAgICAgICAgICAgIGFyZ3M6IFV0aWxzLmFyZ3VtZW50TmFtZXMobWVtYmVyKSxcbiAgICAgICAgICAgICAgcmV0dXJuc1Byb21pc2U6IG1lbWJlci5yZXR1cm5zUHJvbWlzZSB8fCBmYWxzZVxuICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgIGNhc2UgJ29iamVjdCc6XG4gICAgICAgICAgICBpZiAobWVtYmVyLmhhc093blByb3BlcnR5KCdjb25zdHJ1Y3RvcicpKSB7XG4gICAgICAgICAgICAgIGFjY3VtdWxhdG9yW21lbWJlck5hbWVdID0gZ2V0TW9kdWxlRGVmaW5pdGlvbihtZW1iZXIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuXG4gICAgICAgICAgcmV0dXJuIGFjY3VtdWxhdG9yO1xuICAgICAgICB9LCB7fSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gZ2V0TW9kdWxlRGVmaW5pdGlvbihtb2R1bGUpO1xuICAgIH1cbiAgICByZXR1cm4gT2JqZWN0LmdldE93blByb3BlcnR5TmFtZXModGhpcy5fcmVnaXN0ZXJlZEFQSU1vZHVsZXMpLnJlZHVjZSgoYWNjdW11bGF0b3IsIG1vZHVsZU5hbWUpID0+IHtcbiAgICAgIGFjY3VtdWxhdG9yW21vZHVsZU5hbWVdID0gY3JlYXRlTW9kdWxlKG1vZHVsZU5hbWUpO1xuICAgICAgcmV0dXJuIGFjY3VtdWxhdG9yO1xuICAgIH0sIHt9KTtcbiAgfVxuXG5cbiAgLy8gdmFsaWRhdGUgb3JpZ2luIG9mIHBvc3RNZXNzYWdlXG4gIF9jaGVja09yaWdpbihldmVudCwgcmVnKSB7XG4gICAgbGV0IG5vX3NvdXJjZV90eXBlcyA9IFsnaW5pdCcsICdldmVudF9xdWVyeSddO1xuICAgIGxldCBpc05vU291cmNlVHlwZSA9IHJlZyAmJiAhcmVnLnNvdXJjZSAmJiBub19zb3VyY2VfdHlwZXMuaW5kZXhPZihldmVudC5kYXRhLnR5cGUpID4gLTE7XG4gICAgbGV0IHNvdXJjZVR5cGVNYXRjaGVzID0gcmVnICYmIGV2ZW50LnNvdXJjZSA9PT0gcmVnLnNvdXJjZTtcbiAgICBsZXQgaGFzRXh0ZW5zaW9uVXJsID0gcmVnICYmIHJlZy5leHRlbnNpb24udXJsLmluZGV4T2YoZXZlbnQub3JpZ2luKSA9PT0gMDtcbiAgICBsZXQgaXNWYWxpZE9yaWdpbiA9IGhhc0V4dGVuc2lvblVybCAmJiAoaXNOb1NvdXJjZVR5cGUgfHwgc291cmNlVHlwZU1hdGNoZXMpO1xuXG4gICAgLy8gZ2V0X2hvc3Rfb2Zmc2V0IGZpcmVzIGJlZm9yZSBpbml0XG4gICAgaWYoZXZlbnQuZGF0YS50eXBlID09PSAnZ2V0X2hvc3Rfb2Zmc2V0JyAmJiB3aW5kb3cgPT09IHdpbmRvdy50b3ApIHtcbiAgICAgIGlzVmFsaWRPcmlnaW4gPSB0cnVlO1xuICAgIH1cblxuICAgIC8vIGNoZWNrIHVuZGVmaW5lZCBmb3IgY2hyb21pdW0gKElzc3VlIDM5NTAxMClcbiAgICBpZihldmVudC5kYXRhLnR5cGUgPT09ICd1bmxvYWQnICYmIChzb3VyY2VUeXBlTWF0Y2hlcyB8fCBldmVudC5zb3VyY2UgPT09IHVuZGVmaW5lZCkpIHtcbiAgICAgIGlzVmFsaWRPcmlnaW4gPSB0cnVlO1xuICAgIH1cblxuICAgIGlmKCFpc1ZhbGlkT3JpZ2luKSB7XG4gICAgICBVdGlscy5sb2coXCJGYWlsZWQgdG8gdmFsaWRhdGUgb3JpZ2luOiBcIiArIGV2ZW50Lm9yaWdpbik7XG4gICAgfVxuICAgIHJldHVybiBpc1ZhbGlkT3JpZ2luO1xuICB9XG5cbiAgZ2V0UmVnaXN0ZXJlZEV4dGVuc2lvbnMoZmlsdGVyKSB7XG4gICAgaWYoZmlsdGVyKSB7XG4gICAgICByZXR1cm4gdGhpcy5fZmluZFJlZ2lzdHJhdGlvbnMoZmlsdGVyKTtcbiAgICB9XG4gICAgcmV0dXJuIHRoaXMuX3JlZ2lzdGVyZWRFeHRlbnNpb25zO1xuICB9XG5cbiAgdW5yZWdpc3RlckV4dGVuc2lvbihmaWx0ZXIpIHtcbiAgICBsZXQgcmVnaXN0cmF0aW9ucyA9IHRoaXMuX2ZpbmRSZWdpc3RyYXRpb25zKGZpbHRlcik7XG4gICAgaWYocmVnaXN0cmF0aW9ucy5sZW5ndGggIT09IDApe1xuICAgICAgcmVnaXN0cmF0aW9ucy5mb3JFYWNoKGZ1bmN0aW9uKHJlZ2lzdHJhdGlvbikge1xuICAgICAgICBsZXQga2V5cyA9IE9iamVjdC5rZXlzKHRoaXMuX3BlbmRpbmdFdmVudHMpO1xuICAgICAgICBrZXlzLmZvckVhY2goKGluZGV4KSA9PiB7XG4gICAgICAgICAgbGV0IGVsZW1lbnQgPSB0aGlzLl9wZW5kaW5nRXZlbnRzW2luZGV4XTtcbiAgICAgICAgICBsZXQgdGFyZ2V0U3BlYyA9IGVsZW1lbnQudGFyZ2V0U3BlYyB8fCB7fTtcblxuICAgICAgICAgIGlmICh0YXJnZXRTcGVjLmFkZG9uX2tleSA9PT0gcmVnaXN0cmF0aW9uLmV4dGVuc2lvbi5hZGRvbl9rZXkpIHtcbiAgICAgICAgICAgIGRlbGV0ZSB0aGlzLl9wZW5kaW5nRXZlbnRzW2luZGV4XTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGRlbGV0ZSB0aGlzLl9yZWdpc3RlcmVkRXh0ZW5zaW9uc1tyZWdpc3RyYXRpb24uZXh0ZW5zaW9uX2lkXTtcbiAgICAgIH0sIHRoaXMpO1xuICAgIH1cbiAgfVxuXG59XG5cbmV4cG9ydCBkZWZhdWx0IFhETVJQQztcbiJdfQ==