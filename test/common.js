window.addEventListener("message", function (event) {
    console.log("received message", event.data);
}, false);

function log(message) {
    var p = document.createElement("p");
    p.append(message);
    document.getElementById("#log-panel").append(p);
}

function setup(parent, id, extension, type, init) {
    let iframeParams = host.create(extension, init);

    let container = document.createElement('div');
    container.setAttribute('class','panel '+type);

    let iframe = document.createElement('iframe');
    iframe.setAttribute('id', id);
    iframe.setAttribute('class', type);
    iframe.setAttribute('name', iframeParams.name);
    iframe.setAttribute('src', iframeParams.src);
    iframe.setAttribute('frameBorder', 0);

    container.append(iframe);
    document.getElementById(parent).appendChild(container);

    return true;
}

function createPanel(parent, id, payload, type) {
    var token = jwt.encode(payload, "CLIENT_SECRET");
    var extension = {
        addon_key: 'my-addon',
        key: id,
        url: 'http://localhost:8080/' + type + '?jwt=' + token
    };

    function init(extensionId) {
        console.log('Bridge established:' + extensionId);
    }

    setup(parent, id, extension, type, init);
}

function clickFlushdbButton() {
    $.post("/test/flushdb",
        function (data) {
            let iframes = document.getElementsByTagName('iframe');
            for (let i = 0, len = frames.length; i < len; i++) {
                iframes[i].contentWindow.location.reload();
            }
            log("server database flushed");
        });
}

function clickSampleStrollButton() {
    let sample = {
        "name": "Sample Stroll",
        "questions": [{
            "prompt": "Sample Stroll",
            "type": "select",
            "options": [
                {"id": "choice_0", "value": {"en": "Choice 1"}},
                {"id": "choice_1", "value": {"en": "Choice 2"}},
                {"id": "choice_2", "value": {"en": "Choice 3"}}
            ]
        }],
        "open": true
    };
    $.ajax({
        type: "POST",
        url: "/polls",
        data: JSON.stringify(sample),
        contentType: "application/json",
        dataType: 'json',
        success: function (data) {
            log("created stroll: " + data);
        }
    });
}

function clickSaveStrollButton() {
    host.dispatch('dialog-button-clicked',
        {
            addon_key: "my-addon"
        },
        {
            action: "action-save"
        },
        function (result) {
            console.log("save: ", result);
        }
    );
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return undefined;
    if (!results[2]) return undefined;
    var val = decodeURIComponent(results[2].replace(/\+/g, " ")).trim();
    return val.length == 0 ? null : val;
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}