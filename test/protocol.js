function log(message) {
    var ptag = document.createElement("p");
    ptag.append(message);
    document.getElementById("log-panel").append(ptag);
}

host.defineModule('dialog', {
    open: function(id) {
        log("dialog opened: "+JSON.stringify(id));
    },
    close: function(id) {
        log("dialog closed");
        let iframes = document.getElementsByTagName('iframe');
        for (let i = 0, len = frames.length; i < len; i++) {
            if(iframes[i].getAttribute('class')=='dialog') {
                iframes[i].contentWindow.location.reload();
            }
        }
    }
});

