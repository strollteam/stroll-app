const redis = require('../data/redis.js');
var {Document} = require('adf-builder');

module.exports = function (stride, log) {

    return {

        sendClosedPollMessage(poll, cloudId, conversationId) {
            redis.getAnswers(poll.id, (err, answers) => {
                if (err) {
                    log.warn(`Error retriving answers for poll ${poll.id}`);
                }
                else {
                    let question = poll.questions[0];
                    // get all of the answers for this particular question
                    let questionAnswers = [];
                    for (var i = 0; i < answers.length; i++) {
                        // { "[questionId]": "[optionId]" }
                        for (var answer in answers[i].answers) {
                            if (answer === question.id) {
                                questionAnswers.push(answers[i].answers[answer]);
                            }
                        }
                    }
    
                    let votedOptions = question.options
                        .reduce((map, option) => {
                            map[option.id] = {votes: 0, value: option.value.en};
                            return map;
                        }, {});
    
                    for (var i = 0; i < questionAnswers.length; i++) {
                        if (votedOptions[questionAnswers[i]]) {
                            votedOptions[questionAnswers[i]].votes++;
                        }
                    }
    
                    let document = new Document();
                    let panel = document.panel('info');
    
                    panel
                        .paragraph()
                        .text(`A poll as just been closed: `)
                        .text(`'${poll.name}'`);
                    panel.paragraph().text(`Here are the results:`);
                    let results = panel.bulletList();
                    for (var option in votedOptions) {
                        results.textItem(`${votedOptions[option].value}: ${votedOptions[option].votes} votes`);
                    }
    
                    stride.sendDocumentMessage(
                        cloudId,
                        conversationId,
                        document.toJSON(),
                        (err, response) => {
                            if (err) {
                                log.warn(`Error sending message to Stride: ${err}`);
                            }
                            else {
                                log.info('Sent poll close message to Stride.');
                            }
                        }
                    );
                }
    
            })
        }
    }

}

module.exports