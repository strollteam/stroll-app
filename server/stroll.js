const uuidv1 = require('uuid/v1');
const redis = require('../data/redis.js');
const {Document} = require('adf-builder');

module.exports = function (app, io, stride, scheduler, documentHelper, log, statsd, timer) {

    const KEY_API = "api";
    const KEY_WEB = "web";
    const KEY_API_TIMER = "api.timer";
    const KEY_WEB_TIMER = "web.timer";

    const result = function (result, context, timing) {
        timing && timing.end({result: result});
        statsd.increment(KEY_API, Object.assign(context, {result: result}));
    };

    if (app.environment == "development") {
        app.post('/test/flushdb',
            function (req, res) {
                try {
                    redis.flushdb(function (err, suceeded) {
                        res.sendStatus(200);
                    });
                } catch (e) {
                    log.warn(e);
                }
            }
        );
    }

    app.get('/glance',
        function (req, res) {
            res.send({
                "label": {
                    "value": "Stroll"
                }
            });
        }
    );


    app.get('/polls/:id', function (req, res) {
        const context = {action: "get", type: "poll"};
        const timing = timer.start(KEY_API_TIMER, context);

        let pollId = req.params.id;
        redis.getPoll(pollId, function (err, reply) {
            if (reply) {
                res.json(reply);
                result("success", context, timing);
            }
            else {
                res.sendStatus(404);
                result("failed", context, timing);
            }
        })
    });

    app.delete('/polls/:id', function (req, res) {
        const context = {action: "delete", type: "poll"};
        const timing = timer.start(KEY_API_TIMER, context);

        let pollId = req.params.id;
        redis.getPoll(pollId, function (err, reply) {
            log.debug(`Deleting poll ${pollId}`);
            if (err) {
                log.error("Delete failed: " + err);
                res.status(500).send("Delete failed");
                result("failed", context, timing);
            }
            else if (reply) {
                redis.deletePoll(reply, function (err, reply) {

                    if (err) {
                        log.error("Delete failed: " + err);
                        res.status(500).send("Delete failed");
                        result("failed", context, timing);
                    }
                    else {
                        reply && app.environment === "production" && stride.updateGlanceState(reply.cloudId, reply.conversationId, "app-glance", "Strolls");

                        res.sendStatus(200);
                        result("success", context, timing);
                    }

                });
            }
            else {
                res.sendStatus(404);
                result("failed", context, timing);

            }
        })

    });

    app.get('/polls', function (req, res) {
        const context = {action: "list", type: "poll"};
        const timing = timer.start(KEY_API_TIMER, context);

        let conversationId = req.session.conversationId;
        let userId = req.session.userId;
        redis.getPollsByConversation(conversationId, function (err, polls) {
            if (polls) {

                // strip out sensitive information
                for (let i = 0; i < polls.length; i++) {
                    delete polls[i].cloudId;
                    delete polls[i].conversationId;
                }

                if (userId) {
                    // add context for user if specified
                    redis.getAnswersByUser(userId, function (err, reply) {
                        if (reply) {
                            for (let i = 0, leni = polls.length; i < leni; i++) {
                                let pollId = polls[i].id;
                                polls[i].submitted = false;
                                for (let j = 0, lenj = reply.length; j < lenj; j++) {
                                    if (reply[j].pollId == pollId) {
                                        polls[i].submitted = true;
                                        break;
                                    }
                                }
                            }
                        }
                        res.json(polls);
                        result("success", context, timing);

                    });
                } else {
                    res.json(polls);
                    result("success", context, timing);

                }
            }
            else {
                res.json([]);
                result("empty", context, timing);

            }
        })
    });

    app.post('/polls',
        function (req, res) {
            const context = {action: "create", type: "poll"};
            const timing = timer.start(KEY_API_TIMER, context);

            // get the context from the session
            let cloudId = req.session.cloudId;
            let conversationId = req.session.conversationId;

            let poll = req.body;
            poll.conversationId = conversationId;

            if (!poll.questions || poll.questions.length == 0) {
                log.warn(`Attempted to create poll without questions`);
                res.status(400).send('Polls must have at least one question.');
                result("invalid", context, timing);

            }
            else {

                poll.id = uuidv1();
                poll.open = true;
                poll.questions.forEach((q) => {
                    q.id = uuidv1();
                });
                redis.addPoll(poll);
                log.debug(`Created poll ${poll.id}`);

                io.to(cloudId + "/" + conversationId).emit("new_poll", poll);

                app.environment === "production" && stride.updateGlanceState(poll.cloudId, poll.conversationId, "app-glance", "Strolls");

                // schedule the close time, if supplied
                if (poll.durationSeconds) {
                    let now = new Date();
                    let closeTime = now.setSeconds(now.getSeconds() + poll.durationSeconds);
                    redis.addSchedule(poll, closeTime);
                    let pollId = poll.id;

                    scheduler.schedulePollClose(pollId, closeTime);
                }

                res.json(poll);
                result("success", context, timing);

            }
        });

    app.post('/polls/:id/close', function (req, res) {
        const context = {action: "close", type: "poll"};
        const timing = timer.start(KEY_API_TIMER, context);

        let pollId = req.params.id;
        let cloudId = req.session.cloudId;
        let conversationId = req.session.conversationId;

        redis.getPoll(pollId, function (err, poll) {
            if (poll && poll.open) {

                app.environment === "production" && stride.updateGlanceState(cloudId, conversationId, "app-glance", "Strolls");

                poll.open = false;
                redis.updatePoll(poll);
                log.debug(`Poll ${poll.id} closed: ${!poll.open}`);

                documentHelper.sendClosedPollMessage(poll, cloudId, conversationId);

                res.status(200);
                res.json(poll);
                result("success", context, timing);

            }
            else {
                log.debug(`Tried to close poll ${poll.id} but it could not be found.`);
                res.sendStatus(404);
                result("failed", context, timing);

            }
        })
    });

    app.get('/polls/:id/answers', function (req, res) {
        const context = {action: "list", type: "answer"};
        const timing = timer.start(KEY_API_TIMER, context);

        redis.getAnswers(req.params.id, function (err, reply) {
            if (reply) {
                // strip out user IDs
                for (let i = 0; i < reply.length; i++) {
                    delete reply[i].userId;
                }
                res.json(reply);
                result("success", context, timing);

            }
            else {
                res.json([]);
                result("empty", context, timing);

            }
        });
    });

    app.post('/polls/:id/answers', function (req, res) {
        const context = {action: "create", type: "answer"};
        const timing = timer.start(KEY_API_TIMER, context);

        let answer = req.body;
        let pollId = req.params.id;

        // get the context from the session
        let cloudId = req.session.cloudId;
        let conversationId = req.session.conversationId;
        let userId = req.session.userId;

        log.debug(`Trying to add answer to poll ${userId}.`);
        redis.getPoll(pollId, function (err, poll) {
            if (poll) {
                if (poll.open) {
                    answer.id = uuidv1();
                    answer.userId = userId;
                    answer.pollId = pollId;
                    redis.addAnswer(answer);
                    io.to(cloudId + "/" + conversationId).emit("new_answer", answer);

                    res.json(answer);
                    result("success", context, timing);

                }
                else {
                    log.warn(`Tried to vote on closed poll ${pollId}`);
                    res.status(400).send("This poll is closed, so it can't accept answers.");
                    result("closed", context, timing);

                }

            }
            else {
                log.warn(`Tried to post an answer to an invalid poll ${pollId}`);
                res.status(400).send("Answers must be posted to a valid poll id.");
                result("failed", context, timing);


            }
        });
    });

    app.get('/sidebar',
        stride.authenticate(),
        function (req, res) {
            res.render('sidebar');
            statsd.increment(KEY_WEB, {action: "view", component: "sidebar"});
        }
    );

    app.get('/dialog',
        stride.authenticate(),
        function (req, res) {
            res.render('dialog');
            statsd.increment(KEY_WEB, {action: "view", component: "dialog"});
        });

    app.post('/polls/:id/decisions', function (req, res) {
        const context = {action: "create", type: "decision"};
        const timing = timer.start(KEY_API_TIMER, context);

        let {decisionText} = req.body;

        let cloudId = req.session.cloudId;
        let conversationId = req.session.conversationId;

        if (!req.body.decisionText) {
            log.debug('Tried to create a decision without a decisionText field');
            res.status(400).send(`Cannot create a decision without a decisionText field.`);
            result("failed", context, timing);

            return;
        }

        stride.createDecision({
            decisionText: decisionText,
            cloudId: cloudId,
            conversationId: conversationId
        }, (err, response) => {
            if (err) {
                log.warn(`Error creating decision: ${err}`);
                res.status(500).send(`Problem creating a decision: ${err}`);
                result("failed", context, timing);

            }
            else {
                log.debug(`Created decision`);
                res.sendStatus(200);
                result("success", context, timing);

            }
        });
    });

    app.post('/polls/:id/invite', function (req, res) {
        const context = {action: "invite", type: "poll"};
        const timing = timer.start(KEY_API_TIMER, context);

        let cloudId = req.session.cloudId;
        let conversationId = req.session.conversationId;

        let document = new Document();
        let panel = document.panel('info');
        panel.paragraph()
            .text('There is a new Stroll for you to vote on!');
        panel.paragraph()
            .text(`${req.body.name}`);
        panel.paragraph()
            .text('Open the Stroll ')
            .emoji(':man_walking:')
            .text(' sidebar to respond ');

        stride.sendDocumentMessage(
            cloudId,
            conversationId,
            document.toJSON(),
            (err, response) => {
                if (err) {
                    log.warn(`Error sending message to Stride: ${err}`);
                    res.status(500).send(`Internal error communicating with Stride: ${err}`);
                    result("failed", context, timing);

                }
                else {
                    log.debug('Sent poll invite to Stride.');
                    res.status(200).send();
                    result("success", context, timing);

                }
            }
        );
    });
};
