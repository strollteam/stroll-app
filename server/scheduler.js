const redis = require('../data/redis.js');
const schedule = require('node-schedule');

module.exports = function (log) {

    function executePollClose(pollId) {
        log.info(`Closing poll ${pollId} on schedule.`);
        redis.getPoll(pollId, function(err, poll) {
            if (poll) {
                poll.open = false;
                redis.updatePoll(poll);
                redis.deleteSchedule(pollId);
            }
            else {
                log.warn(`Tried to close poll ${pollId} but it couldn't be found`);
            }
        });
    }

    function schedulePollClose(pollId, closeTime) {
        let closeDate = new Date(closeTime);
        let now = new Date();
        if (closeDate < now) {
            executePollClose(pollId);
        }
        else {
            log.info(`Scheduling poll ${pollId} to close at ${closeDate}`)
            schedule.scheduleJob(closeDate, function() {
                executePollClose(pollId);
            });
        }
    }

    return {
            
        loadSchedules() {
            redis.getSchedules(function(err, schedules) {
                if (err) {
                    log.warn(`Problem loading schedules: ${err}`);
                }
                else if (!schedules) {
                    log.debug('No schedules found.');
                }
                else {
                    log.debug(`Setting up ${schedules.length} schedules.`);
                    for (let i = 0; i < schedules.length; i++) {
                        let schedule = schedules[i];
                        schedulePollClose(schedule.pollId, schedule.closeTime);
                    }
                }
            });
        },

        schedulePollClose(pollId, closeTime) {
            schedulePollClose(pollId, closeTime);
        }
    }
};