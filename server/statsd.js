module.exports = function(logger) {

    const statsd = require('@atlassian/statsd-client');
    const servicePrefix = 'stroll-app.';

    let client = undefined;
    if (process.env.NODE_ENV === 'development') {
        client = statsd.loggingClient({logger, servicePrefix});
    } else {
        client = statsd.statsdClient({
            host: process.env.STATSD_HOSTNAME,
            port: process.env.STATSD_PORT,
            logger,
            servicePrefix
        });
    }

    return client;
};

