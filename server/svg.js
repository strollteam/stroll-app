module.exports = function (app) {
    var fs = require('fs');
    var cheerio = require('cheerio');

    var colours = {
        "red": "#FF5630",
        "green": "#36B37E",
        "blue": "#0052CC"
    };

    function loadLogo(name) {
        var colour = colours[name] ? colours[name] : "black";
        var absfile = __dirname + "/logo.svg";
        fs.readFile(absfile, function (err, data) {
            if (!err) {
                var $ = cheerio.load(data, {xmlMode: true});
                $("path").attr('fill', colour);
                logos[name] = $.root().html();
            }
        });
    }

    var logos = {};

    loadLogo("red");
    loadLogo("green");
    loadLogo("blue");
    loadLogo("black");

    app.get("/logo/:colour.svg", function (req, res) {
        let param = req.params.colour;
        let svg = logos[param] ? logos[param] : logos["black"];
        res.setHeader('Content-Type', 'image/svg+xml');
        res.status(200).send(svg);
    });

    app.get("/logo/:colour.svg", function (req, res) {
        let param = req.params.colour;
        let svg = logos[param] ? logos[param] : logos["black"];
        res.setHeader('Content-Type', 'image/svg+xml');
        res.status(200).send(svg);
    });

};