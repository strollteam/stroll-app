const request = require('request');
const jwtUtil = require('jwt-simple');
const {Document} = require('adf-builder');
const uuidv1 = require('uuid/v1');

module.exports = function (app, log, statsd, timer) {
    const API_BASE_URL = app.environment == "production" ? 'https://api.atlassian.com' : 'https://api.atlassian.com';
    const API_AUDIENCE = app.environment == "production" ? "api.atlassian.com" : "api.atlassian.com";
    const AUTH_API_BASE_URL = app.environment == "production" ? 'https://auth.atlassian.com' : 'https://auth.atlassian.com';
    let token = null;

    const KEY_STRIDE = "stride";
    const KEY_STRIDE_TIMER = "stride.timer";
    const result = function (result, context, timing) {
        timing && timing.end({result: result});
        statsd.increment(KEY_STRIDE, Object.assign(context, {result: result}));
    };

    /**
     * Get an access token from the Atlassian Identity API
     */
    function getAccessToken(callback) {
        const context = {action: "get", type: "token"};
        const timing = timer.start(KEY_STRIDE_TIMER, context);

        if (!token || Date.now() > token.refresh_time) {
            //No token yet, token expired or about to expire
            //Generate a new token
            const options = {
                uri: AUTH_API_BASE_URL + '/oauth/token',
                method: 'POST',
                json: {
                    grant_type: "client_credentials",
                    client_id: app.clientId,
                    client_secret: app.clientSecret,
                    "audience": API_AUDIENCE
                }
            };
            request(options, function (err, response, body) {
                if (response && response.statusCode === 200 && body.access_token) {
                    token = body;
                    //refresh the token a minute before it expires (tokens last for an hour)
                    token.refresh_time = Date.now() + (token.expires_in - 60) * 1000;

                    result("refreshed", context, timing);
                    callback(null, body.access_token);
                } else {
                    result("failed", context, timing);
                    callback("could not generate access token: " + JSON.stringify(response));

                }
            });
        } else {
            result("reused", context, timing);
            callback(null, token.access_token);

        }
    }

    /**
     * https://developer.atlassian.com/cloud/stride/apis/rest/#api-site-cloudId-conversation-conversationId-message-post
     */
    function sendMessage(cloudId, conversationId, messageBody, callback) {
        const context = {action: "send", type: "message"};
        const timing = timer.start(KEY_STRIDE_TIMER, context);

        getAccessToken(function (err, accessToken) {
            if (err) {
                result("failed",context,timing);
                callback(err);

            } else {
                const uri = API_BASE_URL + '/site/' + cloudId + '/conversation/' + conversationId + '/message';
                const options = {
                    uri: uri,
                    method: 'POST',
                    headers: {
                        authorization: "Bearer " + accessToken,
                        "cache-control": "no-cache"
                    },
                    json: {
                        body: messageBody
                    }
                };

                request(options, function (err, response, body) {
                    result("success",context,timing);
                    callback(err, body);
                });
            }
        });
    }

    function updateGlanceState(cloudId, conversationId, glanceKey, stateTxt, callback) {
        const context = {action: "update", type: "glance"};
        const timing = timer.start(KEY_STRIDE_TIMER, context);

        getAccessToken(function (err, accessToken) {
            if (err) {
                result("failed",context,timing);
                callback && callback(err);

            } else {
                const uri = API_BASE_URL + '/app/module/chat/conversation/chat:glance/' + glanceKey + '/state';
                const options = {
                    uri: uri,
                    method: 'POST',
                    headers: {
                        authorization: "Bearer " + accessToken,
                        "cache-control": "no-cache"
                    },
                    json: {
                        "context": {
                            "cloudId": cloudId,
                            "conversationId": conversationId
                        },
                        "label": stateTxt,
                        "metadata": {}
                    }
                };

                request(options, function (err, response, body) {
                    result("success",context,timing);
                    callback && callback(err, body);

                });
            }
        });
    }

    function createDecision(message, callback) {
        const {cloudId, conversationId, decisionText} = message;
        const document = new Document();
        document
            .decisionList(uuidv1())
            .decision(uuidv1(), 'done')
            .text(decisionText);

        log.debug(`Sending decision to cloudId ${cloudId}, conversationId ${conversationId}: ${document.toString()}`);
        sendMessage(cloudId, conversationId, document.toJSON(), callback);
    }

    function authenticate() {
        return function (req, res, next) {
            if (req.query.jwt) {
                const jwt = jwtUtil.decode(req.query.jwt, app.clientSecret, true);
                req.jwt = jwt;
                req.session.userId = jwt.sub;
                req.session.cloudId = jwt.context.cloudId;
                req.session.conversationId = jwt.context.resourceId;
                next();
            } else {
                res.status(403).send("Missing JWT");
            }
        }
    }

    return {

        sendDocumentMessage: sendMessage,

        updateGlanceState: updateGlanceState,

        authenticate: authenticate,

        createDecision: createDecision
    }
};
