module.exports = function(statsd) {

    return {
        start: function(metric,tags1) {
            const startTime = (new Date).getTime();
            return {
                end: function(tags2) {
                    const endTime = (new Date).getTime();
                    const length = endTime-startTime;
                    const tags =  Object.assign({}, tags1, tags2);
                    statsd.timing(metric, length, tags);
                }
            }
        }
    };

};