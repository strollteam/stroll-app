#/bin/sh
. docker.sh
. stride.sh

echo Run Stroll locally using the pre-build docker image.

docker run -ti --rm -e "PORT=$PORT" -e "CLIENT_ID=$CLIENT_ID" \
  -e "CLIENT_SECRET=$CLIENT_SECRET" -p 8080:8080 $DOCKER_IMAGE:$DOCKER_TAG
