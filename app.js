const PORT = 8080;
const CLIENT_ID = process.env.CLIENT_ID || process.argv[1];
const CLIENT_SECRET = process.env.CLIENT_SECRET || process.argv[2];
const version = "0.0.36";

const io = require('socket.io')();
const bunyan = require('bunyan');
const _ = require('lodash');
const express = require('express');
const hbs = require('express-hbs');
const bodyParser = require('body-parser');
const http = require('http');
const request = require('request');
const app = express();
const moment = require('moment');
const scheduler = require('./server/scheduler.js');
const fs = require('fs');
const {Document} = require('adf-builder');

const log = bunyan.createLogger({
    name: 'stroll-app',
    level: 'info',
    serializers: bunyan.stdSerializers
});

let environment = process.env.NODE_ENV ? process.env.NODE_ENV : "production";
log.info("environment: ", environment);
const statsd = require('./server/statsd.js')(log);
const timer = require('./server/timer.js')(statsd);

const stride = require('./server/stride.js')({
    clientId: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    environment: environment
}, log, statsd, timer);
const documentHelper = require('./server/strollDocumentHelper.js')(stride, log);

const statsdMiddleware = require('@atlassian/statsd-client/statsd-middleware')(statsd);

if (!PORT || !CLIENT_ID || !CLIENT_SECRET) {
    log.info("Usage:");
    log.info("PORT=<http port> CLIENT_ID=<app client ID> CLIENT_SECRET=<app client secret> node app.js");
    process.exit();
}

// analytics ---------------------------------

app.environment = environment;
app.use(statsdMiddleware);
statsd.increment('lifecycle', {action: "startup"});

// Configure Express -------------------------

const viewsDir = __dirname + '/views';
const generatedDir = __dirname + '/generated';
const imgDir = __dirname + '/img';

app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
app.set('view engine', 'hbs');
app.set('views', viewsDir);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(imgDir));
app.use(express.static(generatedDir));
if (environment !== 'production') {
    app.use(express.static(__dirname + "/test"));
}

// Healthcheck -------------------------------

app.get('/healthcheck', function (req, res) {
    res.send('OK');
});

// Session -----------------------------------

const session = require("express-session")({
    secret: "my-secret",
    resave: true,
    saveUninitialized: true
});
const sharedsession = require("express-socket.io-session");

app.use(session);

io.use(sharedsession(session, {
    autoSave: true
}));

// Add Cors Headers --------------------------
app.use(function (req, res, next) {
    // res.set('Connection', 'close');
    res.setHeader('Access-Control-Allow-Origin', 'https://app.stride.com');
    next();
});

// Scheduler ---------------------------------

let pollScheduler = scheduler(log);
pollScheduler.loadSchedules();

// Routes ------------------------------------

require('./server/svg.js')(app);

app.post('/installed',
    function (req, res) {
        const cloudId = req.body.cloudId;
        const conversationId = req.body.resourceId;

        let document = new Document();
        document.paragraph()
            .text('Stroll has been added to the conversation. Open the Stroll sidebar on the right to view and create polls.');

        stride.sendDocumentMessage(
            cloudId,
            conversationId,
            document.toJSON(),
            (err, response) => {
                if (err) {
                    log.warn(`Error sending message to Stride: ${err}`);

                }
                else {
                    log.debug('Sent poll invite to Stride.');

                }
            }
        );

        statsd.increment('lifecycle', {action: "installed"});
        res.sendStatus(204);
    }
);

// Descriptor --------------------------------
app.get('/descriptor', function (req, res) {
    fs.readFile('./app-descriptor.json', function (err, descriptorTemplate) {
        const template = _.template(descriptorTemplate);
        const descriptor = template({
            host: 'https://' + req.headers.host
        });
        res.set('Content-Type', 'application/json');
        res.send(descriptor);
    });
});

// Start Server ------------------------------

const server = http.createServer(app);

io.on('connection', function (socket) {
    let cloudId = socket.handshake.session.cloudId;
    let conversationId = socket.handshake.session.conversationId;

    if (cloudId && conversationId) {
        let path = cloudId + "/" + conversationId;
        log.debug("join", path);
        socket.join(path);
    }

    socket.on('disconnect', () => {
        log.debug("socket disconnect: ", socket.id);
    });
});
io.listen(server.listen(PORT));

server.listen(PORT, function () {
    log.debug('App running on port ' + PORT);
});

// Stroll App --------------------------------

const stroll = require('./server/stroll.js')(app, io, stride, pollScheduler, documentHelper, log, statsd, timer);
